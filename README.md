# System
========================

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist exoo/yii2-system
```

or add

```json
"exoo/yii2-system": "*"
```

to the require section of your composer.json.



Usage
-----

1. Example backend configuration:

```php
return [
    'modules' => [
        'system' => [
            'class' => 'exoo\system\Module',
            'isBackend' => true,
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'exoo\system\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/system/site/login'],
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
    ],
];
```

2. Example frontend configuration:

```php
return [
    'modules' => [
        'system' => [
            'class' => 'exoo\system\Module',
        ]
    ],
];
```

3. Example console configuration:

```php
return [
    'modules' => [
        'system' => [
            'class' => 'exoo\system\Module',
            'controllerNamespace' => 'exoo\system\controllers\console',
        ]
    ],
];
```



4. Run the migrations

```
yii migrate --migrationPath=@yii/log/migrations/
yii migrate --migrationPath=@yii/rbac/migrations
yii migrate --migrationPath=@exoo/system/migrations
```

5. Init rbac
```
yii system/rbac/init
```

6. Role assignment for super admin
```
yii system/user/assign-role admin 1
```
