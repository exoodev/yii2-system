<?php

namespace exoo\system\controllers\frontend;

use Yii;
use exoo\system\components\AuthHandler;
use exoo\system\models\SystemSettings;
use exoo\system\models\forms\LoginForm;
use exoo\system\models\frontend\form\SignupForm;
use exoo\system\models\frontend\form\AddEmailForm;
use exoo\system\models\frontend\form\EmailConfirmForm;
use exoo\system\models\frontend\form\ResendEmailConfirmForm;
use exoo\system\models\frontend\form\PasswordResetRequestForm;
use exoo\system\models\frontend\form\ResetPasswordForm;
use yii\web\Response;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\InvalidParamException;
use exoo\uikit\ActiveForm;

/**
 * User controller
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $registration = Yii::$app->settings->get('system', 'registration') != SystemSettings::REGISTRATION_DISABLED;
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'logout',
                    'signup',
                    'resend-activation',
                    'request-password-reset',
                    'add-email',
                ],
                'rules' => [
                    [
                        'actions' => [
                            'signup',
                            'resend-activation',
                            'request-password-reset',
                            'add-email',
                        ],
                        'allow' => $registration,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($user = $model->signup()) {
                if (Yii::$app->settings->get('system', 'emailConfirm')) {
                    $user->sendMailConfirm();
                    return $this->redirect(['activation', 'email' => $model->email]);
                }
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Activation page
     *
     * @param string $email
     * @return mixed
     */
    public function actionActivation($email = false)
    {
        return $this->render('activation', [
            'email' => $email,
        ]);
    }

    /**
     * Resend email confirm token page.
     */
    public function actionResendActivation($email = false)
    {
        $model = new ResendEmailConfirmForm();
        $model->email = $email;

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->resend()) {
                return $this->redirect(['activation', 'email' => $model->email]);
            } else {
                return $this->refresh();
            }
        }
        return $this->render('resendActivation', [
            'model' => $model,
        ]);
    }

    /**
     * Add e-mail.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionAddEmail($token)
    {
        try {
            $model = new AddEmailForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->addEmail()) {
                if (Yii::$app->settings->get('system', 'emailConfirm')) {
                    $user->sendMailConfirm();
                    return $this->redirect(['activation', 'email' => $model->email]);
                }
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('addEmail', [
            'model' => $model,
        ]);
    }

    /**
     * Confirm e-mail.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionEmailConfirm($token)
    {
        try {
            $model = new EmailConfirmForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->confirmEmail()) {
            Yii::$app->getSession()->setFlash('alert.success', Yii::t('system', 'Your e-mail successfully confirmed.'));
        } else {
            Yii::$app->getSession()->setFlash('alert.error', Yii::t('system', 'Error confirmation e-mail.'));
        }

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->sendEmail();
            Yii::$app->session->setFlash('alert.success', Yii::t('system', 'Check your e-mail for further instructions.'));
            return $this->redirect(['login']);
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
            Yii::$app->session->setFlash('alert.success', Yii::t('system', 'New password saved.'));

            return $this->redirect(['login']);
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
