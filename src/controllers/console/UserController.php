<?php
/**
* @copyright Copyright &copy; ExooDev, exoodev.com, 2016
* @version 1.0.0
*/

namespace exoo\system\controllers\console;

use Yii;
use yii\console\Controller;
use exoo\system\models\User;

/**
* Manages user roles.
*
* For example,
*
* ```php
* yii system/user/assign-role admin 1
* ```
*
* @author ExooDev <info@exoodev.com>
* @since 1.0
*/
class UserController extends Controller
{
    /**
     * Assign role to user.
     *
     * @param string $role role name.
     * @param integer $userId user id.
     */
    public function actionAssignRole($roleName, $userId)
    {
        $auth = Yii::$app->authManager;
        $role = $this->getRole($roleName, $userId);
        if (array_key_exists($role->name, $auth->getRolesByUser($userId))) {
            exit("The role $roleName is already assigned to user $userId");
        }
        $auth->assign($role, $userId);
        echo "Role $roleName assigned to user $userId";
    }

    /**
     * Revoke the user role.
     *
     * @param string $role role name.
     * @param integer $userId user id.
     */
    public function actionRevokeRole($roleName, $userId)
    {
        $auth = Yii::$app->authManager;
        $role = $this->getRole($roleName, $userId);
        if (!array_key_exists($role->name, $auth->getRolesByUser($userId))) {
            exit("Role $roleName not assigned to user $userId");
        }
        if ($auth->revoke($role, $userId)) {
            echo "Role $roleName revoked from user $userId";
        }
    }

    /**
     * Displays user roles.
     * @param integer $userId user id.
     */
    public function actionRoles($userId)
    {
        $user = User::findOne($userId);
        if (!$user) {
            exit("User $userId not exists");
        }
        $roles = $user->roles;
        if ($roles) {
            echo implode(', ', $roles);
        } else {
            echo "User has not roles";
        }
    }

    /**
     * Get role
     *
     * @param string $roleName
     * @param integer $userId
     * @return string the result
     */
    protected function getRole($roleName, $userId)
    {
        if (User::findOne($userId) === null) {
            exit("User $userId not exists");
        }
        if (($role = Yii::$app->authManager->getRole($roleName)) === null) {
            exit("Role $roleName not exists");
        }
        return $role;
    }
}
