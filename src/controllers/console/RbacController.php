<?php

namespace exoo\system\controllers\console;

use Yii;
use exoo\system\components\BaseRbacController;

/**
* Manages rbac roles and permissions of module system.
*
* For example,
*
* ```php
* yii system/rbac/init
*```
*/
class RbacController extends BaseRbacController
{
    /**
     * Add roles, permissions and rules of module System to the RBAC system.
     */
    public function actionInit()
    {
        $auth = Yii::$app->getAuthManager();
        $data = [
            'app' => 'System',
            'translations' => 'system',
        ];

        /**
         * Permission to enter the backend
         */
        $adminIn = $auth->createPermission('system.backendIn');
        $adminIn->description = 'Entrance to the backend';
        $adminIn->data = $data;
        $this->updatePermission(['adminIn', 'system.backendIn'], $adminIn);

        /**
         * Permission to edit system languages.
         */
        $editLanguages = $auth->createPermission('system.editLanguages');
        $editLanguages->description = 'Manage languages';
        $editLanguages->data = $data;
        $this->updatePermission('system.editLanguages', $editLanguages);

        /**
         * Permission to manage pages in backend
         */
        $editPages = $auth->createPermission('system.editBlocks');
        $editPages->description = 'Manage blocks';
        $editPages->data = $data;
        $this->updatePermission(['system.editBlocks', 'system.editPages'], $editPages);

        /**
         * Permission to edit rbac roles
         */
        $editRoles = $auth->createPermission('system.editRoles');
        $editRoles->description = 'Manage roles';
        $editRoles->data = $data;
        $this->updatePermission(['editRoles', 'system.editRoles'], $editRoles);

        /**
         * Permission to edit Rbac system and Users
         */
        $editUsers = $auth->createPermission('system.editUsers');
        $editUsers->description = 'Manage users';
        $editUsers->data = $data;
        $this->updatePermission(['editUsers', 'system.editUsers'], $editUsers);

        /**
         * Permission to edit system settings
         */
        $editSettings = $auth->createPermission('system.editSettings');
        $editSettings->description = 'Manage system settings';
        $editSettings->data = $data;
        $this->updatePermission(['editSettings', 'system.editSettings'], $editSettings);

        /**
         * Base system role "Registered"
         */
        $registered = $auth->createRole('Registered');
        $registered->description = 'Registered';
        $this->updateRole('Registered', $registered);

        /**
         * Role 'Admin'
         */
        $admin = $auth->createRole('Admin');
        $admin->description = 'Administrator';
        $this->updateRole('Admin', $admin);
        $this->addChild($admin, $adminIn);
        $this->addChild($admin, $editPages);
        $this->addChild($admin, $editUsers);
        $this->addChild($admin, $editRoles);
        $this->addChild($admin, $editSettings);

        echo 'All rbac items of module system are initialized.';
    }

    /**
     * Remove roles, permissions and rules of module System from the RBAC system.
     */
    public function actionRemoveAll()
    {
        $this->removePermission('system.backendIn');
        $this->removePermission('system.editPages');
        $this->removePermission('system.editRoles');
        $this->removePermission('system.editUsers');
        $this->removePermission('system.editSettings');
        $this->removeRole('Registered');
        $this->removeRole('Admin');

        echo 'All rbac items of module system are removed.';
    }
}
