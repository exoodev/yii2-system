<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\controllers\backend;

use Yii;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\helpers\FileHelper;
use exoo\system\models\SystemSettings;
use exoo\system\models\CompanySettings;
use exoo\system\models\forms\LoginForm;
use exoo\settings\actions\SettingsAction;
use yii2tech\sitemap\File;

/**
 * Manages system settings.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                    'settings' => ['get', 'post'],
                    'company' => ['get', 'post'],
                    'disable-temp-files' => ['post'],
                    'delete-temp-files' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
            'settings' => [
                'class' => SettingsAction::class,
                'modelClass' => SystemSettings::class,
            ],
            'company' => [
                'class' => SettingsAction::class,
                'modelClass' => CompanySettings::class,
                'group' => 'company',
            ],
        ];
    }

    /**
     * Displayes main page.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displayes main page.
     * @return mixed
     */
    public function actionGit()
    {
        return $this->render('git');
    }

    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = '@exoo/system/themes/master/views/layouts/login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm(['scenario' => LoginForm::SCENARIO_BACKEND_IN]);
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Clear cache
     * @return string
     */
    public function actionClearCache($type)
    {
        if ($type == 'settings') {
            $result = Yii::$app->settings->cacheDelete();
        } elseif ($type == 'flush') {
            $result = Yii::$app->cache->flush();
        }

        if ($result) {
            return $this->asJson([
                'result' => true,
                'notification' => Yii::t('system', 'Cache cleared.')
            ]);
        }
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return string the result
     */
    public function actionSitemap()
    {
        $siteMapFile = new File();

        $siteMapFile->writeUrl(['site/index'], ['priority' => '0.9']);
        $siteMapFile->writeUrl(['site/about'], ['priority' => '0.8', 'changeFrequency' => File::CHECK_FREQUENCY_WEEKLY]);
        $siteMapFile->writeUrl(['site/signup'], ['priority' => '0.7', 'lastModified' => '2015-05-07']);
        $siteMapFile->writeUrl(['site/contact']);

        $siteMapFile->close();
    }
}
