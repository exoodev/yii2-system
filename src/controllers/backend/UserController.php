<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\controllers\backend;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use exoo\system\models\Profile;
use exoo\system\models\backend\User;
use exoo\system\models\backend\search\UserSearch;

/**
 * Manages users.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['system.editUsers'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'update' => ['get', 'post'],
                    'delete' => ['post'],
                    'batch-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $statuses = User::getStatuses();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statuses' => $statuses,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $user = $this->findUser($id);
        $profile = $user->profile;
        return $this->render('view', [
            'user' => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * Creates a new User model.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = new User(['scenario' => User::SCENARIO_CREATE]);
        $profile = new Profile();
        $statuses = User::getStatuses();
        $rolesList = Yii::$app->authManager->getRoles();

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            $profile->fileAvatar = UploadedFile::getInstance($profile, 'fileAvatar');
            if ($user->validate() && $profile->validate()) {
                if ($user->save(false)) {
                    $profile->uploadAvatar();
                    $user->link('profile', $profile);
                    Yii::$app->session->setFlash('notify.success', Yii::t('system', 'User created.'));
                    return $this->redirect(['update', 'id' => $user->id]);
                }
            }
        }
        return $this->render('create', [
            'user' => $user,
            'rolesList' => $rolesList,
            'statuses' => $statuses,
            'profile' => $profile,
        ]);
    }

    /**
     * Updates an existing User model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->findUser($id, 'profile');
        $profile = $user->profile;
        $statuses = User::getStatuses();
        $rolesList = Yii::$app->authManager->getRoles();

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            $profile->fileAvatar = UploadedFile::getInstance($profile, 'fileAvatar');
            if ($user->validate() && $profile->validate()) {
                if ($user->save(false)) {
                    $profile->uploadAvatar();
                    $user->link('profile', $profile);
                    Yii::$app->session->setFlash('notify.success', Yii::t('system', 'User saved.'));
                    return $this->refresh();
                }
            }
        }
        return $this->render('update', [
            'user' => $user,
            'rolesList' => $rolesList,
            'statuses' => $statuses,
            'profile' => $profile,
        ]);
    }

    /**
     * Deletes an existing User model.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findUser($id);
        $model->status_id = User::STATUS_DELETED;
        if ($model->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('system', 'User deleted'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Delete multiple posts page.
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            $query = User::updateAll(['status_id' => User::STATUS_DELETED], ['id' => $ids]);
            if ($query) {
                Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Deleted users {0,number}.', $query));
            }
            return $this->redirect(['index']);
        } else {
            throw new BadRequestHttpException(400);
        }
    }

    /**
     * Avatar delete.
     * @param integer $id the user id
     * @return mixed
     */
    public function actionAvatarDelete($id)
    {
        $profile = Profile::find()->where(['user_id' => $id])->one();
        if ($profile) {
            $profile->avatar_url = '';
            if ($profile->save()) {
                Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Avatar deleted'));
            }
        } else {
            throw new NotFoundHttpException('User not found.');
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Settings action.
     * @return string
     */
    public function actionSettings()
    {
        $model = UserSetting::find();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Settings updated.'));
            return $this->refresh();
        } else {
            return $this->render('settings', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUser($id, $with = false)
    {
        $query = User::find()->where(['id' => $id]);
        if ($with) {
            $query->with($with);
        }
        $model = is_array($id) ? $query->all() : $query->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('User not found.');
        }
    }
}
