<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\controllers\backend;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use exoo\system\models\backend\Rbac;

/**
 * Manages roles and permissions.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class RoleController extends Controller
{
	/**
     * @inheritdoc
     */
	public function behaviors()
	{
		return [
			'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['system.editRoles'],
                    ],
                ],
            ],
			'verbs' => [
				'class' => 'yii\filters\VerbFilter',
				'actions' => [
					'index' => ['get'],
					'create' => ['get', 'post'],
					'update' => ['get', 'post'],
					'delete' => ['post'],
					'permission' => ['post'],
				],
			],
		];
	}

	/**
     * Displays main page.
     * @return mixed
	 * @throws NotFoundHttpException If role does not exists.
     */
	public function actionIndex($name = false)
	{
		$rbac = Rbac::find($name);
		if (is_null($rbac)) {
			throw new NotFoundHttpException(Yii::t('system', 'Role {role} not found.', ['role' => $name]));
		}
		$permissions = Yii::$app->authManager->getPermissions();
		return $this->render('index', [
			'rbac' => $rbac,
			'permissions' => $permissions,
		]);
	}

	/**
     * Creates a new role.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
	public function actionCreate()
	{
		$rbac = new Rbac();
		if ($rbac->load(Yii::$app->request->post()) && $rbac->save()) {
			Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Role created'));
			return $this->redirect(['index', 'name' => $rbac->name ]);
		}
		return $this->render('create', [
			'rbac' => $rbac,
		]);
	}

	/**
     * Updates an existing role.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param string $name Role name.
     * @return mixed
	 * @throws NotFoundHttpException If role does not exists.
     */
	public function actionUpdate($name)
	{
		$rbac = Rbac::find($name);
		if (is_null($rbac)) {
			throw new NotFoundHttpException(Yii::t('system', 'Role {role} not found.', ['role' => $name]));
		}
		if ($rbac->load(Yii::$app->request->post()) && $rbac->update()) {
			Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Role updated'));
			return $this->redirect(['index', 'name' => $rbac->name]);
		}
		return $this->render('update', ['rbac' => $rbac]);
	}

	/**
     * Deletes an existing role.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $name Role name.
     * @return mixed
	 * @throws NotFoundHttpException If role does not exists.
     */
	public function actionDelete($name)
	{
		$role = $this->findRole($name);
		if ($role->name !== Rbac::ADMIN_ROLE && $role->name !== Rbac::DEFAULT_ROLE && Yii::$app->authManager->remove($role)) {
			Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Role deleted'));
		} else {
			$message = Yii::t('system', 'Can\'t delete role {role} ', ['role' => Rbac::DEFAULT_ADMIN_ROLE]);
			Yii::$app->session->setFlash('notify.danger', $message);
		}
		return $this->redirect(['index']);
	}

	public function actionPermission()
	{
		if (($request = Yii::$app->request->post()) !== null) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$auth = Yii::$app->authManager;

			$role = $this->findRole($request['role']);
			$permission = $this->findPermission($request['permission']);
			$checked = (boolean) $request['checked'];

			if (!$auth->hasChild($role, $permission) && $checked) {
				if ($auth->addChild($role, $permission)) {
					return [
						'status' => 'success',
						'message' => Yii::t('system', 'Permission added'),
					];
				}
			} elseif ($auth->hasChild($role, $permission) && !$checked) {
				if ($auth->removeChild($role, $permission)) {
					return [
						'status' => 'success',
						'message' => Yii::t('system', 'Permission disabled'),
					];
				}
			}
		}
	}

	/**
	 * Changes inheritance of the roles.
	 * @return mixed
	 */
	public function actionNestable()
	{
	    if (($roles = Yii::$app->request->post('roles')) !== null) {
			if ($this->nestableRoles($roles)) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return [
					'status' => 'success',
					'message' => Yii::t('system', 'Role is changed'),
				];
			}
	    }
	}

	/**
	 * @return \yii\rbac\Item Role
	 */
	protected function findRole($name)
	{
		if (($model = Yii::$app->authManager->getRole($name)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException(Yii::t('system', 'Role {role} not found.', ['role' => $name]));
		}
	}

	/**
	 * @return \yii\rbac\Item Permission
	 */
	protected function findPermission($name)
	{
		if (($model = Yii::$app->authManager->getPermission($name)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('Permission not found.');
		}
	}

	/**
	 * Recursive adding and removing child role
	 * @param array $items
	 * @param \yii\rbac\Item Rbac role
	 * @return boolean
	 */
	protected function nestableRoles($items = [], $childRole = null)
 	{
 		$auth = Yii::$app->authManager;
 		foreach ($items as $item) {
 			$role = $auth->getRole($item['id']);
 			$oldChild = Rbac::getChildByRole($item['id']);
 			if ($oldChild != $childRole) {
 				if (!is_null($oldChild)) {
 					$auth->removeChild($role, $oldChild);
 				}
 				if (!is_null($childRole)) {
 					$auth->addChild($role, $childRole);
 				}
				return true;
 			}
 			if (isset($item['children'])) {
 				if (self::nestableRoles($item['children'], $role)) {
 					return true;
 				}
 			}
 		}
 		return false;
 	}
}
