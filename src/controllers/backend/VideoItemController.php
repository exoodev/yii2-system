<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\controllers\backend;

use Yii;
use yii\helpers\Html;
use yii\base\Model;
use exoo\system\models\Video;
use exoo\system\models\backend\search\VideoSearch;
use exoo\system\models\VideoFolder;
use exoo\status\StatusAction;
use exoo\position\PositionAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;

/**
 * Manages videos.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class VideoItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'index' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'post'],
                    'batchDelete' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'status' => [
                'class' => StatusAction::class,
                'modelClass' => Video::class,
            ],
            'position' => [
                'class' => PositionAction::class,
                'modelClass' => Video::class,
            ],
        ];
    }

    /**
     * Lists all [[Video]] models.
     * @return mixed
     */
    public function actionIndex()
    {
        $video = new Video();
        $searchModel = new VideoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'video' => $video,
        ]);
    }

    /**
     * Creates a new [[Video]] model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate($folder_id)
    {
        $model = new Video();
        $model->folder_id = $folder_id;
        $post = Yii::$app->request->post();

        if ($model->load($post) && Model::loadMultiple($model->getVariationModels(), $post)) {
            if ($model->save()) {
                Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Saved'));
                return $this->redirect(['update', 'id' => $model->id]);
            } elseif (Yii::$app->request->isAjax) {
                return $this->validationForm($model);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing [[Video]] model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Video id.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        if ($model->load($post) && Model::loadMultiple($model->getVariationModels(), $post)) {
            if ($model->save() && Yii::$app->request->isAjax) {
                return $this->asJson(['success' => Yii::t('system', 'Saved')]);
            } else {
                return $this->validationForm($model);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Validation form.
     */
    protected function validationForm($model)
    {
        $result = [];
        foreach ($model->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($model, $attribute)] = $errors;
        }

        return $this->asJson(['validation' => $result]);
    }

    /**
     * Deletes an existing [[Video]] model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Video id.
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Deleted'));
        }

        return $this->goBack();
    }

    /**
     * Deletes multiple an existing [[Video]] models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            $models = Video::findAll($ids);
            foreach ($models as $model) {
                $model->delete();
            }
            Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Deleted'));

            return $this->goBack();
        } else {
            throw new BadRequestHttpException(400);
        }
    }

    /**
     * Finds the [[Video]] model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id Video id.
     * @return Video The loaded model.
     * @throws NotFoundHttpException If the model is not found.
     */
    protected function findModel($id)
    {
        if (($model = Video::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('system', 'The requested page does not exist.'));
        }
    }
}
