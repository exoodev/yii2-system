<?php

/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @version 1.0.0
 */

namespace exoo\system\controllers\backend;

use Yii;
use yii\web\Controller;

/**
 * Documentation controller.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class DocsController extends Controller
{
    /**
     * Default layout for cms documentation
     * @var string
     */
    public $layout = 'docsLayout';

    /**
     * List of all docs
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect('about');
    }

    /**
     * List of all docs
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * How to install application
     *
     * @return mixed
     */
    public function actionInstall()
    {
        return $this->render('install');
    }

    /**
     * How to update application
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        return $this->render('update');
    }

    /**
     * How to configurate Rbac
     *
     * @return mixed
     */
    public function actionRbac()
    {
        return $this->render('rbac');
    }

    /**
     * How to manage users
     *
     * @return mixed
     */
    public function actionUsers()
    {
        return $this->render('users');
    }
}
