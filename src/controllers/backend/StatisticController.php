<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2017
 * @package
 * @version 1.0.0
 */

namespace exoo\system\controllers\backend;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\httpclient\Client;
use yii\base\InvalidConfigException;

/**
 * Statistics Yandex.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class StatisticController extends Controller
{
    /**
     * @var string
     */
    public $ymId;
    /**
     * @var string
     */
    public $ymToken;

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->ymId = Yii::$app->settings->get('system', 'ymId');
        $this->ymToken = Yii::$app->settings->get('system', 'ymOAuthToken');

        if (!$this->ymId || !$this->ymToken) {
            throw new InvalidConfigException("The 'ymId' and 'ymToken' settings are required.");
        }
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'data' => ['post'],
                    'counter' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Request for data
     * @return array the json result
     */
    public function actionData()
    {
        if (Yii::$app->request->isAjax) {
            $url ='https://api-metrika.yandex.net/stat/v1/data';
            $params = Yii::$app->request->post();
            $period = ArrayHelper::remove($params, 'period', 'week');
            Yii::$app->session->set('stat.period', $period);

            if ($mode = ArrayHelper::remove($params, 'mode', '')) {
                $url = $url . '/' . $mode;
            }

            $params = array_merge([
                'ids' => $this->ymId,
                // 'oauth_token' => $this->ymToken,
                'date1' => $period . 'daysAgo',
            ], $params);

            return $this->asJson($this->request($url, $params));
        }
    }

    /**
     * Requesting Counter Data
     * @return array the json result
     */
    public function actionCounter()
    {
        if (Yii::$app->request->isAjax) {
            $url = 'https://api-metrika.yandex.net/management/v1/counter/' . $this->ymId;
            $params = Yii::$app->request->post();

            $params = array_merge($params, [
                // 'oauth_token' => $this->ymToken,
            ]);

            return $this->asJson($this->request($url, $params));
        }
    }

    /**
     * Render dashboard
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'id' => $this->ymId
        ]);
    }

    /**
     * Request to API Yandex
     * @param string $url
     * @param array $data
     * @return array the json result
     */
    protected function request($url, $data = false)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setUrl($url)
            ->setData($data)
            ->addHeaders([
                'content-type' => 'application/x-yametrika+json',
                'authorization' => 'OAuth ' . $this->ymToken,
            ])
            ->send();

        return $response->data;
    }
}
