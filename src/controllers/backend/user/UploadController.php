<?php

namespace exoo\system\controllers\backend\user;

use Yii;
use yii\rest\Controller;
use yii\web\UploadedFile;
use exoo\system\models\user\Upload;

/**
 *
 */
class UploadController extends Controller
{
    public function actionFile()
    {
        $model = new Upload();
        $model->file = UploadedFile::getInstance($model, 'file');
        return $model->upload();
    }
}
