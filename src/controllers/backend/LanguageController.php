<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\controllers\backend;

use Yii;
use yii\helpers\ArrayHelper;
use exoo\system\models\Language;
use exoo\system\models\backend\search\LanguageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;

/**
 * Manages application languages.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class LanguageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['system.editLanguages'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'index' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'post'],
                    'delete' => ['post'],
                    'position' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'position' => [
                'class' => 'exoo\system\actions\PositionAction',
                'modelClass' => 'exoo\system\models\Language',
                'afterMove' => function() {
                    Yii::$app->locale->cacheDelete();
                }
            ],
        ];
    }

    /**
     * Lists all [[Language]] models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new [[Language]] model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Language();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->locale->cacheDelete();
            Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Language added'));
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing [[Language]] model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Language id.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->locale->cacheDelete();
            Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Language updated'));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing [[Language]] model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Language id.
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->locale->cacheDelete();
            Yii::$app->session->setFlash('notify.success', Yii::t('system', 'Language deleted'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the [[Language]] model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id Language id.
     * @return Language The loaded model.
     * @throws NotFoundHttpException If the model is not found.
     */
    protected function findModel($id)
    {
        if (($model = Language::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('system', 'The requested page does not exist.'));
        }
    }
}
