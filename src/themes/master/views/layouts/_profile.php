<?php

use yii\helpers\Html;

$user = Yii::$app->user;
?>

<?= Html::img($user->identity->profile->avatarUrl, [
    'class' => 'uk-border-round uk-margin-small-right tm-avatar',
    'height' => 35,
    'width' => 35,
]) ?>
<span><?= Html::encode($user->identity->username) ?></span>
<div class="uk-margin">
    <?= Html::a(
        '<i uk-icon="icon: cog"></i>',
        ['/system/user/view', 'id' => $user->identity->id],
        [
            'class' => 'uk-icon-button',
            'uk-tooltip' => Yii::t('system', 'Profile')
        ]
    ) ?>
    <?= Html::a(
        '<i uk-icon="icon: sign-out"></i>',
        ['/system/site/logout'],
        [
            'class' => 'uk-icon-button',
            'uk-tooltip' => Yii::t('system', 'Logout'),
            'data-method' => 'post'
        ]
    ) ?>
</div>
