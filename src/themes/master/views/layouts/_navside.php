<?php

use exoo\uikit\Nav;
use exoo\system\models\backend\Log;

$user = Yii::$app->user;
$profile = $this->render('_profile');
$menuItems = [
    [
        'icon' => '<i class="fas fa-table fa-lg fa-fw uk-margin-small-right"></i>',
        'label' => Yii::t('system', 'Dashboard'),
        'url' => ['/dashboard/index'],
    ],
    [
        'icon' => '<i class="fas fa-chart-bar fa-lg fa-fw uk-margin-small-right"></i>',
        'label' => Yii::t('system', 'Statistics'),
        'url' => ['/system/statistic/index'],
        'visible' => Yii::$app->settings->get('system', 'ymId') ? true : false
    ],
    [
        'icon' => '<i class="far fa-window-restore fa-lg fa-fw uk-margin-small-right"></i>',
        'label' => Yii::t('system', 'Blocks'),
        'url' => ['/system/block/index'],
    ],
    [
        'icon' => '<i class="fas fa-building fa-lg fa-fw uk-margin-small-right"></i>',
        'label' => Yii::t('system', 'Company'),
        'url' => ['/system/site/company'],
    ],
];

if ($extensions = Yii::$app->menu->getItems('navside')) {
    $menuItems[] = ['divider' => true];
    $menuItems[] = [
        'icon' => '<i class="fab fa-youtube fa-lg fa-fw uk-margin-small-right"></i>',
        'label' => Yii::t('system', 'Video'),
        'url' => ['/system/video/index'],
    ];
    $menuItems = array_merge($menuItems, $extensions);
}

$menuItems[] = ['divider' => true];
$menuItems['system'] = [
    'icon' => '<i class="fas fa-cog fa-lg fa-fw uk-margin-small-right"></i>',
    'label' => Yii::t('system', 'System'),
    'items' => [
        [
            'label' => Yii::t('system', 'Settings'),
            'url' => ['/system/site/settings'],
            'visible' => $user->can('system.editSettings'),
        ],
        [
            'label' => Yii::t('system', 'Users'),
            'url' => ['/system/user/index'],
            'visible' => $user->can('system.editUsers'),
        ],
        // [
        //     'label' => Yii::t('system', 'Roles'),
        //     'url' => ['/system/role/index'],
        //     'visible' => $user->can('system.editRoles'),
        // ],
        [
            'label' => Yii::t('system', 'Content languages'),
            'url' => ['/system/language/index'],
            'visible' => $user->can('system.editLanguages'),
        ],
        [
            'label' => Yii::t('system', 'Log'),
            'url' => ['/system/log/index'],
            'visible' => !is_null(Yii::$app->db->schema->getTableSchema(Log::tableName()))
        ],
        [
            'label' => Yii::t('system', 'DB log'),
            'url' => ['/system/db-log/index'],
        ],
        // [
        //     'label' => 'Exookit',
        //     'url' => ['/kit/docs/core'],
        //     'linkOptions' => ['target' => '_blank'],
        // ],
    ],
];
?>

<div class="tm-navside-left uk-background-secondary uk-light uk-visible@l">
    <div class="uk-card uk-card-primary uk-card-body uk-card-small">
        <?= $profile ?>
    </div>
    <div class="uk-card uk-card-body uk-card-small">
        <?= Nav::widget([
            'items' => $menuItems,
            'type' => 'navside',
            'encodeLabels' => false,
        ]) ?>
    </div>
    <div class="tm-navside-footer uk-card uk-card-body uk-card-small uk-width-1-6@m">
        <div class="uk-text-muted uk-text-small">
            <span>ExooCMF <?= Yii::$app->system->version ?></span>
            <span class="ex-text-grey-800"> | </span>
            <span>Yii <?= Yii::getVersion() ?></span>
        </div>
    </div>
</div>

<div id="offcanvas-menu" uk-offcanvas>
    <div class="uk-offcanvas-bar">
        <?= $profile ?>
        <hr>
        <?= Nav::widget([
            'items' => $menuItems,
            'type' => 'offcanvas',
        ]) ?>
    </div>
</div>
