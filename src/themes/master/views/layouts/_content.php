<?php

use exoo\uikit\Message;
use exoo\uikit\Nav;
use yii\widgets\Breadcrumbs;

?>

<div class="tm-main uk-section uk-section-xsmall">
    <div class="uk-container uk-container-expand">
        <?php if (isset($this->blocks['top-a'])): ?>
            <div class="tm-top-a">
                <div class="uk-container">
                    <?= $this->blocks['top-a'] ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (isset($this->blocks['top-b-left']) || isset($this->blocks['top-b-right'])): ?>
            <div class="tm-top-b">
                <div class="uk-container">
                    <div >
                        <div class="uk-width-1-2@m">
                            <?php if (isset($this->blocks['top-b-left'])) {
                                echo $this->blocks['top-b-left'];
                            } ?>
                        </div>
                        <div class="uk-width-1-2@m">
                            <?php if (isset($this->blocks['top-b-right'])) {
                                echo $this->blocks['top-b-right'];
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="tm-content">
            <?php if ($subnav = Yii::$app->menu->getItemsCurrentModule('subnav')): ?>
                <div class="tm-subnav">
                    <?= Nav::widget([
                        'items' => $subnav,
                        'type' => 'subnav',
                        'options' => ['class' => 'uk-subnav-pill'],
                    ]) ?>
                </div>
            <?php endif; ?>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'options' => ['class' => 'uk-breadcrumb'],
                'homeLink' => false
            ]) ?>
            <?= Message::widget() ?>
            <?= $content ?>
        </div>

        <?php if (isset($this->blocks['bottom-a'])): ?>
            <div class="tm-bottom-a">
                <div class="uk-container">
                    <?= $this->blocks['bottom-a'] ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (isset($this->blocks['bottom-b'])): ?>
            <div class="tm-bottom-b">
                <div class="uk-container">
                    <?= $this->blocks['bottom-b'] ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
