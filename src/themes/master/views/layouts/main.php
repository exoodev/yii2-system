<?php

/* @var $this \yii\web\View */
/* @var $content string */

use exoo\system\themes\master\ThemeAsset;

ThemeAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="uk-height-1-1">
    <?= $this->render('_head') ?>
    <body>
        <?php $this->beginBody() ?>
        <div class="uk-offcanvas-content">
            <div class="uk-grid-collapse" uk-grid>
                <div class="uk-width-1-6@l">
                    <?= $this->render('_navside') ?>
                </div>
                <div class="uk-width-expand@l">
                    <div uk-height-viewport="offset-bottom: 40px">
                        <?= $this->render('_navbar') ?>
                        <?= $this->render('_content', ['content' => $content]) ?>
                    </div>
                    <?= $this->render('_footer') ?>
                </div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
