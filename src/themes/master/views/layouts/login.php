<?php

use exoo\system\themes\master\ThemeAsset;

ThemeAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="uk-height-1-1">
	<?= $this->render('_head') ?>
	<body class="uk-height-1-1">
	<?php $this->beginBody() ?>
		<?= $content ?>
	<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>
