<?php

use yii\helpers\Html;

?>

<div class="tm-navbar uk-navbar-container uk-margin uk-background-primary uk-dark uk-box-shadow-small" uk-sticky="media: 960">
    <div class="uk-container uk-container-expand">
        <nav uk-navbar>

            <div class="uk-navbar-left">
                <a class="uk-navbar-item uk-logo" href="http://exoodev.com" target="_blank">
                    <?php if ($company = Yii::$app->settings->get('company', 'name')): ?>
                        <span><?= Html::encode($company) ?></span>
                    <?php else: ?>
                        <span>ExooCMF</span>
                    <?php endif; ?>
                </a>
            </div>

            <div class="uk-navbar-right">
                <?php if (isset($this->blocks['notifications'])): ?>
                    <div class="tm-notifications">
                        <?= $this->blocks['notifications'] ?>
                    </div>
                <?php endif; ?>
                <div class="uk-navbar-item uk-visible@s">
                    <?= Html::a(
                        '<i class="fas fa-external-link-alt uk-margin-small-right"></i>' .
                        Yii::t('system', 'Visit website'),
                        Yii::$app->settings->get('system', 'frontendUrl'),
                        [
                            'target' => '_blank',
                            'class' => 'uk-link-heading'
                        ]
                    ) ?>
                </div>
                <a class="uk-navbar-toggle uk-hidden@l" uk-navbar-toggle-icon uk-toggle="target: #offcanvas-menu"></a>
            </div>

        </nav>
    </div>
</div>
