<?php

namespace exoo\system\themes\master;

use yii\web\AssetBundle;

class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@exoo/system/themes/master/assets';

    public $css = [
        'css/theme.css',
    ];
    public $js = [
        'js/theme.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'exoo\uikit\UikitAsset',
        'exoo\kit\ExookitAsset',
        'exoo\fontawesome\FontawesomeAsset',
    ];
}
