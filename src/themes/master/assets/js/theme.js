jQuery(document).ready(function () {

    // Stiky subnav
    var element = '.tm-sticky-subnav';
    // var subnav = '<div class="tm-subnav uk-section uk-section-default uk-section-xsmall" uk-sticky="offset: 60"></div>';

    if ($(element).length) {
        var $navbar = $('.tm-navbar');
        var heightNavbar = $navbar.height();
        var subnav = UIkit.sticky(element, {offset: heightNavbar + 10, media: 960});
        var $subnav = $(subnav.$el);

        UIkit.util.on(subnav.$el, 'active', function() {
            $navbar.height(heightNavbar + 50);
            UIkit.update($navbar, event = 'update');
            $subnav.find('.uk-button').addClass('uk-button-small');
        })
        UIkit.util.on(subnav.$el, 'inactive', function() {
            $navbar.height(heightNavbar)
            UIkit.update($navbar, event = 'update');
            $subnav.find('.uk-button').removeClass('uk-button-small');
        });
    }
});
