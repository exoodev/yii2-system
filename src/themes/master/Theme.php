<?php

namespace exoo\system\themes\master;

class Theme extends \yii\base\Theme
{
    public $pathMap = [
        '@app/views' => '@exoo/system/themes/master/views',
        '@app/modules' => '@exoo/system/themes/master/modules',
    ];

    public $basePath = '@exoo/system/themes/master';
}
