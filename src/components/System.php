<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Component;
use exoo\system\models\SystemSettings;
use exoo\system\components\Locale;

/**
 * System class.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class System extends Component
{
    /**
     * @var string
     */
    private $_version;

    /**
     * Get version CMS
     * @return string
     */
    public function getVersion()
    {
        if ($this->_version === null) {
            $this->_version = ArrayHelper::getValue(Yii::$app->extensions, 'exoo/system.version');
        }

        return $this->_version;
    }

    /**
     * Returns a value indicating whether the registration is disabled.
     * @return boolean Whether the registration is disabled.
     */
    public function getIsRegistrationDisabled()
    {
        return Yii::$app->settings->get('system', 'registration') == SystemSettings::REGISTRATION_DISABLED;
    }

    /**
     * Returns a value indicating whether the registration is enabled.
     * @return boolean Whether the registration is enabled.
     */
    public function getIsRegistrationEnabled()
    {
        return Yii::$app->settings->get('system', 'registration') == SystemSettings::REGISTRATION_ENABLED;
    }

    /**
     * Returns a value indicating whether the registration is approval.
     * @return boolean Whether the registration is approval.
     */
    public function getIsRegistrationApproval()
    {
        return Yii::$app->settings->get('system', 'registration') == SystemSettings::REGISTRATION_APPROVAL;
    }

    /**
     * Get regions
     * @return array the result
     */
    public function getRegions()
    {
        return [
            'Africa' => \DateTimeZone::AFRICA,
            'America' => \DateTimeZone::AMERICA,
            'Antarctica' => \DateTimeZone::ANTARCTICA,
            'Aisa' => \DateTimeZone::ASIA,
            'Atlantic' => \DateTimeZone::ATLANTIC,
            'Europe' => \DateTimeZone::EUROPE,
            'Indian' => \DateTimeZone::INDIAN,
            'Pacific' => \DateTimeZone::PACIFIC,
        ];
    }

    /**
     * Get timezones
     * @return array the result
     */
    public function getTimezones()
    {
        $timezones = [];
        foreach ($this->regions as $name => $mask) {
            $zones = \DateTimeZone::listIdentifiers($mask);
            foreach($zones as $timezone) {
                $timezones[$name][$timezone] = $this->getTimeForTimezone($name, $timezone);
        	}
        }

        return $timezones;
    }

    /**
     * Get timezones for optgroup (Select2)
     * @return array the result
     */
    public function getTimezonesGroup()
    {
        $timezones = [];
        foreach ($this->regions as $name => $mask) {
            $zones = \DateTimeZone::listIdentifiers($mask);
            $group = ['text' => $name];
            foreach($zones as $timezone) {
                $group['children'][] = [
                    'id' => $timezone,
                    'text' => $this->getTimeForTimezone($name, $timezone),
                ];
        	}
            $timezones[] = $group;
        }

        return $timezones;
    }

    /**
     * Get time in format 18:00 (6:00pm)
     * @param string $value
     * @return string the result
     */
    public function getTimeForTimezone($name, $timezone)
    {
        $time = new \DateTime(NULL, new \DateTimeZone($timezone));
        $ampm = $time->format('H') > 12 ? ' ('. $time->format('g:i a'). ')' : '';
        return substr($timezone, strlen($name) + 1) . ' - ' . $time->format('H:i') . $ampm;
    }
}
