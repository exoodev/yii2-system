<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components;

use Yii;

/**
 * UrlRule used to create URL depending on the application language.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class UrlRule extends \yii\web\UrlRule
{
    /**
     * @inheritdoc
     */
    public function createUrl($manager, $route, $params)
    {
        $url = parent::createUrl($manager, $route, $params);
        if ($url !== false) {
            $app = Yii::$app;
            $settingLanguage = $app->system->language->getLocale($app->getModule('system')->settingLanguage);
            if ($app->language != $settingLanguage['tag']) {
                return current(explode('-', $app->language)) . '/' . $url;
            }
        }

        return $url;
    }

    /**
     * @inheritdoc
     */
    public function parseRequest($manager, $request)
    {
        return parent::parseRequest($manager, $request);
    }
}
