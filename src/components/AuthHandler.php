<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\imagine\Image;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\authclient\ClientInterface;
use exoo\system\models\Auth;
use exoo\system\models\User;
use exoo\system\models\Profile;
use exoo\system\models\setting\SystemSetting;


/**
 * AuthHandler handles successful authentication via Yii auth component
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class AuthHandler extends Model
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var string
     */
    private $source_id;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $avatar;
    /**
     * @var Profile
     */
    public $profile;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
        $attributes = $this->client->getUserAttributes();
        $this->source_id = ArrayHelper::getValue($attributes, 'id');
        $this->username = ArrayHelper::getValue($attributes, 'username');
        $this->email = ArrayHelper::getValue($attributes, 'email', '');
        $this->profile = new Profile();
        $this->profile->first_name = ArrayHelper::getValue($attributes, 'first_name');
        $this->profile->last_name = ArrayHelper::getValue($attributes, 'last_name');
        $this->profile->birthday = ArrayHelper::getValue($attributes, 'birthday');
        $this->profile->gender_id = ArrayHelper::getValue($attributes, 'gender_id');
        $this->profile->timezone = ArrayHelper::getValue($attributes, 'timezone');

        if ($this->avatar = ArrayHelper::getValue($attributes, 'avatar_url')) {
            $this->profile->avatar_url = $this->profile->folderAvatars . '/' . uniqid() . '.jpg';
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            [['username', 'source_id', 'email'], 'required'],
            [['username', 'source_id', 'avatar'], 'string', 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
        ];
    }

    public function handle()
    {
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $this->source_id,
        ])->one();

        if (Yii::$app->user->isGuest) {
            $duration = Yii::$app->params['user.rememberMeDuration'];
            if ($auth) {
                $user = $auth->user;
                if (!$user->email) {
                    Yii::$app->controller->redirect(['add-email', 'token' => $user->email_token]);
                }
                Yii::$app->user->login($user, $duration);
            } else {
                if ($this->email && ($user = User::findByEmail($this->email)) !== null) {
                    $this->addAuth($user->id);
                    Yii::$app->user->login($user, $duration);
                    // Yii::$app->session->setFlash('modal', [
                    //     Yii::t('system', '<h3>Пользователь зарегистрирован ранее</h3> Пользователь {client} с электронной почтой {email} уже существует, но с ним не связан. <br>{link} на сайт используя {email}, что бы связать их.', [
                    //         'email' => $this->email,
                    //         'client' => $this->client->getTitle(),
                    //         'link' => Html::a('Войдите', ['/user/login']),
                    //     ]),
                    // ]);
                } else {
                    $this->signup();
                }
            }
        } else {
            if (!$auth) {
                $this->addAuth(Yii::$app->user->id);
            } else {
                Yii::$app->session->setFlash('alert.danger', [
                    Yii::t('system', 'Unable to link {client} account. There is another user using it.', [
                        'client' => $this->client->getTitle(),
                    ]),
                ]);
            }
        }
    }

    /**
     * Linked auth account.
     * @param integer $user_id
     */
    public function addAuth($user_id)
    {
        $auth = new Auth([
            'user_id' => $user_id,
            'source' => $this->client->getId(),
            'source_id' => (string) $this->source_id,
        ]);
        if ($auth->save()) {
            Yii::$app->session->setFlash('alert.success', [
                Yii::t('system', 'Linked {client} account', [
                    'client' => $this->client->getTitle(),
                ]),
            ]);
        }
    }

    /**
     * Signup user
     */
    public function signup()
    {
        $password = Yii::$app->security->generateRandomString(8);
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($password);
        $user->_password = $password;
        $user->generateAuthKey();

        if (Yii::$app->settings->get('system', 'registration') == SystemSetting::REGISTRATION_APPROVAL
            || Yii::$app->settings->get('system', 'emailConfirm')
            || !$this->email) {
            $user->status_id = User::STATUS_INACTIVE;
        } else {
            $user->status_id = User::STATUS_ACTIVE;
        }

        if (Yii::$app->settings->get('system', 'emailConfirm') || !$this->email) {
            $user->generateEmailToken();
        }

        $transaction = $user->getDb()->beginTransaction();
        if ($user->save(false)) {
            $auth = new Auth([
                'user_id' => $user->id,
                'source' => $this->client->getId(),
                'source_id' => (string) $this->source_id,
            ]);
            if ($auth->save()) {
                $user->link('profile', $this->profile);
                $user->assignRole(User::ROLE_REGISTERED);
                if ($this->profile->avatar_url) {
                    $this->uploadAvatar();
                }
                $transaction->commit();
                if ($this->email) {
                    Yii::$app->user->login($user);
                    if (Yii::$app->settings->get('system', 'emailRegistered')) {
                        $user->sendMailRegistered();
                    }
                } else {
                    Yii::$app->controller->redirect(['add-email', 'token' => $user->email_token]);
                }
            } else {
                Yii::$app->session->setFlash('alert.danger', [
                    Yii::t('app', 'Unable to save {client} account: {errors}', [
                        'client' => $this->client->getTitle(),
                        'errors' => json_encode($auth->getErrors()),
                    ]),
                ]);
            }
        } else {
            Yii::$app->session->setFlash('alert.danger', [
                Yii::t('system', 'Unable to save {client} user.', [
                    'client' => $this->client->getTitle()
                ]),
                $user->getErrors(),
            ]);
        }
    }

    /**
     * Upload image in storage
     * @param string $image
     */
    public function uploadAvatar()
    {
        if (FileHelper::createDirectory($this->profile->pathToAvatars . $this->profile->folderAvatars)) {
            $params = Yii::$app->params;
            Image::thumbnail($this->avatar, $params['user.avatar.width'], $params['user.avatar.heigth'])
                ->save($this->profile->pathToAvatars . $this->profile->avatar_url, [
                    'quality' => $params['user.avatar.quality'],
                ]);
        }
    }
}
