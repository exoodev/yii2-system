<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components;

/**
 * Formatter provides a set of commonly used data formatting methods.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Formatter extends \yii\i18n\Formatter
{
    /**
     * IP to integer
     * @param string $ip
     * @return string the result
     */
    public function asIntIP($ip)
    {
        return sprintf("%u", ip2long($ip));
    }

    /**
     * Integer to IP
     * @return string the result
     */
    public function asIP($ip)
    {
        return long2ip($ip);
    }
}
