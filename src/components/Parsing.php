<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components;

use Closure;
use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * Parsing content
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Parsing
{
    /**
     * @var string
     */
    public static $configAlias = '@root/config/';

    /**
     * Get parser params
     *
     * @param mixed $value
     * @return string the result
     */
    public static function getParams($params)
    {
        $params = array_merge([
            'storage' => Yii::$app->settings->get('system', 'storageUrl'),
            'block' => function($value) {
                if (($block = Yii::$app->block->get($value)) !== null) {
                    return $block->content;
                }
            },
            'render' => function($value) {
                return Yii::$app->view->render('@app/views/frontend/' . $value);
            },
            'company' => function($value) {
                $settings = Yii::$app->settings->get('company', $value);
                if (is_array($settings)) {
                    return Json::encode($settings);
                }
                return $settings;
            }
        ], $params);

        $configFile = Yii::getAlias(static::$configAlias) . 'parsing.php';

        if (file_exists($configFile)) {
            $params = array_merge($params, (array) require $configFile);
        }

        return $params;
    }

    /**
     * Executing parsing
     *
     * @param mixed $value
     * @return string the result
     */
    public static function process($content, $params = [])
    {
        foreach (static::getParams($params) as $key => $value) {
            if ($value instanceof Closure) {
                $callbacks[$key] = $value;
            } else {
                $search[] = '{{' . $key . '}}';
                $replace[] = $value;
            }
        }

        if (isset($callbacks)) {
            $content = static::addCallbacks($callbacks, $content);
        }

        if (isset($search, $replace)) {
            $content = str_replace($search, $replace, $content);
        }

        return preg_replace('/\{\{(.*?)\}\}/is', '', $content);
    }

    /**
     * Add widgets to content
     *
     * @param array $callbacks
     * @param string $content
     */
    protected static function addCallbacks($callbacks, $content) {
        $pattern = '/\{\{(' . implode(array_keys($callbacks), '|') . ')(.*?)\}\}/is';
        preg_match_all($pattern, $content, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            if (isset($callbacks[$match[1]])) {
                $search[] = $match[0];
                $replace[] = call_user_func($callbacks[$match[1]], str_replace(':', '', $match[2]));
            }
        }

        if (isset($search, $replace)) {
            $content = str_replace($search, $replace, $content);
        }

        return $content;
    }
}
