<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2019
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components;

use Yii;
use yii\base\Component;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use exoo\system\models\Block as BlockModel;

/**
 * Block component.
 *
 * // Get all blocks
 * $blocks = Yii::$app->block->getAll(['author', 'training', 'alfredo']);
 * $block = $blocks->get('training');
 *
 * // Get one block
 * $block = Yii::$app->block->get('training');
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Block extends Component
{
    /**
     * @var array the property
     */
    private $_blocks = [];

    /**
     * Get one block.
     *
     * @param string|integer $id
     * @return BlockModel
     */
    public function get($id, $encode = true)
    {
        if (isset($this->_blocks[$id])) {
            return $this->_blocks[$id];
        }

        $block = $this->find($id)->one();

        if ($block && $encode) {
            $block->title = Html::encode($block->title);
            $block->content = HtmlPurifier::process($block->content);
        }

        return $block;
    }

    /**
     * Get all blocks
     *
     * @param string|integer $id
     * @return BlockModel
     */
    public function getAll($id, $encode = true)
    {
        $this->_blocks = $this->find($id)->all();
        return $this;
    }

    /**
     * Get BlockQuery
     *
     * @param string|integer $id
     * @return BlockQuery
     */
    protected function find($id)
    {
        $attr = is_int($id) ? 'id' : 'slug';

        return BlockModel::find()
            ->where([$attr => $id])
            ->indexBy($attr)
            ->active();
    }
}
