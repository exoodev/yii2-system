<?php

namespace exoo\system\components;

use Yii;
use yii\console\Controller;

class BaseRbacController extends Controller
{
    /**
     * Adds an item as a child of another item.
     * @param \yii\rbac\Item $parent
     * @param \yii\rbac\Item $child
     */
    public static function addChild($parent, $child)
    {
        $auth = Yii::$app->getAuthManager();
        if (!$auth->hasChild($parent, $child) && $auth->canAddChild($parent, $child)) {
            $auth->addChild($parent, $child);
        }
    }

    /**
     * Updates permission in the RBAC system.
     * @param string|array $name the old name of the permission.
     * @param \yii\rbac\Permission $permission
     * @param boolean $add whether to add the permission to the RBAC system if it is not exists.
     * @return boolean whether the updating or adding is successful.
     */
    public static function updatePermission($name, $permission, $add = true)
    {
        $auth = Yii::$app->getAuthManager();
        if (is_array($name)) {
            foreach ($name as $n) {
                $oldPermission = $auth->getPermission($n);
                if ($oldPermission) {
                    break;
                }
            }
        } else {
            $oldPermission = $auth->getPermission($name);
        }
        if (!$oldPermission) {
            if ($add) {
                return $auth->add($permission);
            }
        } else {
            return $auth->update($oldPermission->name, $permission);
        }
    }

    /**
     * Updates role in the RBAC system.
     * @param string|array $name the old name of the role.
     * @param \yii\rbac\Role $object
     * @param boolean $add whether to add the role to the RBAC system if it is not exists.
     * @return boolean whether the updating or adding is successful
     * @throws \Exception if data validation or saving fails (such as the name of the role)
     */
    public static function updateRole($name, $role, $add = true)
    {
        $auth = Yii::$app->getAuthManager();
        if (is_array($name)) {
            foreach ($name as $n) {
                $oldRole = $auth->getRole($n);
                if ($oldRole) {
                    break;
                }
            }
        } else {
            $oldRole = $auth->getRole($name);
        }
        if (!$oldRole) {
            if ($add) {
                return $auth->add($role);
            }
        } else {
            return $auth->update($oldRole->name, $role);
        }
    }

    /**
     * Removes permission via [[remove()]].
     * @param string $permisssionName Permission name.
     */
    public static function removePermission($permisssionName)
    {
        return self::remove(Yii::$app->getAuthManager()->getPermission($permisssionName));
    }

    /**
     * Removes role via [[remove()]].
     * @param string $roleName Role name.
     */
    public static function removeRole($roleName)
    {
        return self::remove(Yii::$app->getAuthManager()->getRole($roleName));
    }

    /**
     * Removes rule via [[remove()]].
     * @param string $ruleName Rule name.
     */
    public static function removeRule($ruleName)
    {
        return self::remove(Yii::$app->getAuthManager()->getRule($ruleName));
    }

    /**
     * Removes a role, permission or rule from the RBAC system.
     * @param \yii\rbac\Role|\yii\rbac\Permission|\yii\rbac\Rule $object
     */
    public static function remove($object)
    {
        if ($object) {
            return Yii::$app->getAuthManager()->remove($object);
        }
        return true;
    }
}
