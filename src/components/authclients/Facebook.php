<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components\authclients;

use Yii;
use yii\helpers\ArrayHelper;
use exoo\system\models\Profile;

/**
 * Facebook allows authentication via Facebook OAuth.
 *
 * In order to use Facebook OAuth you must register your application at <https://developers.facebook.com/apps>.
 *
 * Example application configuration:
 *
 * ```php
 * 'components' => [
 *     'authClientCollection' => [
 *         'class' => 'yii\authclient\Collection',
 *         'clients' => [
 *             'facebook' => [
 *                 'class' => 'yii\authclient\clients\Facebook',
 *                 'clientId' => 'facebook_client_id',
 *                 'clientSecret' => 'facebook_client_secret',
 *             ],
 *         ],
 *     ]
 *     // ...
 * ]
 * ```
 *
 * @see https://developers.facebook.com/apps
 * @see http://developers.facebook.com/docs/reference/api
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Facebook extends \yii\authclient\clients\Facebook
{
    /**
     * @inheritdoc
     */
    public $attributeNames = [
        'name',
        'email',
        'gender',
        'birthday',
        'first_name',
        'last_name',
        'timezone',
    ];
    /**
     * @var string
     */
    public $sizeAvatar = 'width=460&height=460';

    /**
     * @inheritdoc
     */
    public function getUserAttributes()
    {
        $attributes = parent::getUserAttributes();
        $result = [];

        if (isset($attributes['id'])) {
            $result['id'] = $attributes['id'];
        }

        if (isset($attributes['name'])) {
            $result['username'] = $attributes['name'];
        }

        if (isset($attributes['email'])) {
            $result['email'] = $attributes['email'];
        }

        if (isset($attributes['first_name'])) {
            $result['first_name'] = $attributes['first_name'];
        }

        if (isset($attributes['last_name'])) {
            $result['last_name'] = $attributes['last_name'];
        }

        if (isset($attributes['timezone'])) {
            $result['timezone'] = $attributes['timezone'];
        }

        if (isset($attributes['gender'])) {
            switch ($attributes['gender']) {
                case 'male':
                    $result['gender_id'] = Profile::GENDER_MAN;
                    break;
                case 'female':
                    $result['gender_id'] = Profile::GENDER_WOMAN;
                    break;
                default:
                    $result['gender_id'] = Profile::GENDER_NOT_SPECIFIED;
                    break;
            }
        }

        if (!empty($attributes['id'])) {
            $result['avatar_url'] = 'http://graph.facebook.com/' . $attributes['id'] . '/picture?' . $this->sizeAvatar;
        }

        return $result;
    }
}
