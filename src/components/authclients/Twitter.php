<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components\authclients;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Twitter allows authentication via Twitter OAuth.
 *
 * In order to use Twitter OAuth you must register your application at <https://dev.twitter.com/apps/new>.
 *
 * Example application configuration:
 *
 * ```php
 * 'components' => [
 *     'authClientCollection' => [
 *         'class' => 'yii\authclient\Collection',
 *         'clients' => [
 *             'twitter' => [
 *                 'class' => 'yii\authclient\clients\Twitter',
 *                 'attributeParams' => [
 *                     'include_email' => 'true'
 *                 ],
 *                 'consumerKey' => 'twitter_consumer_key',
 *                 'consumerSecret' => 'twitter_consumer_secret',
 *             ],
 *         ],
 *     ]
 *     // ...
 * ]
 * ```
 *
 * > Note: some auth workflows provided by Twitter, such as [application-only authentication](https://dev.twitter.com/oauth/application-only),
 *   uses OAuth 2 protocol and thus are impossible to be used with this class. You should use [[TwitterOAuth2]] for these.
 *
 * @see TwitterOAuth2
 * @see https://apps.twitter.com/
 * @see https://dev.twitter.com/
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Twitter extends \yii\authclient\clients\Twitter
{
    /**
     * @inheritdoc
     */
    public $attributeParams = [
        'include_email' => 'true',
    ];

    /**
     * @inheritdoc
     */
    public function getUserAttributes()
    {
        $attributes = parent::getUserAttributes();
        $result = [];

        if (isset($attributes['id'])) {
            $result['id'] = $attributes['id'];
        }

        if (isset($attributes['name'])) {
            $result['username'] = $attributes['name'];
        }

        if (isset($attributes['email'])) {
            $result['email'] = $attributes['email'];
        }

        if (isset($attributes['profile_image_url'])) {
            $result['avatar_url'] = str_replace('_normal', '', $attributes['profile_image_url']);
        }

        return $result;
    }
}
