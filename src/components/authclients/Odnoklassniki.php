<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components\authclients;

use Yii;
use yii\helpers\ArrayHelper;
use yii\authclient\OAuth2;

/**
 * Odnoklassniki allows authentication via Odnoklassniki OAuth.
 * In order to use Odnoklassniki OAuth you must:
 * 1. Registered development <https://ok.ru/devaccess>
 * 2. Register your application at <https://ok.ru/dk?st.cmd=appEditBasic&st._aid=Apps_Info_MyDev_AddApp>.
 *
 * Example application configuration:
 *
 * ```php
 * 'components' => [
 *     'authClientCollection' => [
 *         'class' => 'yii\authclient\Collection',
 *         'clients' => [
 *             'vkontakte' => [
 *                 'class' => 'exoo\system\components\authclients\Odnoklassniki',
 *                 'clientId' => 'odnoklassniki_app_id',
 *                 'clientSecret' => 'odnoklassniki_secret_key',
 *                 'clientPublic' => 'odnoklassniki_public_key',
 *             ],
 *         ],
 *     ]
 *     ...
 * ]
 * ```
 *
 * @see    https://ok.ru/devaccess
 * @see    https://ok.ru/dk?st.cmd=appEditBasic&st._aid=Apps_Info_MyDev_AddApp
 * @see    http://apiok.ru/wiki/pages/viewpage.action?pageId=81822109
 * @see    http://apiok.ru/wiki/pages/viewpage.action?pageId=75989046
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Odnoklassniki extends OAuth2
{
    /**
     * @inheritdoc
     */
    public $authUrl = 'https://connect.ok.ru/oauth/authorize';
    /**
     * @inheritdoc
     */
    public $tokenUrl = 'https://api.ok.ru/oauth/token.do';
    /**
     * @inheritdoc
     */
    public $apiBaseUrl = 'https://api.ok.ru/fb.do';
    /**
     * @var string OAuth client public key.
     */
    public $clientPublic;

    /**
     * @inheritdoc
     */
    protected function initUserAttributes()
    {
        return $this->api('users.getCurrentUser', 'GET');
    }

    /**
     * @inheritdoc
     */
    protected function defaultName()
    {
        return 'odnoklassniki';
    }

    /**
     * @inheritdoc
     */
    protected function defaultTitle()
    {
        return 'Odnoklassniki';
    }

    /**
     * @inheritdoc
     */
    protected function defaultNormalizeUserAttributeMap()
    {
        return [
            'id' => 'uid'
        ];
    }
}
