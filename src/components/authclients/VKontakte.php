<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components\authclients;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * VKontakte allows authentication via VKontakte OAuth.
 *
 * In order to use VKontakte OAuth you must register your application at <http://vk.com/editapp?act=create>.
 *
 * Example application configuration:
 *
 * ```php
 * 'components' => [
 *     'authClientCollection' => [
 *         'class' => 'yii\authclient\Collection',
 *         'clients' => [
 *             'vkontakte' => [
 *                 'class' => 'yii\authclient\clients\VKontakte',
 *                 'clientId' => 'vkontakte_client_id',
 *                 'clientSecret' => 'vkontakte_client_secret',
 *             ],
 *         ],
 *     ]
 *     // ...
 * ]
 * ```
 *
 * @see http://vk.com/editapp?act=create
 * @see http://vk.com/developers.php?oid=-1&p=users.get
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class VKontakte extends \yii\authclient\clients\VKontakte
{
    /**
     * @inheritdoc
     */
    public $scope = ['email'];
    /**
     * @inheritdoc
     */
    public $attributeNames = [
        'uid',
        'first_name',
        'last_name',
        'nickname',
        'screen_name',
        'sex',
        'bdate',
        'city',
        'country',
        'timezone',
        'photo_max_orig',
    ];

    /**
     * @inheritdoc
     */
    public function getUserAttributes()
    {
        $attributes = parent::getUserAttributes();
        $result = [];

        if (isset($attributes['id'])) {
            $result['id'] = $attributes['id'];
        }

        if (isset($attributes['first_name'])) {
            $result['first_name'] = $attributes['first_name'];
        }

        if (isset($attributes['last_name'])) {
            $result['last_name'] = $attributes['last_name'];
        }

        $result['username'] = $attributes['first_name'] . ' ' . $attributes['last_name'];

        if (isset($attributes['email'])) {
            $result['email'] = $attributes['email'];
        }

        if (isset($attributes['bdate'])) {
            $result['birthday'] = Yii::$app->formatter->asTimestamp($attributes['bdate']);
        }

        if (isset($attributes['sex'])) {
            $result['gender_id'] = $attributes['sex'];
        }

        if (isset($attributes['timezone'])) {
            $result['timezone'] = $attributes['timezone'];
        }

        if (isset($attributes['photo_max_orig'])) {
            $result['avatar_url'] = $attributes['photo_max_orig'];
        }

        return $result;
    }
}
