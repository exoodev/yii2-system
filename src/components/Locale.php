<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Object;
use exoo\system\models\Language;

/**
 * Locale class.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Locale extends Object
{
    /**
     * @var string cache key for languages.
     */
    public $cacheKey = 'system/locales';

    /**
     * Returns system languages.
     * @return array system languages.
     */
    public function getLanguages()
    {
        $languages = [];
        $cache = Yii::$app->cache->get($this->cacheKey);

        if ($cache === false) {
            $languages = Yii::$app->db->createCommand('SELECT * FROM ' . Language::tableName() . ' ORDER BY `position`')->queryAll();
            Yii::$app->cache->set($this->cacheKey, $languages);
        } else {
            $languages = $cache;
        }

        return $languages;
    }

    /**
     * Returns array list of system languages in a key-value format, where the key is language 'tag' and value is a language 'name'.
     * @return array array list of system languages.
     */
    public function getLanguageList()
    {
        return ArrayHelper::map($this->getLanguages(), 'id', 'url');
    }

    /**
     * Returns locale if id or tag will match.
     * @param integer $id Locale id.
     * @param string $tag Locale tag.
     * @return array
     */
    public function getLocale($id)
    {
        return ArrayHelper::getValue($this->getLocales(), $id);
    }

    /**
     * Deletes cache used for languages.
     * @return boolean If no error happens during deletion.
     */
    public function cacheDelete()
    {
        return Yii::$app->cache->delete($this->cacheKey);
    }

    /**
     * List of locales
     * @return array
     */
    public function getList()
    {
        $locales = [];

        foreach (resourcebundle_locales('') as $key => $tag) {
            $locale = str_replace('_', '-', $tag);
            $locales[$locale] = $locale . ' - ' . locale_get_display_name($locale, Yii::$app->language);
        }

        return $locales;
    }

    // /**
    //  * List of UI locales
    //  * @return array
    //  */
    // public function getUiList()
    // {
    //     $tags = ArrayHelper::getValue(Yii::$app->params, 'locales_ui', []);
    //     $locales = [];

    //     foreach ($tags as $tag) {
    //         $locales[$tag] = $tag . ' - ' . locale_get_display_name($tag, Yii::$app->language);
    //     }

    //     return $locales;
    // }
}
