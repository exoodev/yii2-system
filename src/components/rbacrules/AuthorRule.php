<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\components\rbacrules;

use yii\rbac\Rule;

/**
 * Checks if authorID matches user passed via params
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class AuthorRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'isAuthor';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return isset($params['model']) ? $params['model']->created_by == $user : false;
    }
}
