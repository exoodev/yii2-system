<?php

namespace exoo\system\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Undocumented class.
 */
class Menu extends Component
{
    /**
     * @var array the items
     */
    public $items = [];
    /**
     * @var array
     */
    private $_menus = [];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        foreach ((array) $this->items as $app => $path) {
            $menuFile = Yii::getAlias($path . '.php');

            if ($item = $this->loadMenuFromFile($menuFile)) {
                $this->_menus[$app] = $item;
            }
        }
    }

    public function get($menu)
    {
        return ArrayHelper::getValue($this->_menus, $menu, '[]');
    }

    public function getItems($key)
    {
        $items = [];
        foreach ($this->_menus as $menus) {
            if (isset($menus[$key])) {
                foreach ($menus[$key] as $menu) {
                    $items[] = $menu;
                }
            }
        }

        return $items;
    }

    public function getItemsCurrentModule($key)
    {
        $module_id = Yii::$app->controller->module->id;

        if (isset($this->_menus[$module_id][$key])) {
            return $this->_menus[$module_id][$key];
        }
    }

    protected function loadMenuFromFile($menuFile)
    {
        if (is_file($menuFile)) {
            $menu = include $menuFile;
            if (!is_array($menu)) {
                $menu = [];
            }
            return $menu;
        }
        throw new InvalidConfigException('Configuration file not found ' . $menuFile);
    }
}
