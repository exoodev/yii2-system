<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$company = Yii::$app->settings->get('system', 'company');
$companyLink = Html::a($company, Url::home(true), ['target' => '_blank']);
?>
<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#F7F7F7">
<tbody>
    <tr>
        <td width="15">
            <table cellspacing="0" cellpadding="0" width="15">
                <tbody>
                    <tr>
                        <td>
                            <div style="min-height:0;line-height:0;font-size:0"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <td align="center" style="padding:30px 0">
        <table cellspacing="0" cellpadding="0" width="600" bgcolor="#FFFFFF">
            <tbody>
                <tr>
                    <td align="center" style="padding-top:50px;padding-right:50px;padding-bottom:50px;padding-left:50px;background-color:#8cc14c">
                        <div style="font-size:28px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:34px;color:#ffffff;">
                            <?= $company ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:50px;padding-bottom:30px;background-color:#ffffff;color:#333333;font-size:14px;line-height:18px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif">
                        <div style="padding-left:50px;padding-right:50px">
                            <h2><?= Yii::t('system', 'Your data to access the site') ?></h2>
                            <p style="margin-top:1em;margin-bottom:0">
                                <?= Yii::t('system', 'For you to create a profile and stored login information. The next time you log in using your e-mail and password.') ?>
                            </p>
                            <h3><?= Yii::t('system', 'Save your data to avoid losing') ?></h3>
                            <hr>
                            <table>
                                <tr>
                                    <td><?= Yii::t('system', 'Name') ?></td>
                                    <td><?= Html::encode($user->username) ?></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><?= Html::encode($user->email) ?></td>
                                </tr>
                                <tr>
                                    <td><?= Yii::t('system', 'Password') ?></td>
                                    <td>
                                        <strong><?= Html::encode($user->_password) ?></strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="margin-top:30px;padding-top:30px;padding-left:50px;padding-right:50px;border-top:1px solid #ebebeb;color:#9c9c9c;font-size:12px;line-height:16px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif">
                            <p style="margin-top:1em;margin-bottom:0">
                                <?= Yii::t('system', 'Message generated confirm system') ?> <?= $companyLink ?>.<br>
                                <?= Yii::t('system', 'If you have not registered on our site, then simply delete this e-mail.') ?>
                            </p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
    <td width="15">
        <table cellspacing="0" cellpadding="0" width="15">
            <tbody>
                <tr>
                    <td>
                        <div style="min-height:0;line-height:0;font-size:0"></div>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tbody>
</table>
