<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/reset-password', 'token' => $user->password_reset_token]);
$company = Yii::$app->settings->get('system', 'company');
?>

<?= $company ?>

<?= Yii::t('system', 'Access recovery') ?>

<?= Yii::t('system', 'Hello') ?> <?= $user->username ?>

<?= Yii::t('system', 'You have requested to reset your password to your account') ?>

<?= Yii::t('system', 'Follow the link below to reset your password:') ?>

<?= $resetLink ?>


<?= Yii::t('system', 'Message generated password reset system') ?> <?= $company ?>.
<?= Yii::t('system', 'If you did not request it, just delete this e-mail.') ?>
