<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/reset-password', 'token' => $user->password_reset_token]);
$company = Yii::$app->settings->get('system', 'company');
$companyLink = Html::a($company, Url::home(true), ['target' => '_blank']);
?>
<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#F7F7F7">
<tbody>
    <tr>
        <td width="15">
            <table cellspacing="0" cellpadding="0" width="15">
                <tbody>
                    <tr>
                        <td>
                            <div style="min-height:0;line-height:0;font-size:0"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <td align="center" style="padding:30px 0">
        <table cellspacing="0" cellpadding="0" width="600" bgcolor="#FFFFFF">
            <tbody>
                <tr>
                    <td align="center" style="padding-top:50px;padding-right:50px;padding-bottom:50px;padding-left:50px;background-color:#00a0df">
                        <div style="font-size:28px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;line-height:34px;color:#ffffff;">
                            <?= $company ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:50px;padding-bottom:30px;background-color:#ffffff;color:#333333;font-size:14px;line-height:18px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif">
                        <div style="padding-left:50px;padding-right:50px">
                            <h2><?= Yii::t('system', 'Access recovery') ?></h2>
                            <p style="margin-top:0;margin-bottom:0">
                                <?= Yii::t('system', 'Hello') ?> <?= Html::encode($user->username) ?>
                            </p>
                            <p style="margin-top:1em;margin-bottom:0">
                                <?= Yii::t('system', 'You have requested to reset your password to your account.') ?><br>
                                <?= Yii::t('system', 'To continue, click on the button:') ?>
                            </p>
                            <p style="margin-top:1.5em;margin-bottom:1.5em;text-align:center">
                                <?= Html::a(Yii::t('system', 'Reset password'), $resetLink, [
                                    'style' => 'display:inline-block;border-radius:5px!important;text-decoration:none;white-space:nowrap;padding-left:20px;padding-right:20px;padding-top:10px;padding-bottom:10px;min-height:20px;line-height:20px;font-size:16px;font-weight:bold;background:#8cc14c;color:#ffffff!important',
                                    'target' => '_blank',
                                ]) ?>
                            </p>
                        </div>
                        <div style="margin-top:30px;padding-top:30px;padding-left:50px;padding-right:50px;border-top:1px solid #ebebeb;color:#9c9c9c;font-size:12px;line-height:16px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif">
                            <p style="margin-top:1em;margin-bottom:0">
                                <?= Yii::t('system', 'Message generated password reset system') ?> <?= $companyLink ?>.<br>
                                <?= Yii::t('system', 'If you did not request it, just delete this e-mail.') ?>
                            </p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
    <td width="15">
        <table cellspacing="0" cellpadding="0" width="15">
            <tbody>
                <tr>
                    <td>
                        <div style="min-height:0;line-height:0;font-size:0"></div>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tbody>
</table>
