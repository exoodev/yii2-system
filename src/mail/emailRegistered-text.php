<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$company = Yii::$app->settings->get('system', 'company');
?>

<?= $company ?>

<?= Yii::t('system', 'Your data to access the site') ?>

<?= Yii::t('system', 'For you to create a profile and stored login information. The next time you log in using your e-mail and password.') ?>

<?= Yii::t('system', 'Save your data to avoid losing') ?>

<?= Yii::t('system', 'Name') ?> <?= $user->username ?>

Email <?= $user->email ?>

<?= Yii::t('system', 'Password') ?> <?= $user->_password ?>

<?= Yii::t('system', 'Message generated confirm system') ?> <?= $company ?>
<?= Yii::t('system', 'If you have not registered on our site, then simply delete this e-mail.') ?>
