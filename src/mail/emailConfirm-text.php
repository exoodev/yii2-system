<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/email-confirm', 'token' => $user->email_token]);
$company = Yii::$app->settings->get('system', 'company');
?>

<?= $company ?>

<?= Yii::t('system', 'Activate e-mail') ?>.

<?= Yii::t('system', 'Hello') ?> <?= $user->username ?>

<?= Yii::t('system', 'Confirm your e-mail to complete the registration.') ?>

<?= Yii::t('system', 'Follow the link below to confirm e-mail:') ?>

<?= $confirmLink ?>


<?= Yii::t('system', 'Message generated confirm system') ?> <?= $company ?>.
<?= Yii::t('system', 'If you have not registered on our site, then simply delete this e-mail.') ?>
