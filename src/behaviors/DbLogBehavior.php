<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2017
 * @package
 * @version 1.0.0
 */

namespace exoo\system\behaviors;

use Yii;
use yii\helpers\Json;
use yii\base\InvalidCallException;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use exoo\system\models\DbLog;

/**
 * To use DbLogBehavior, insert the following code to your ActiveRecord class:
 *
 * ```php
 *
 * public function behaviors()
 * {
 *     return [
 *         DbLogBehavior::className(),
 *         //attributes => ['...']
 *     ];
 * }
 * ```
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class DbLogBehavior extends Behavior
{
    /**
     * @var array
     */
    public $attributes;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'saveLog',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveLog',
            ActiveRecord::EVENT_AFTER_DELETE => 'saveLog',
        ];
    }

    public function saveLog($event)
    {
        $owner = $this->owner;
        $model = new DbLog();
        $modelEvent = [
            'afterInsert' => $model::EVENT_CREATE,
            'afterUpdate' => $model::EVENT_UPDATE,
            'afterDelete' => $model::EVENT_DELETE,
        ];
        $model->event_id = $modelEvent[$event->name];
        $model->data = Json::encode($owner->getAttributes($this->attributes));
        $model->model = $owner::className();
        $model->model_id = $owner->id;
        $model->user_id = Yii::$app->user->id;
        $model->created_at = time();
        $model->save();
    }
}
