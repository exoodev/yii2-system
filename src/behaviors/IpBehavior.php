<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2017
 * @package
 * @version 1.0.0
 */

namespace exoo\system\behaviors;

use Yii;
use yii\base\InvalidCallException;
use yii\db\BaseActiveRecord;
use yii\behaviors\AttributeBehavior;

/**
 * IpBehavior automatically fills the specified attribute with the current ip.
 *
 * To use IpBehavior, insert the following code to your ActiveRecord class:
 *
 * ```php
 *
 * public function behaviors()
 * {
 *     return [
 *         IpBehavior::className(),
 *     ];
 * }
 * ```
 *
 * By default, IpBehavior will fill the `ip` attribute with the Yii::$app->request->userIP.
 *
 * Because attribute value will be set automatically by this behavior, they are usually not user input and should therefore
 * not be validated, i.e. `ip` should not appear in the [[\yii\base\Model::rules()|rules()]] method of the model.
 *
 * For the above implementation to work with MySQL database, please declare the column(`ip`) as int(11) for IP.
 *
 * If your attribute name are different or you want to use a different way of calculating the ip,
 * you may configure the [[ipAttribute]] and [[value]] properties like the following:
 *
 * ```php
 * use yii\db\Expression;
 *
 * public function behaviors()
 * {
 *     return [
 *         [
 *             'class' => IpBehavior::className(),
 *             'ipAttribute' => 'ip',
 *             'value' => new Expression('...'),
 *         ],
 *     ];
 * }
 * ```
 *
 * In case you use an [[\yii\db\Expression]] object as in the example above, the attribute will not hold the ip value, but
 * the Expression object itself after the record has been saved. If you need the value from DB afterwards you should call
 * the [[\yii\db\ActiveRecord::refresh()|refresh()]] method of the record.
 *
 * IpBehavior also provides a method named [[setIP()]] that allows you to assign the ip to the specified attribute and save them to the database. For example,
 *
 * ```php
 * $model->setIP();
 * ```
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class IpBehavior extends AttributeBehavior
{
    /**
     * @var string the attribute that will receive ip value
     * Set this property to false if you do not want to record the ip.
     */
    public $ipAttribute = 'ip';
    /**
     * @inheritdoc
     */
    public $value;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => $this->ipAttribute,
                BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->ipAttribute,
            ];
        }
    }

    /**
     * @inheritdoc
     */
    protected function getValue($event)
    {
        if ($this->value === null) {
            return Yii::$app->formatter->asIntIP(Yii::$app->request->userIP);
        }
        return parent::getValue($event);
    }

    /**
     * Updates a ip attribute to the ip.
     *
     * ```php
     * $model->setIP();
     * ```
     * @param string $attribute the name of the attribute to update.
     * @throws InvalidCallException if owner is a new record.
     */
    public function setIP($attribute)
    {
        /* @var $owner BaseActiveRecord */
        $owner = $this->owner;
        if ($owner->getIsNewRecord()) {
            throw new InvalidCallException('Updating the ip is not possible on a new record.');
        }
        $owner->updateAttributes(array_fill_keys((array) $attribute, $this->getValue(null)));
    }
}
