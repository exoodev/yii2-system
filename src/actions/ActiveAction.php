<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\system\actions;

use Yii;
use yii\web\BadRequestHttpException;

/**
 * ActiveAction class.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ActiveAction extends Action
{
    /**
     * @var string the attribute name associated with this column. When neither [[content]] nor [[value]]
     * is specified, the value of the specified attribute will be retrieved from each data model and displayed.
     *
     * Also, if [[label]] is not specified, the label associated with the attribute will be displayed.
     */
    public $attribute = 'active';

    /**
     * Changing the status of the [[Model]] model on the opposite.
     * @param integer $id Model id.
     * @return mixed
     */
    public function run($id)
    {
        if (!Yii::$app->request->isPost) {
            throw new BadRequestHttpException('Only POST is allowed');
        }

        $model = $this->findModel($id);
        $model->updateAttributes([$this->attribute => !$model->active]);

        if (!Yii::$app->request->isAjax) {
            return Yii::$app->getResponse()->redirect(Yii::$app->getUser()->getReturnUrl());
        }
    }
}
