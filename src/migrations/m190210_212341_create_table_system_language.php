<?php

use yii\db\Migration;

class m190210_212341_create_table_system_language extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_language}}', [
            'id' => $this->string()->notNull()->append('PRIMARY KEY'),
            'url' => $this->string()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%system_language}}');
    }
}
