<?php

use yii\db\Migration;

class m190210_212341_create_table_blog_post extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%blog_post}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'image' => $this->string(),
            'small_text' => $this->text()->notNull(),
            'full_text' => $this->text()->notNull(),
            'category_id' => $this->integer(),
            'meta_title' => $this->string()->notNull(),
            'meta_description' => $this->string()->notNull(),
            'publish_on' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'status_id' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_blog_post', '{{%blog_post}}', ['slug', 'category_id', 'created_by']);
        $this->createIndex('fk_blog_post_blog_category', '{{%blog_post}}', 'category_id');
        $this->addForeignKey('blog_post_ibfk_1', '{{%blog_post}}', 'category_id', '{{%blog_category}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%blog_post}}');
    }
}
