<?php

use yii\db\Migration;

class m190210_212341_create_table_system_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'auth_key' => $this->string()->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string(),
            'email_token' => $this->string(),
            'email' => $this->string()->notNull(),
            'status_id' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'last_visit' => $this->integer(),
            'ip' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('password_reset_token', '{{%system_user}}', 'password_reset_token', true);
        $this->createIndex('idx_user', '{{%system_user}}', ['username', 'email', 'status_id', 'created_at']);
        $this->createIndex('email', '{{%system_user}}', 'email', true);
        $this->createIndex('email_token', '{{%system_user}}', 'email_token', true);

        $this->insert('{{%system_user}}', [
            'id' => 1,
            'username' => 'admin',
            'auth_key' => 'SekdRXQzWsqaN1sclVLiHtMpxEbA-UEI',
            'password_hash' => '$2y$13$LXsPyJgr6USmGyOHCMADuO76AWuIu4oQQF6TMHu7eoU0oYc1cfJvW',
            'email' => 'admin@mail.com',
            'status_id' => 1,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%system_user}}');
    }
}
