<?php

use yii\db\Migration;

class m190210_212341_create_table_review_content extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%review_content}}', [
            'id' => $this->primaryKey(),
            'header' => $this->text(),
            'footer' => $this->text(),
            'meta_title' => $this->string(),
            'meta_description' => $this->string(),
            'language' => $this->string(),
        ], $tableOptions);

        $this->createIndex('idx_review_content', '{{%review_content}}', 'language');
    }

    public function down()
    {
        $this->dropTable('{{%review_content}}');
    }
}
