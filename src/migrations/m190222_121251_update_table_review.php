<?php

use yii\db\Migration;

class m190222_121251_update_table_review extends Migration
{
    public function up()
    {
        $this->addColumn('{{%review}}', 'answer', $this->text());
        $this->addColumn('{{%review}}', 'created_by', $this->integer());
        $this->addColumn('{{%review}}', 'updated_by', $this->integer());
    }

    public function down()
    {
        echo "m190222_121251_update_table_review cannot be reverted.\n";
        return false;
    }
}
