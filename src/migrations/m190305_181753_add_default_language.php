<?php

use yii\db\Migration;

/**
 * Class m190305_181753_add_default_language
 */
class m190305_181753_add_default_language extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('{{%system_language}}', [
            'id' => 'ru-RU',
            'url' => 'ru',
            'position' => 1,
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%system_language}}');
    }
}
