<?php

use yii\db\Migration;

class m190210_212341_create_table_review extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%review}}', [
            'id' => $this->primaryKey(),
            'author' => $this->string()->notNull(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'comment' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_review', '{{%review}}', ['author', 'email', 'status']);
    }

    public function down()
    {
        $this->dropTable('{{%review}}');
    }
}
