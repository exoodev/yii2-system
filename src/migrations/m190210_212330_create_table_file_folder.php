<?php

use yii\db\Migration;

class m190210_212330_create_table_file_folder extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%file_folder}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'slug' => $this->string()->notNull(),
            'bucket' => $this->string()->notNull(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->createIndex('slug', '{{%file_folder}}', 'slug');
    }

    public function down()
    {
        $this->dropTable('{{%file_folder}}');
    }
}
