<?php

use yii\db\Migration;

class m190210_212342_create_table_system_block extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_block}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'position' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->createIndex('idx_system_block', '{{%system_block}}', ['status', 'slug', 'position']);
    }

    public function down()
    {
        $this->dropTable('{{%system_block}}');
    }
}
