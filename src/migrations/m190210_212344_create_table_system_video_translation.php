<?php

use yii\db\Migration;

class m190210_212344_create_table_system_video_translation extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_video_translation}}', [
            'video_id' => $this->integer()->notNull(),
            'language_id' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%system_video_translation}}', ['video_id', 'language_id']);
        $this->createIndex('language_id', '{{%system_video_translation}}', 'language_id');
        $this->addForeignKey('system_video_translation_ibfk_1', '{{%system_video_translation}}', 'video_id', '{{%system_video}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('system_video_translation_ibfk_2', '{{%system_video_translation}}', 'language_id', '{{%system_language}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%system_video_translation}}');
    }
}
