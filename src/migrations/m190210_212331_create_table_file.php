<?php

use yii\db\Migration;

class m190210_212331_create_table_file extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%file}}', [
            'id' => $this->primaryKey(),
            'filename' => $this->string()->notNull(),
            'folder_id' => $this->integer()->notNull(),
            'position' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_file-folder_id', '{{%file}}', 'folder_id');
        $this->addForeignKey('file_ibfk_1', '{{%file}}', 'folder_id', '{{%file_folder}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%file}}');
    }
}
