<?php

use yii\db\Migration;

class m190210_212342_create_table_system_setting extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_setting}}', [
            'id' => $this->primaryKey(),
            'group' => $this->string()->notNull(),
            'key' => $this->string()->notNull(),
            'value' => $this->text()->notNull(),
            'type' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('app', '{{%system_setting}}', ['group', 'key'], true);
    }

    public function down()
    {
        $this->dropTable('{{%system_setting}}');
    }
}
