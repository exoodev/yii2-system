<?php

use yii\db\Migration;

class m190210_212342_create_table_system_block_translation extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_block_translation}}', [
            'block_id' => $this->integer()->notNull(),
            'language_id' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%system_block_translation}}', ['block_id', 'language_id']);
        $this->createIndex('language_id', '{{%system_block_translation}}', 'language_id');
        $this->addForeignKey('system_block_translation_ibfk_1', '{{%system_block_translation}}', 'block_id', '{{%system_block}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('system_block_translation_ibfk_2', '{{%system_block_translation}}', 'language_id', '{{%system_language}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%system_block_translation}}');
    }
}
