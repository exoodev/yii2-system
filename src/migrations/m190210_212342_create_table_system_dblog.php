<?php

use yii\db\Migration;

class m190210_212342_create_table_system_dblog extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_dblog}}', [
            'id' => $this->primaryKey(),
            'event_id' => $this->smallInteger()->notNull(),
            'data' => $this->text()->notNull(),
            'model' => $this->string()->notNull(),
            'model_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_dblog', '{{%system_dblog}}', ['model_id', 'user_id', 'created_at']);
    }

    public function down()
    {
        $this->dropTable('{{%system_dblog}}');
    }
}
