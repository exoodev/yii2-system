<?php

use yii\db\Migration;

class m190210_212341_create_table_blog_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%blog_category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'status_id' => $this->smallInteger()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_blog_category-slug', '{{%blog_category}}', 'slug');
    }

    public function down()
    {
        $this->dropTable('{{%blog_category}}');
    }
}
