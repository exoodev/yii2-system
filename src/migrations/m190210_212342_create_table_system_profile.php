<?php

use yii\db\Migration;

class m190210_212342_create_table_system_profile extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_profile}}', [
            'user_id' => $this->integer()->notNull(),
            'uniq_id' => $this->string()->notNull(),
            'avatar_url' => $this->string(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'birthday' => $this->integer(),
            'gender_id' => $this->smallInteger(),
            'phone' => $this->string(),
            'country' => $this->string(),
            'city' => $this->string(),
            'address' => $this->string()->notNull(),
            'timezone' => $this->string(),
        ], $tableOptions);

        $this->createIndex('user_id', '{{%system_profile}}', 'user_id', true);
        $this->createIndex('profile_index', '{{%system_profile}}', 'user_id');
        $this->createIndex('uniq_id', '{{%system_profile}}', 'uniq_id', true);
        $this->addForeignKey('fk-system_profile-user_id-system_user-id', '{{%system_profile}}', 'user_id', '{{%system_user}}', 'id', 'CASCADE', 'CASCADE');

        $this->insert('{{%system_profile}}', [
            'user_id' => 1,
            'uniq_id' => uniqid(),
            'avatar_url' => null,
            'first_name' => null,
            'last_name' => null,
            'birthday' => null,
            'gender_id' => null,
            'phone' => null,
            'city' => null,
            'country' => null,
            'timezone' => null,
            'address' => '',
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%system_profile}}');
    }
}
