<?php

use yii\db\Migration;

class m190210_212343_create_table_system_video extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_video}}', [
            'id' => $this->primaryKey(),
            'folder_id' => $this->integer()->notNull(),
            'url' => $this->string()->notNull(),
            'position' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->createIndex('idx_system_block', '{{%system_video}}', ['status', 'position']);
        $this->createIndex('folder_id', '{{%system_video}}', 'folder_id');
        $this->addForeignKey('system_video_ibfk_1', '{{%system_video}}', 'folder_id', '{{%system_video_folder}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%system_video}}');
    }
}
