<?php

use yii\db\Migration;

class m190210_212342_create_table_system_auth extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%system_auth}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'source' => $this->string()->notNull(),
            'source_id' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_auth', '{{%system_auth}}', 'user_id');
        $this->addForeignKey('system_auth_ibfk_1', '{{%system_auth}}', 'user_id', '{{%system_user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%system_auth}}');
    }
}
