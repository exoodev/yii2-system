<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\InputWidget;

/**
 * Widget for rendering permissions.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Permissions extends InputWidget
{
	public $items;
	public $model;
	/**
	 * Undocumented property
	 *
	 * @var string
	 */
	public $tableOptions = ['class' => 'uk-table uk-table-divider uk-margin-remove uk-table-justify uk-table-small'];

	/**
     * @inheritdoc
     */
	public function run()
	{
		echo $this->renderApps();
	}

	public function renderApps()
	{
		$result = [];
		$apps = $this->getApps();
		$appsValues = array_values($apps);
		foreach ($apps as $app) {
			$table = Html::tag('table', $this->renderTable($app), $this->tableOptions);
			$result[] = Html::tag('div', $table . "\n");
		}
		return implode("\n", $result);
	}

	public function renderTable($app)
	{
		$items = $this->getAppPermissions($app);
		$appName = Html::tag(
			'td',
			Html::tag('h3', $this->translate($app, $app), [
				'class' => 'uk-margin-bottom-remove'
			])
		);
		$rows[] = Html::tag('tr', $appName . '<td></td>');

		foreach ($items as $permission => $description) {
			$checked = in_array($permission, $this->model->permission);
			$disabled = (!$this->model->hasChildPermission($permission) && $checked) ? true : false;
			$tdA = Html::tag('td', $this->translate($app, $description)) . "\n";
			$checkbox = Html::checkbox($permission, $checked, [
				'uncheck' => $disabled ? null : 0,
				'disabled' => $disabled,
			]);
			$tdB = Html::tag('td', $checkbox, ['class' => 'uk-text-right']) . "\n";
			$rows[] = Html::tag('tr', $tdA . $tdB);
		}
		return implode("\n", $rows);
	}

	protected function getAppPermissions($app)
	{
		$permissions = [];

		foreach ($this->items as $permission) {
			if (isset($permission->data['app']) && $permission->data['app'] == $app) {
				$permissions[$permission->name] = $permission->description;
			}
		}
		return $permissions;
	}

	protected function getApps()
	{
		$apps = [];

		foreach ($this->items as $permission) {
			$apps[] = $permission->data['app'];

		}
		return array_unique($apps);
	}

	/**
	 * Translates string.
	 * @param string $app App name.
	 * @param string $string String to be translated.
	 * @return string the result
	 */
	public function translate($app, $string)
	{
		if (isset(Yii::$app->i18n->translations[strtolower($app)])) {
			return Yii::t(strtolower($app), $string);
		}
		return $string;
	}

}
