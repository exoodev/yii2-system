<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\widgets;

use Yii;
use yii\helpers\Html;
use exoo\widgets\Dropdown;

/**
 * Widget for changing app language.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Language extends Dropdown
{
    /**
     * @var array options for dropdown container
     */
    public $dropdownOptions = ['class' => 'uk-dropdown'];
    /**
     * @var @inheritdoc
     */
    public $encodeLabel = false;
    /**
     * @var string Array list of locales.
     */
    public $items = [];
    /**
     * @var string Action for changing language.
     */
    public $url = '/site/language';

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (empty($this->items)) {
            $this->items = Yii::$app->system->language->localesList;
        }
        if (empty($this->content)) {
            $this->content = $this->renderItems();
        }
        parent::init();
    }

    /**
     * Render languages list.
     */
    public function renderItems()
    {
        $language = Yii::$app->language;
        $items = [];
        foreach ($this->items as $item) {
            $img = '';
            if (!empty($item['image_url'])) {
                $img = Html::img($item['image_url'], ['width' => 20]) . ' ';
            }
            $label = Html::encode($item['name']);
            if ($item['tag'] == $language && !isset($this->toggleOptions['label'])) {
                $this->toggleOptions['label'] = $img . $label;
            }
            $items[] = $img . Html::a($label, [$this->url, 'tag' => $item['tag']], [
                'encode' => false,
            ]);
        }

        return implode('<hr class="uk-margin">', $items);
    }
}
