<?php

use yii\helpers\Html;

?>
<div class="uk-thumbnail uk-overlay-hover uk-border-circle">
    <figure class="uk-overlay">
        <?= Html::img($profile->avatarUrl, [
            'alt' => Html::encode($user->username),
            'width' => 200,
            'height' => 200,
            'class' => 'uk-border-circle',
        ]) ?>
        <?php if ($profile->avatar_url): ?>
        <figcaption class="uk-overlay-panel uk-overlay-background uk-flex uk-flex-center uk-flex-middle uk-text-center uk-border-circle">
            <div>
                <?php
                // Html::a(null, ['/system/user/avatar-delete', 'id' => $user->id], [
                //     'class' => 'uk-icon-button uk-icon-trash uk-text-danger',
                //     'data-method' => 'post',
                //     'data-pjax' => '0',
                //     'alt' => Yii::t('system', 'Delete'),
                // ]);
                ?>
            </div>
        </figcaption>
        <?php endif; ?>
    </figure>
</div>

<div class="uk-margin">
    <?php // $form->field($profile, 'fileAvatar')->fileInput(['class' => 'form-control'])->label(false) ?>
</div>
