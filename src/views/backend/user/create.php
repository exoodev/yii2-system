<?php

use yii\helpers\Html;

$this->title = Yii::t('system', 'Create User');
?>
<?= $this->render('_form', [
    'user' => $user,
    'rolesList' => $rolesList,
    'statuses' => $statuses,
    'profile' => $profile,
]) ?>
