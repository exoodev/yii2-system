<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'id' => 'form-user',
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h1 class="uk-margin-remove"><?= Html::encode($this->title) ?></h1>
        </div>
        <div>
            <?= Html::submitButton(Yii::t('system', 'Save'), [
                'class' => 'uk-button uk-button-success'
            ]) ?>
            <?= Html::a(Yii::t('system', 'Close'), ['index'], [
                'class' => 'uk-button uk-button-default'
            ]) ?>
        </div>
    </div>
    <div class="uk-card uk-card-default uk-card-body">
        <div class="uk-grid-divider" uk-grid>
            <div class="uk-width-auto@m uk-text-center">
                <?= $this->render('_form_avatar', [
                    'user' => $user,
                    'form' => $form,
                    'profile' => $profile,
                ]) ?>
            </div>
            <div class="uk-width-expand@m">
                <?= $this->render('_form_profile', [
                    'user' => $user,
                    'form' => $form,
                    'profile' => $profile,
                    'statuses' => $statuses,
                    'rolesList' => $rolesList,
                ]) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
