<?php

use yii\helpers\Html;
use yii\helpers\Url;
use exoo\uikit\DetailView;

$this->title = Html::encode($user->username);
Url::remember(['view', 'id' => $user->id], 'view');
?>

<div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
    <div>
        <h1 class="uk-margin-remove"><?= Yii::t('system', 'User profile') ?></h1>
    </div>
    <div>
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $user->id], [
            'class' => 'uk-button uk-button-primary'
        ]) ?>
        <?= Html::a(Yii::t('system', 'Close'), ['index'], [
            'class' => 'uk-button uk-button-default'
        ]) ?>
    </div>
</div>
<div class="uk-card uk-card-default uk-card-body">
    <div class="uk-grid-divider" uk-grid>
        <div class="uk-width-auto@m uk-text-center">
            <?= Html::img($profile->avatarUrl, [
                'alt' => Html::encode($user->username),
                'width' => 200,
                'height' => 200,
                'class' => 'uk-thumbnail uk-border-circle',
            ]) ?>
            <h2 class="uk-margin"><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="uk-width-expand@m">
            <ul class="uk-tab" data-uk-tab="{connect:'#profile-switcher'}">
                <li><a href="#"><?= Yii::t('system', 'Registration data') ?></a></li>
                <li><a href="#"><?= Yii::t('system', 'Profile') ?></a></li>
                <li><a href="#"><?= Yii::t('system', 'Log') ?></a></li>
            </ul>

            <ul id="profile-switcher" class="uk-switcher uk-margin">
                <li>
                    <?= DetailView::widget([
                        'model' => $user,
                        'attributes' => [
                            'username',
                            'email',
                            [
                                'attribute' => 'status_id',
                                'format' => 'html',
                                'value' => Html::tag('div', $user->status, [
                                    'class' => 'uk-label uk-label-' . $user->statusCssClass,
                                ]),
                            ],
                            [
                                'attribute' => 'roles',
                                'value' => implode(', ', $user->roles),
                            ]
                        ],
                    ]) ?>
                </li>
                <li>
                    <?= DetailView::widget([
                        'model' => $profile,
                        'attributes' => [
                            'first_name',
                            'last_name',
                            'birthday:date',
                            'gender_id',
                            'phone',
                			'city',
                			'country',
                			'timezone',
                        ],
                    ]) ?>
                </li>
                <li>
                    <?= DetailView::widget([
                        'model' => $user,
                        'attributes' => [
                            'created_at:datetime',
                            'updated_at:datetime',
                            'last_visit:datetime',
                            'ip:ip',
                        ],
                    ]) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
