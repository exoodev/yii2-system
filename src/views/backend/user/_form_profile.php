<?php

use yii\helpers\ArrayHelper;
use exoo\uikit\DetailView;
use exoo\datepicker\DatePicker;
use exoo\select2\Select2;

?>
<ul class="uk-tab" uk-tab="{connect:'#profile-switcher'}">
    <li><a href="#"><?= Yii::t('system', 'Registration data') ?></a></li>
    <li><a href="#"><?= Yii::t('system', 'Profile') ?></a></li>
    <li><a href="#"><?= Yii::t('system', 'Log') ?></a></li>
</ul>

<ul id="profile-switcher" class="uk-switcher">
    <li>
        <?= $form->field($user, 'username')->textInput(['maxlength' => true]) ?>
        <?= $form->field($user, 'email') ?>
        <?= $form->field($user, 'status_id')->dropDownList($statuses) ?>
        <?= $form->field($user, 'roles')->checkboxList(ArrayHelper::map($rolesList, 'name', 'name')) ?>
        <?= $form->field($user, 'password')->passwordInput(['toggle' => true]) ?>
    </li>
    <li>
        <?= $form->field($profile, 'first_name') ?>
        <?= $form->field($profile, 'last_name') ?>
        <?= $form->field($profile, 'birth')->widget(DatePicker::className())->icon('<span uk-icon="icon:calendar"></span>') ?>
        <?= $form->field($profile, 'gender_id')->dropDownList($profile->genderList, ['prompt' => '']) ?>
        <?= $form->field($profile, 'phone')->textInput(['type' => 'tel', 'maxlength' => true]) ?>
        <?= $form->field($profile, 'country')->textInput(['maxlength' => true]) ?>
        <?= $form->field($profile, 'city')->textInput(['maxlength' => true]) ?>
        <?= $form->field($profile, 'address')->textInput(['maxlength' => true]) ?>
        <?= $form->field($profile, 'timezone')->widget(Select2::className(), [
            'items' => Yii::$app->system->timezonesGroup,
        ]) ?>
    </li>
    <li>
        <?= DetailView::widget([
            'model' => $user,
            'attributes' => [
                'created_at:datetime',
                'updated_at:datetime',
                'last_visit:datetime',
                'ip:ip',
            ],
        ]) ?>
    </li>
</ul>
