<?php

use yii\helpers\Html;
use yii\helpers\Url;
use exoo\grid\GridView;

Url::remember(['index'], 'index');

$this->title = Yii::t('system', 'Users');
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'buttons' => [
        Html::a(Yii::t('system', 'Create'), ['create'], [
            'class' => 'uk-button uk-button-success'
        ]),
        Html::a(Yii::t('system', 'Delete'), ['batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data' => [
                'method' => 'post',
                'confirm' => Yii::t('system', 'Are you sure you want to delete the selected items?'),
            ],
            'ex-selected' => true
        ]),
    ],
    'columns' => [
        ['class' => 'exoo\grid\CheckboxColumn'],
        [
            'attribute' => 'id',
            'headerOptions' => ['style' => 'width:50px'],
        ],
        [
            'attribute' => 'profile.avatar_url',
            'format' => 'raw',
            'value' => function($model){
                return Html::img($model->profile->avatarUrl, [
                    'class' => 'uk-border-circle uk-margin-small-right',
                    'alt' => Html::encode($model->username),
                    'height' => 40,
                    'width' => 40,
                ]);
            },
        ],
        [
            'attribute' => 'email',
            'format' => 'html',
            'value' => function($model){
                return Html::a(Html::encode($model->email), [
                    'view', 'id' => $model->id
                ]);
            }
        ],
        'username',
        'last_visit:datetime',
        // [
        //     'attribute' => 'last_visit',
        //     'value' => function($model){
        //         if ($model->last_visit) {
        //             return Yii::$app->formatter->asDateTime($model->last_visit);
        //         }
        //     }
        // ],
        [
            'attribute' => 'status_id',
            'filter' => $statuses,
            'format' => 'html',
            'filterInputOptions' => ['prompt' => 'Все'],
            'value' => function ($model) {
                return Html::tag('div', $model->status, [
                    'class' => 'uk-label uk-label-' . $model->statusCssClass,
                ]);
            },
        ],
    ],
]); ?>
