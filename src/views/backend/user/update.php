<?php

use yii\helpers\Html;

$this->title = Yii::t('system', 'Update User') . ': ' . $user->username;
?>
<?= $this->render('_form', [
    'user' => $user,
    'rolesList' => $rolesList,
    'statuses' => $statuses,
    'profile' => $profile,
]) ?>
