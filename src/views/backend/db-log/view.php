<?php

use yii\helpers\Html;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $model common\modules\system\models\ActiveRecordLog */

if ($model->data) {
    $items = [];
    $data = Json::decode($model->data);
    foreach ($data as $key => $value) {
        $items[] = '<tr><th class="uk-width-1-4">' . $key . '</th><td>' . $value . '</td></tr>';
    }
    $table = Html::tag('table', implode("\n", $items), [
        'class' => 'uk-table uk-table-divider uk-table-small'
    ]);
}
?>
<?= Html::tag('span', $model->event, [
    'class' => 'uk-label uk-label-' . $model->eventLabel
]) . ' ' . $model->model . ' ' . 'ID: ' . $model->model_id ?>
<p class="uk-text-small uk-text-muted">
    <?= Html::icon('user') . ' ' . Html::encode($model->user->username) ?>
    <?= Html::icon('calendar', ['class' => 'uk-margin-left']) . ' ' . Yii::$app->formatter->asDateTime($model->created_at) ?>
</p>
<hr>
<?php if (isset($table)): ?>
    <h3 class="uk-margin-small-top"><?= Yii::t('system', 'Data') ?></h3>
    <?= $table ?>
<?php endif; ?>
