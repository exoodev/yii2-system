<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use exoo\grid\GridView;
use exoo\uikit\Modal;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\modules\system\models\search\ActiveRecordLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('system', 'Database log');
// $this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'containerOptions' => ['class' => 'uk-card uk-card-default'],
        'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => [
                    'class' => 'uk-table-shrink',
                    'style' => 'min-width:50px',
                ],
            ],
            [
                'attribute' => 'event_id',
                'format' => 'html',
                'value' => function($model) {
                    return Html::tag('span', $model->event, [
                        'class' => 'uk-label uk-label-' . $model->eventLabel
                    ]);
                },
                'filter' => $searchModel->events,
                'filterInputOptions' => ['prompt' => Yii::t('system', 'All')],
                'headerOptions' => [
                    'class' => 'uk-table-shrink',
                    'style' => 'min-width:75px',
                ],
            ],
            'model',
            [
                'attribute' => 'model_id',
                'headerOptions' => [
                    'class' => 'uk-table-shrink',
                    'style' => 'min-width:100px',
                ],
            ],
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return Html::encode($model->user->username);
                },
                'headerOptions' => [
                    'class' => 'uk-table-shrink',
                ],
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    return Yii::$app->formatter->asDateTime($model->created_at);
                },
                'headerOptions' => [
                    'class' => 'uk-table-shrink',
                    'style' => 'min-width:200px',
                ],
            ],
            [
                'attribute' => 'Action',
                'label' => false,
                'format' => 'raw',
                'value' => function($model) {
                    return Html::button(Yii::t('system', 'Open'), [
                        'title' => Yii::t('yii', 'View'),
                        'class' => 'uk-button uk-button-default',
                        'uk-tooltip' => true,
                        'data-url' => Url::to(['view', 'id' => $model->id]),
                    ]);
                },
                'headerOptions' => [
                    'class' => 'uk-table-shrink',
                    'style' => 'min-width:150px',
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
<?php Modal::begin([
    'id' => 'viewLog',
    'large' => true
]) ?>
    <div class="uk-modal-header">
        <h2 class="uk-modal-title"><?= Yii::t('system', 'Preview log') ?></h2>
    </div>
    <div class="uk-modal-body" uk-overflow-auto></div>
    <div class="uk-modal-footer uk-text-right">
        <?= Html::button(Yii::t('system', 'Close'), [
            'class' => 'uk-button uk-button-primary uk-modal-close'
        ]) ?>
    </div>
<?php Modal::end() ?>
<?php
$js = <<<JS
$(document).on('click', '[data-url]', function() {
    $('#viewLog .uk-modal-body').load($(this).data('url'), function() {
        UIkit.modal('#viewLog').show();
    });
});
JS;
$this->registerJs($js);
