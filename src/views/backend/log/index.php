<?php

use yii\helpers\Html;
use exoo\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel exoo\system\models\backend\search\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('system', 'Log');
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'containerOptions' => ['class' => 'uk-card uk-card-default'],
        'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
        'buttons' => [
            Html::a(Yii::t('system', 'Delete'), ['batch-delete'], [
                'class' => 'uk-button uk-button-danger uk-hidden',
                'data-ex-grid-action' => [
                    'confirm' => Yii::t('system', 'Are you sure you want to delete the selected items?'),
                ],
            ]),
            Html::a(Yii::t('system', 'Delete all'), ['delete-all'], [
                'class' => 'uk-button uk-button-danger',
                'data' => [
                    'confirm' => Yii::t('system', 'Are you sure you want to delete the all items?'),
                    'method' => 'post',
                ],
            ])
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'exoo\grid\CheckboxColumn'],

            [
                'attribute' => 'id',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->id, ['view', 'id' => $model->id]);
                },
                'headerOptions' => ['class' => 'uk-table-shrink'],
            ],
            [
                'attribute' => 'level',
                'format' => 'html',
                'value' => function($model) {
                    return Html::tag('div', $model->levelName, [
                        'class' => 'uk-label'
                    ]);
                },
            ],

            'category',
            'log_time:datetime',
            [
                'attribute' => 'prefix',
                'format' => 'html',
                'value' => function($model) {
                    return Html::tag('code', Yii::$app->formatter->asNtext($model->prefix));
                },
            ],
        ],
    ]); ?>
