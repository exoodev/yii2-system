<?php

use yii\helpers\Html;
use exoo\uikit\DetailView;

/* @var $this yii\web\View */
/* @var $model exoo\system\models\backend\Log */

$this->title = $model->category;
// $this->params['breadcrumbs'][] = ['label' => Yii::t('system', 'Logs'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
:not(pre)>code, :not(pre)>kbd, :not(pre)>samp {
    white-space: inherit;
}
CSS;
$this->registerCss($css);

?>

<div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
    <div>
        <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
    </div>
    <div>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'uk-button uk-button-danger',
            'data' => [
                'confirm' => Yii::t('system', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
</div>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        // 'id',
        [
            'attribute' => 'level',
            'format' => 'html',
            'value' => Html::tag('div', $model->levelName, ['class' => 'uk-label']) . ' ID: ' . $model->id
        ],
        'category',
        'log_time:datetime',
        [
            'attribute' => 'prefix',
            'format' => 'html',
            'value' => Html::tag('code', Yii::$app->formatter->asNtext($model->prefix))
        ],
        [
            'attribute' => 'message',
            'format' => 'html',
            'value' => Html::tag('code', Yii::$app->formatter->asNtext($model->message))
        ]
    ],
]) ?>
