<?php
use yii\helpers\Html;
use exoo\widgets\Nav;

$items = [
    [
        'header' => 'Введение'
    ],
    [
        'url' => '/system/docs/about',
        'label' => 'О EXOO CMS',
    ],
    [
        'header' => 'Подготовка и установка'
    ],
    [
        'url' => ['docs/install'],
        'label' => 'Установка CMS',
    ],
    [
        'url' => ['docs/installExt'],
        'label' => 'Установка расширений',
    ],
    [
        'url' => ['docs/update'],
        'label' => 'Обновление',
    ],
    [
        'url' => ['docs/runApp'],
        'label' => 'Запуск приложения',
    ],
    [
        'header' => 'Управление ролями и пользователями'
    ],
    [
        'url' => ['docs/rbac'],
        'label' => 'Управление ролями и разрешениями',
    ],
    [
        'url' => ['docs/users'],
        'label' => 'Управление пользователями',
    ],
    [
        'header' => 'Настройки'
    ],
    [
        'url' => '#',
        'label' => 'Информация о приложении',
    ],
    [
        'url' => '#',
        'label' => 'Локализация',
    ],
    [
        'url' => '#',
        'label' => 'Настройка почтовой службы',
    ],
    [
        'url' => '#',
        'label' => 'Конфигурация регистрации',
    ],
    [
        'url' => '#',
        'label' => 'Кэш',
    ],
];
?>

<?= Nav::widget([
    'items' => $items,
    'encodeLabels' => false,
    'type' => 'navside',
]) ?>
