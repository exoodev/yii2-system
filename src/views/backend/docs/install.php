<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Установка EXOO CMS';

$this->beginBlock('sidebar-a');
echo $this->render('_sidebar');
$this->endBlock();
?>

<h1><?= $this->title ?></h1>
<p>
    Установка EXOO CMS происходит с использованием менеджера пакетов <?= Html::a('Composer', Url::to('https://getcomposer.org', true), ['target' => '_blank']) ?>
    (<?= Html::a('Как установить Composer', Url::to('https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx', true), ['target' => '_blank']) ?>).
    Так же Composer применяется для установки расширений и обновления CMS, используя для этого несколько команд в консольной строке.
</p>
<h3>Получение ключа развертывания (в процессе)</h3>
<p>
    После приобретения програмнного продукта Вы получите файл-ключ <strong>id_rsa</strong>
    необходимый для установки и дальнейшей технического обслуживания.
</p>
<div class="uk-alert uk-alert-danger" data-uk-alert>
    <p><?= Html::icon('uk-icon-warning uk-icon-medium uk-margin-right') .
    'Внимание! Данный файл-ключ будет привязан к Вашей учетной записи и платежному документу на сайта ' .
    Html::a('exoodev.com', Url::to('http://exoodev.com', true), ['target' => '_blank']) . '. Не передавайте данный ключ третьим лицам!' ?></p>
</div>
<h3>Настройка рабочего пространства</h3>
<h4>Настройка сервера на CentOS 7</h4>
<p>
    Подготовка сервера - один из важных пунктов предшествующий установки, который нельзя опустить.
    Для настройки сервера, Вам необходимо следовать следующим пункам:
    <ol>
        <li>
            Создать пользователя <em>web</em>. Это можно сделать через панель
            управления на хостинге, или с помощью ввода команды в консоль:
<pre><code>useradd web</code></pre>
        </li>
        <li>
            Разместите файл-ключ id_rsa в папке <code>/home/web/.ssh</code> удобным для Вас способом.
            Если данный файл-ключ отсутствует - его необходимо сгененировать.
<pre><code>su web
cd ~/.ssh
ssh-keygen</code></pre>
            После этого будет предложено ввести имя файла с ключами, либо если имя файла не указать, ключи будут созданы в файле id_rsa.
            Ключи будут храниться в домашней директории пользователя <code>~/.ssh/id_rsa</code> и <code>~/.ssh/id_rsa.pub</code>.
            <p>
                По умолчанию при подключении к серверу для ssh-авторизации используется ключ с именем id_rsa.
                Если было указано другое имя, отличное от id_rsa, отредактируйте файл  <code>~/.ssh/config</code> (если файла ещё нет, создайте),
                добавьте содержимое для идентификации bitbucket-хоста:
<pre><code>Host bitbucket.org
    IdentityFile ~/.ssh/keyname_rsa</code></pre>
            </p>
            <div class="uk-alert">
                Содержимое файла id_rsa.pub необходимо передать support@exodev.com (?)
            </div>
        </li>
        <li>
            Убедитесь что Вы работает от имени учетной записи <em>web</em>.
            Изменить права на папку <code>/home/web</code> на 0755. Команда для
            выполнения:
<pre><code>chown -R 755 /home/web</code></pre>
        </li>
        <li>
            Внести в системные переменные путь до php
<pre><code>PATH=$PATH:/usr/local/zend/bin &amp;&amp; export PATH</code></pre>
        </li>
        <li>
            Установите Git
<pre><code>yum - y update &amp;&amp; yum -y install git</code></pre>
        </li>
        <li>
            Если Composer еще не установлен, установите его с помощью следующих команд
<pre><code>curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer</code></pre>
            <div class="uk-alert">
                Если у Вас возникли трудности с установкой Composer, обратитесь к инструкции на стайте
                <?= Html::a('getcomposer.org/download', Url::to('https://getcomposer.org/download/', true), ['target' => '_blank']) ?>,
            </div>
        </li>
        <li>
            Установите <code>composer-asset-plugin</code>
            введя в консоли следующую команду из под web-директории, в которую будет устанавливаться EXOO CMS:
<pre><code>composer global require "fxp/composer-asset-plugin:^1.2.0"</code></pre>
        </li>
    </ol>
</p>
<h3>Рекомендуемые настройки web-сервера Apache</h3>
<p>
    Используйте следующую конфигурацию в <code>httpd.conf</code> файле, или в файле концигуриции виртуальных хостов.
</p>
<div class="uk-alert">
    Примечение. Замените <code>path/to/frontend/web</code>
    и <code>path/to/backend/web</code> на актуальный путь до <code>app/web</code>.
</div>
<pre><code>&lt;VirtualHost *:80&gt;
    ServerName your-domain-name.com
    ServerAlias your-domain-name.com
    DocumentRoot &quot;path/to/frontend/web&quot;
    &lt;Directory &quot;path/to/frontend/web&quot;&gt;
	    RewriteEngine on
	    RewriteCond %{REQUEST_FILENAME} !-f
	    RewriteCond %{REQUEST_FILENAME} !-d
	    RewriteRule . index.php

	    Options All -Indexes
	    AllowOverride none
    	Require all granted
	&lt;/Directory&gt;
&lt;/VirtualHost&gt;

&lt;VirtualHost *:80&gt;
    ServerName admin.your-domain-name.com
    DocumentRoot &quot;path/to/backend/web&quot;
    &lt;Directory &quot;path/to/backend/web&quot;&gt;
	    RewriteEngine on
	    RewriteCond %{REQUEST_FILENAME} !-f
	    RewriteCond %{REQUEST_FILENAME} !-d
	    RewriteRule . index.php

	    Options All -Indexes
	    AllowOverride none
    	Require all granted
	&lt;/Directory&gt;
&lt;/VirtualHost&gt;</code></pre>

<h3>Установка CMS</h3>
<p>
    Убедитесь что Вы находитесь в папке <code>/home/web</code>, после чего введи команду <code>git clone</code> для получения EXOO CMS
<pre><code>git clone git@bitbucket.org:exoo/exoocms.git</code></pre>
    По завершению копирования файлов из репозитория,
    необходимо произвести инициализацию приложения и получить расширения из списка зависимостей в файле <code>composer.json</code>.
<pre><code>cp /home/web/exoocms
composer install</code></pre>
</p>
