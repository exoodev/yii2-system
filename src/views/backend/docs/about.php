<?php
use yii\helpers\Html;

$this->title = 'CMS documentation';

$this->beginBlock('sidebar-a');
echo $this->render('_sidebar');
$this->endBlock();
?>

<h1>The Definitive Documentation to EXOO CMS</h1>
<h3>What is EXOO CMS?</h3>
<p>
    EXOO CMS is based on php framework Yii2, one of the most populare, safety and flexible.
</p>
<h3>About CMS</h3>
<p>
    CMS was developed from professional to the end users, with set of ultimate features
    and extensions. EXOO CMS presents the following features:
    <ul>
        <li>High degree of security against burglary</li>
        <li>Flexible structure that makes it easy to expand the system</li>
        <li>A wide range of tools for managing CMS</li>
        <li>Manage users and they permissions</li>
        <li>Сonvenient and intuitive dashboard</li>
    </ul>
</p>
<h3>List of extensions that extend the capabilities of the system:</h3>
<p>
    <ul>
        <li>Blog - used to display news block</li>
        <li>Forum - communication and discussion</li>
        <li>Company - all in one: online store, inventory control and accounting documents</li>
        <li>Kit - set of widgets and all for the personalization site</li>
    </ul>
</p>
<h3>What is EXOO CMS best for?</h3>
<p>
    Through convenient and efficient EXOO CMS can be used for as a tool for small
    projects as well as for corporate sites and heavy duty systems.
</p>
