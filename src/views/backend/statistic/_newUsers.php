<?php

use yii\helpers\Url;

$name = Yii::t('system', 'New users');
$url = Url::to(['/system/statistic/data']);

$js = <<<JS
function newUsers() {
    $('#newUsers').html(spinner)
    var chart = request('$url', {
        mode: 'bytime',
        metrics: 'ym:s:newUsers',
        dimensions: [],
        date2: 'today',
        sort: 'ym:s:newUsers',
        group: 'day'
    });

    chart.done(function(response) {
        Highcharts.chart('newUsers', {
            title: false,
            chart: {
                type: 'spline',
                height: '200px'
            },
            xAxis: {
                type: 'datetime'
            },
            plotOptions: {
                series: {
                    pointStart: Date.parse(response.query.date1),
                    pointIntervalUnit: 'day'
                }
            },
            yAxis: {
                title: false,
                allowDecimals: false
            },
            series: [{
                showInLegend: false,
                name: '$name',
                color: '#8cc14c',
                data: response.data[0].metrics[0]
            }]
        });
    });

    var count = request('$url', {
        metrics: 'ym:s:newUsers',
        dimensions: [],
        date2: 'today',
        sort: 'ym:s:newUsers',
        group: 'day'
    });

    count.done(function(response) {
        $('#newUsersCount').html(response.data[0].metrics[0]);
    });
}
JS;
$this->registerJs($js);
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <div class="uk-card-badge">
        <span class="uk-label" id="newUsersCount"></span>
    </div>
    <h3 class="uk-card-title"><?= $name ?></h3>
    <div id="newUsers"></div>
</div>
