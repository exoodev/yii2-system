<?php

use yii\helpers\Url;

$name = Yii::t('system', 'Age');
$visits = Yii::t('system', 'Visits');
$url = Url::to(['/system/statistic/data']);


$js = <<<JS
function age() {
    $('#ageChart').html(spinner)
    var count = request('$url', {
        metrics: 'ym:s:visits',
        dimensions: 'ym:s:ageInterval',
        date2: 'today',
        sort: '-ym:s:visits',
        group: 'Week'
    });

    count.done(function(response) {
        var data = response.data,
            total = 0,
            series = [],
            categories = [];

        $.each(data, function() {
            total += parseInt(this.metrics[0], 10);
        });

        $.each(data, function() {
            var name = this.dimensions[0].name,
                value = this.metrics[0],
                percent = Number((value * 100 / total).toFixed(2));

            series.push({y: percent, v: value});
            categories.push(name);
        });

        Highcharts.chart('ageChart', {
            title: false,
            chart: {
                type: 'bar'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.v} ({point.y:.1f}%)</b>'
            },
            xAxis: {
                categories: categories,
                title: {
                    text: null
                }
            },
            yAxis: {
                visible: false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f} %'
                    }
                }
            },
            series: [{
                name: '$visits',
                showInLegend: false,
                colorByPoint: true,
                data: series
            }]
        });
    });
}
JS;
$this->registerJs($js);
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <h3 class="uk-card-title uk-margin-remove"><?= $name ?></h3>
    <p class="uk-article-meta uk-margin-remove"><?= $visits ?></p>
    <div id="ageChart" style="height:415px"></div>
</div>
