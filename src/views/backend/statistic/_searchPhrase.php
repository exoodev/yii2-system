<?php

use yii\helpers\Url;

$name = Yii::t('system', 'Last search phrase');
$url = Url::to(['/system/statistic/data']);

$css = <<<CSS
#searchPhraseTable {
    font-size: 13px;
    table-layout: fixed;
}
.uk-table td, .uk-table th {
    border: none;
    border-bottom: 1px solid #E5E5E5;
}
#searchPhraseTable td+td, #searchPhraseTable th+th {
    text-align: right;
}
.icon-page {
    width: 16px;
    height: 16px;
    display: inline-block;
    margin-right: 5px;
    vertical-align: middle;
}
CSS;
$this->registerCss($css);
$js = <<<JS
function searchPhrase() {
    var count = request('$url', {
        metrics: 'ym:s:visits',
        dimensions: 'ym:s:searchPhrase',
        date2: 'today',
        sort: '-ym:s:visits',
        group: 'Week',
        limit: 10
    });

    count.done(function(response) {
        var table = $('#searchPhraseTable tbody');
        table.empty();

        $.each(response.data, function() {
            var item = this.dimensions[0],
                value = this.metrics[0],
                link = '<a href="' + item.url + '" target="_blank" title="' + item.name + '" uk-tooltip>' + item.name + '</a>',
                icon = '<i class="icon-page" style="background-image: url(//favicon.yandex.net/favicon/' + item.favicon + '/)"></i>';

            table.append('<tr><td class="uk-text-truncate">' + icon + link + '</td><td>' + value + '</td></tr>');
        });
    });
}
JS;
$this->registerJs($js);
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin">
    <h3 class="uk-card-title uk-margin-remove"><?= $name ?></h3>
    <table id="searchPhraseTable" class="uk-table uk-table-striped uk-table-condensed">
        <thead>
            <tr>
                <th><?= Yii::t('system', 'Phrase') ?></th>
                <th><?= Yii::t('system', 'Visits') ?></th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
