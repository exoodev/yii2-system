<?php

use yii\helpers\Url;

$name = Yii::t('system', 'Users ');
$url = Url::to(['/system/statistic/data']);

$js = <<<JS
function users() {
    $('#users').html(spinner)
    var chart = request('$url', {
        mode: 'bytime',
        metrics: 'ym:s:users',
        dimensions: [],
        date2: 'today',
        sort: 'ym:s:users',
        group: 'day'
    });

    chart.done(function(response) {
        Highcharts.chart('users', {
            title: false,
            chart: {
                type: 'spline',
                height: '200px'
            },
            xAxis: {
                type: 'datetime'
            },
            plotOptions: {
                series: {
                    pointStart: Date.parse(response.query.date1),
                    pointIntervalUnit: 'day'
                }
            },
            yAxis: {
                title: false,
                allowDecimals: false
            },
            series: [{
                showInLegend: false,
                name: '$name',
                color: '#00a8e6',
                data: response.data[0].metrics[0]
            }]
        });
    });

    var count = request('$url', {
        metrics: 'ym:s:users',
        dimensions: [],
        date2: 'today',
        sort: 'ym:s:users',
        group: 'day'
    });

    count.done(function(response) {
        $('#usersCount').html(response.data[0].metrics[0]);
    });
}
JS;
$this->registerJs($js);
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <div class="uk-card-badge uk-label"><span id="usersCount"></span></div>
    <h3 class="uk-card-title"><?= $name ?></h3>
    <div id="users"></div>
</div>
