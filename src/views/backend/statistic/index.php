<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::t('system', 'Statistic');
$this->registerJsFile('https://code.highcharts.com/highcharts.js');
$lang = substr(Yii::$app->language, 0, 2);
$period = Yii::$app->session->get('stat.period', 7);
$counterUrl = Url::to(['/system/statistic/counter']);

$js = <<<JS
var statistic = $('#statistic'),
    button = 'button[name="period"]',
    period = '$period',
    lang = '$lang';

var spinner = '<div class="uk-height-small uk-flex uk-flex-center" uk-spinner ratio="2"></div>'

$.post('$counterUrl').done(function(response) {
    $('#counterSite').html(response.counter.site);
});

getData();

$(document).on('click', button, function() {
    var e = $(this);
    period = e.attr('value');
    getData();
});

function getData() {
    setActiveButton();
    setDefaultOptions();
    users();
    newUsers();
    traffic();
    bounceRate();
    pageDepth();
    visitDuration();
    devices();
    age();
    pages();
    searchPhrase();
}
function request(url, data) {
    data.period = period;
    data.lang = lang;
    return $.post(url, data);
}
function setActiveButton() {
    statistic.find(button).removeClass('uk-button-primary');
    statistic.find('button[value="' + period + '"]').addClass('uk-button-primary');
}

function setDefaultOptions() {
    var options = {
        credits: {
            enabled: false
        },
        title: {
            text: false
        },
    };

    if (lang == 'ru') {
        options.lang = {
            loading: 'Загрузка...',
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
            exportButtonTitle: "Экспорт",
            printButtonTitle: "Печать",
            rangeSelectorFrom: "С",
            rangeSelectorTo: "По",
            rangeSelectorZoom: "Период",
            downloadPNG: 'Скачать PNG',
            downloadJPEG: 'Скачать JPEG',
            downloadPDF: 'Скачать PDF',
            downloadSVG: 'Скачать SVG',
            printChart: 'Напечатать график'
        };
    }

    Highcharts.setOptions(options);
}

JS;
$this->registerJs($js);
?>
<div id="statistic">
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-margin-remove"><?= Html::encode($this->title) ?></h3>
            <span id="counterSite"></span>
            <a href="https://metrika.yandex.ru/dashboard?id=<?= $id ?>" target="_blank">(<?= $id ?>)</a>
        </div>
        <div>
            <?= Html::button(Yii::t('system', 'Week'), [
                'class' => 'uk-button uk-button-default',
                'name' => 'period',
                'value' => 7
            ]) ?>
            <?= Html::button(Yii::t('system', 'Month'), [
                'class' => 'uk-button uk-button-default',
                'name' => 'period',
                'value' => 30
            ]) ?>
            <?= Html::button(Yii::t('system', 'Quarter'), [
                'class' => 'uk-button uk-button-default',
                'name' => 'period',
                'value' => 90
            ]) ?>
            <?= Html::button(Yii::t('system', 'Year'), [
                'class' => 'uk-button uk-button-default',
                'name' => 'period',
                'value' => 365
            ]) ?>
        </div>
    </div>
    <div class="uk-child-width-1-2@m" uk-grid>
        <div>
            <?= $this->render('_users') ?>
        </div>
        <div>
            <?= $this->render('_newUsers') ?>
        </div>
    </div>

    <div class="uk-child-width-1-3@m" uk-grid uk-margin>
        <div>
            <?= $this->render('_traffic') ?>
        </div>
        <div uk-margin>
            <?= $this->render('_bounceRate') ?>
            <?= $this->render('_pageDepth') ?>
            <?= $this->render('_visitDuration') ?>
        </div>
        <div>
            <?= $this->render('_age') ?>
        </div>
    </div>

    <div class="uk-child-width-1-2@m" uk-grid>
        <div>
            <?= $this->render('_devices') ?>
            <?= $this->render('_searchPhrase') ?>
        </div>
        <div>
            <?= $this->render('_pages') ?>
        </div>
    </div>
</div>

<?php
$css = <<<CSS
#statistic .uk-card-title {
    font-size: 1.1rem;
    line-height: 1;
}
#statistic .uk-card-badge {
    top: 15px;
    right: 15px;
}
CSS;
$this->registerCss($css);
