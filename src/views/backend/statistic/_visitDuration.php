<?php

use yii\helpers\Url;

$name = Yii::t('system', 'Time on site');
$url = Url::to(['/system/statistic/data']);

$js = <<<JS
function visitDuration() {
    $('#visitDuration').html(spinner)
    var currentValue;
    var chart = request('$url', {
        mode: 'bytime',
        metrics: 'ym:s:avgVisitDurationSeconds',
        dimensions: [],
        date2: 'today',
        sort: '-ym:s:avgVisitDurationSeconds',
        group: 'day'
    });

    chart.done(function(response) {
        Highcharts.chart('visitDuration', {
            title: false,
            chart: {
                type: 'area',
                height: '80px',
                marginBottom: 10,
                spacingBottom: 0
            },
            tooltip: {
                formatter: function() {
                    return this.series.name + ': ' + getTime(this.point.y)
                }
            },
            plotOptions: {
                series: {
                    pointStart: Date.parse(response.query.date1),
                    pointIntervalUnit: 'day',
                    lineWidth: 1,
                    lineColor: '#60c5e2'
                },
                area: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            xAxis: {
                type: 'datetime',
                visible: false
            },
            yAxis: {
                title: false,
                allowDecimals: false,
                visible: false
            },
            series: [{
                showInLegend: false,
                name: '$name',
                color: '#addbec',
                data: response.data[0].metrics[0]
            }]
        });
        currentValue = response.totals[0][0];
        $('#visitDurationCount').html(getTime(currentValue));

        var count = request('$url', {
            metrics: 'ym:s:avgVisitDurationSeconds',
            dimensions: [],
            date1: parseInt(period) * 2 + 'daysAgo',
            date2: period + 'daysAgo',
            sort: '-ym:s:avgVisitDurationSeconds',
            group: 'day'
        });

        count.done(function(response) {
            var cssClass,
                el = $('#visitDurationCountDiff'),
                result = (((currentValue * 100) / response.totals[0]) - 100).toFixed(2);

            if (result < 0) {
                el.removeClass('uk-label-success').addClass('uk-label-danger');
            } else {
                result = '+' + result;
                el.removeClass('uk-label-danger').addClass('uk-label-success');
            }

            el.html(result + '%');
        });
    });

    function getTime(min) {
        min = Math.floor(min);
        var m = min % 60,
            h = (min - m) / 60,
            z = m < 10 ? '0' : '';
        return h + ':' + z + m;
    }
}
JS;
$this->registerJs($js);
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <div class="uk-card-badge">
        <span class="uk-label uk-text-small" id="visitDurationCountDiff"></span>
        <span class="uk-label" id="visitDurationCount"></span>
    </div>
    <h3 class="uk-card-title"><?= $name ?></h3>
    <div id="visitDuration"></div>
</div>
