<?php

use yii\helpers\Url;

$name = Yii::t('system', 'Pages');
$url = Url::to(['/system/statistic/data']);

$css = <<<CSS
#pagesTable {
    font-size: 13px;
    table-layout: fixed;
}
.uk-table td, .uk-table th {
    border: none;
    border-bottom: 1px solid #E5E5E5;
}
#pagesTable td+td, #pagesTable th+th {
    text-align: right;
}
.icon-page {
    width: 16px;
    height: 16px;
    display: inline-block;
    margin-right: 5px;
    vertical-align: middle;
}
CSS;
$this->registerCss($css);
$js = <<<JS
function pages() {
    var count = request('$url', {
        metrics: 'ym:pv:pageviews',
        dimensions: 'ym:pv:URLHash',
        date2: 'today',
        sort: '-ym:pv:pageviews',
        group: 'Week',
        limit: 10
    });

    count.done(function(response) {
        var table = $('#pagesTable tbody');
        table.empty();

        $.each(response.data, function() {
            var table = $('#pagesTable tbody'),
                item = this.dimensions[0],
                value = this.metrics[0],
                name = item.name.replace(/https?:\/\//gi,''),
                link = '<a href="' + item.name + '" target="_blank" title="' + name + '" uk-tooltip>' + name + '</a>',
                icon = '<i class="icon-page" style="background-image: url(//favicon.yandex.net/favicon/' + item.favicon + '/)"></i>';
            table.append('<tr><td class="uk-text-truncate">' + icon + link + '</td><td>' + value + '</td></tr>');
        });
    });
}
JS;
$this->registerJs($js);
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin">
    <h3 class="uk-card-title uk-margin-remove"><?= $name ?></h3>
    <table id="pagesTable" class="uk-table uk-table-striped uk-table-condensed">
        <thead>
            <tr>
                <th class=""><?= Yii::t('system', 'Page address') ?></th>
                <th><?= Yii::t('system', 'Views') ?></th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
