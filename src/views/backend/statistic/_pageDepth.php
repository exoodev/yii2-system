<?php

use yii\helpers\Url;

$name = Yii::t('system', 'Page depth');
$url = Url::to(['/system/statistic/data']);

$js = <<<JS
function pageDepth() {
    $('#pageDepth').html(spinner)
    var currentValue;
    var chart = request('$url', {
        mode: 'bytime',
        metrics: 'ym:s:pageDepth',
        dimensions: [],
        date2: 'today',
        sort: '-ym:s:pageDepth',
        group: 'day'
    });

    chart.done(function(response) {
        Highcharts.chart('pageDepth', {
            title: false,
            chart: {
                type: 'area',
                height: '80px',
                marginBottom: 10,
                spacingBottom: 0
            },
            tooltip: {
                pointFormat: '{series.name}: {point.y:.1f} %'
            },
            plotOptions: {
                series: {
                    pointStart: Date.parse(response.query.date1),
                    pointIntervalUnit: 'day',
                    lineWidth: 1,
                    lineColor: '#60c5e2'
                },
                area: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            xAxis: {
                type: 'datetime',
                visible: false
            },
            yAxis: {
                title: false,
                allowDecimals: false,
                visible: false
            },
            series: [{
                showInLegend: false,
                name: '$name',
                color: '#addbec',
                data: response.data[0].metrics[0]
            }]
        });
        currentValue = response.totals[0][0];
        $('#pageDepthCount').html(currentValue.toFixed(2) + '%');

        var count = request('$url', {
            metrics: 'ym:s:pageDepth',
            dimensions: [],
            date1: parseInt(period) * 2 + 'daysAgo',
            date2: period + 'daysAgo',
            sort: '-ym:s:pageDepth',
            group: 'day'
        });

        count.done(function(response) {
            var cssClass,
                el = $('#pageDepthCountDiff'),
                result = (currentValue - response.totals[0]).toFixed(2);

            if (result < 0) {
                el.removeClass('uk-label-success').addClass('uk-label-danger');
            } else {
                result = '+' + result;
                el.removeClass('uk-label-danger').addClass('uk-label-success');
            }

            el.html(result + '%');
        });
    });
}
JS;
$this->registerJs($js);
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <div class="uk-card-badge">
        <span class="uk-label uk-text-small" id="pageDepthCountDiff"></span>
        <span class="uk-label" id="pageDepthCount"></span>
    </div>
    <h3 class="uk-card-title"><?= $name ?></h3>
    <div id="pageDepth"></div>
</div>
