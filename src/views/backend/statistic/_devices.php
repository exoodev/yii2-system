<?php

use yii\helpers\Url;

$name = Yii::t('system', 'Devices');
$visits = Yii::t('system', 'Visits');
$url = Url::to(['/system/statistic/data']);


$js = <<<JS
function devices() {
    $('#devicesChart').html(spinner)
    var count = request('$url', {
        metrics: 'ym:s:visits',
        dimensions: 'ym:s:deviceCategory',
        date2: 'today',
        sort: '-ym:s:visits'
    });

    count.done(function(response) {
        var data = response.data,
            total = 0,
            series = [];

        $.each(data, function() {
            total += parseInt(this.metrics[0], 10);
        });

        $.each(data, function() {
            var name = this.dimensions[0].name,
                value = this.metrics[0],
                percent = Number((value * 100 / total).toFixed(2));

            series.push({name, y: percent, v: value});
        });

        Highcharts.chart('devicesChart', {
            title: false,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.v} ({point.percentage:.1f}%)</b>'
            },
            legend: {
                align: 'left',
                itemStyle: {
                    font: 'normal 13px / 26px "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: '$visits',
                colorByPoint: true,
                data: series
            }]
        });
    });
}
JS;
$this->registerJs($js);
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <h3 class="uk-card-title uk-margin-remove"><?= $name ?></h3>
    <p class="uk-article-meta uk-margin-remove"><?= $visits ?></p>
    <div id="devicesChart"></div>
    <!-- <div id="devicesCounts"></div> -->
</div>
