<?php

use yii\helpers\Url;

$name = Yii::t('system', 'Bounce rate');
$url = Url::to(['/system/statistic/data']);

$js = <<<JS
function bounceRate() {
    $('#bounceRate').html(spinner)
    var currentValue;
    var chart = request('$url', {
        mode: 'bytime',
        metrics: 'ym:s:bounceRate',
        dimensions: [],
        date2: 'today',
        sort: '-ym:s:bounceRate',
        group: 'day'
    });

    chart.done(function(response) {
        Highcharts.chart('bounceRate', {
            title: false,
            chart: {
                type: 'area',
                height: '80px'
            },
            tooltip: {
                pointFormat: '{series.name}: {point.y:.1f} %'
            },
            plotOptions: {
                series: {
                    pointStart: Date.parse(response.query.date1),
                    pointIntervalUnit: 'day',
                    lineWidth: 1,
                    lineColor: '#60c5e2'
                },
                area: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                column: {
                    pointPadding: 0,
                    borderWidth: 0
                }
            },
            xAxis: {
                type: 'datetime',
                visible: false
            },
            yAxis: {
                title: false,
                allowDecimals: false,
                visible: false
            },
            series: [{
                showInLegend: false,
                name: '$name',
                color: '#addbec',
                data: response.data[0].metrics[0]
            }]
        });
        currentValue = response.totals[0][0];
        $('#bounceRateCount').html(currentValue.toFixed(2) + '%');

        var count = request('$url', {
            metrics: 'ym:s:bounceRate',
            dimensions: [],
            date1: parseInt(period) * 2 + 'daysAgo',
            date2: period + 'daysAgo',
            sort: '-ym:s:bounceRate',
            group: 'day'
        });

        count.done(function(response) {
            var cssClass,
                el = $('#bounceRateCountDiff'),
                result = (currentValue - response.totals[0]).toFixed(2);

            if (result < 0) {
                el.removeClass('uk-label-danger').addClass('uk-label-success');
            } else {
                result = '+' + result;
                el.removeClass('uk-label-success').addClass('uk-label-danger');
            }

            el.html(result + '%');
        });
    });
}
JS;
$this->registerJs($js);
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <div class="uk-card-badge">
        <span class="uk-label uk-text-small" id="bounceRateCountDiff"></span>
        <span class="uk-label" id="bounceRateCount"></span>
    </div>
    <h3 class="uk-card-title"><?= $name ?></h3>
    <div id="bounceRate"></div>
</div>
