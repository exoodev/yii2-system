<?php

use exoo\widgets\JoditEditor;
use exoo\markdowneditor\MarkdownEditor;

$variationModels = $model->variationModels;
$showLocales = count($variationModels) > 1 ? '' : ' uk-hidden';
?>
<div class="uk-child-width-expand@m uk-grid-medium" uk-grid>
    <div>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div>
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="uk-grid-medium" uk-grid>
    <div class="uk-width-auto@m<?= $showLocales ?>">
        <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
            <?php foreach ($variationModels as $index => $variationModel): ?>
                <li><a href="#"><?= $variationModel->language->url ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="uk-width-expand@m">
        <ul id="component-tab-left" class="uk-switcher">
            <?php foreach ($variationModels as $index => $variationModel): ?>
            <li>
                <?= $form
                    ->field($variationModel, "[{$index}]title")
                    ->textInput(['maxlength' => true])
                    ->label($variationModel->getAttributeLabel('title'))
                ?>
                <?= $form
                    ->field($variationModel, "[{$index}]content")
                    ->widget(MarkdownEditor::className(), [
                        'clientOptions' => [
                            // 'marked' => new \yii\web\JsExpression('App.marked')
                        ]
                    ])
                    ->label($variationModel->getAttributeLabel('content'))
                ?>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php
$js = <<<JS
var App = {
    init: function() {
        this._reMarked()
    },
    _reMarked: function() {
        var renderer = new marked.Renderer()

        // link
        var linkRenderer = renderer.link
        renderer.link = function(href, title, text) {
            var link = linkRenderer.call(renderer, href, title, text)
            return link.replace(/^<a /, '<a target="_blank" rel="nofollow noopener noreferrer" ')
        }

        // image
        var imageRenderer = renderer.image
        renderer.image = function(href, title, text) {
            var image = $(imageRenderer.call(renderer, href, title, text))
            return '<span uk-lightbox><a href="' + image.attr('src') + '" data-caption="' + image.attr('alt') + '" rel="noopener noreferrer">' + image.prop('outerHTML') + '</a></span>'
        }

        marked.setOptions({ renderer })
    }
}
App.init()
JS;
$this->registerJs($js);
