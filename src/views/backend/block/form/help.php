<h3>Шорткоды для контента</h3>
<table class="uk-table uk-table-divider">
    <tbody>
        <thead>
            <tr>
                <th>Код</th>
                <th>Результат</th>
            </tr>
        </thead>
        <tr>
            <td><code>&#91;block slug="test"&#93;</code></td>
            <td>Вывод блока</td>
        </tr>
        <tr>
            <td><code>&#91;company attr="phone.1.number"&#93;</code></td>
            <td>Информация о компании. Раздел <a href="/system/company">"Компания"</a></td>
        </tr>
    </tbody>
</table>
<div class="uk-alert">
    <p>Значения хранятся в конфигурационном файле <code>config/shortcodes.php</code></p>
</div>
