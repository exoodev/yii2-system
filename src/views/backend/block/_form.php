<?php
use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\uikit\Switcher;
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?php $form = ActiveForm::begin(['id' => 'blockForm']); ?>
        <div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
            <div>
                <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
            </div>
            <div>
                <?= $form
                    ->field($model, 'status')
                    ->toggle(['uk-tooltip' => Yii::t('system', 'Status')])
                    ->inline()
                    ->label(false)
                ?>
                <?= Html::submitButton(Yii::t('system', 'Save'), [
                    'class' => 'uk-button uk-button-success'
                ]) ?>
                <?= Html::a(Yii::t('system', 'Close'), Yii::$app->user->returnUrl, [
                    'class' => 'uk-button uk-button-default'
                ]) ?>
                <?php if (!$model->isNewRecord): ?>
                    <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], [
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('system', 'Are you sure want to delete this block?'),
                        'class' => 'uk-button uk-button-danger',
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
        <?= Switcher::widget([
            'type' => 'tab',
            'clientOptions' => ['swiping' => false],
            'items' => [
                [
                    'label' => Yii::t('system', 'Main'),
                    'content' => $this->render('form/main', [
                        'form' => $form,
                        'model' => $model,
                    ]),
                ],
                [
                    'label' => '<i class="far fa-question-circle"></i>',
                    'content' => $this->render('form/help'),
                    'encode' => false
                ],
            ],
        ]) ?>
    <?php ActiveForm::end(); ?>
</div>

<?php
$js = <<<JS
$('#blockForm').on('beforeSubmit', function () {
    var form = $(this)
    $.post(form.attr('action'), form.serializeArray())
        .done(function (data) {
            if (data.success) {
                UIkit.notification(data.success, 'success')
            } else if (data.validation) {
                form.yiiActiveForm('updateMessages', data.validation, true)
            }
        })

    return false
})
JS;
$this->registerJs($js);
