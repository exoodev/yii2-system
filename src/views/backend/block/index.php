<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\grid\GridView;
use exoo\grid\CheckboxColumn;
use exoo\position\PositionColumn;
use exoo\status\StatusColumn;

$this->title = Yii::t('system', 'Blocks');
$positions = Yii::$app->params['positions'];
?>
<?= GridView::widget([
    'id' => 'blockGrid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'pjax' => true,
    'buttons' => [
        Html::a(Yii::t('system', 'Create'), ['create'], [
            'class' => 'uk-button uk-button-success',
            'data-pjax' => 0
        ]),
        Html::a(Yii::t('system', 'Delete'), ['batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data' => [
                'method' => 'post',
                'confirm' => Yii::t('system', 'Are you sure you want to delete the selected items?'),
            ],
            'ex-selected' => true
        ]),
    ],
    'columns' => [
        ['class' => CheckboxColumn::class],
        [
            'attribute' => 'id',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function($model) {
                return Html::a(Html::encode($model->name), ['update', 'id' => $model->id], ['data-pjax' => 0]);
            },
        ],
        'slug',
        ['class' => PositionColumn::class],
        ['class' => StatusColumn::class],
    ],
]); ?>
