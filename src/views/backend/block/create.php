<?php
$this->title = Yii::t('system', 'Creation');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
