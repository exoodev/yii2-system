<?php
$this->title = Yii::t('system', 'Updating language');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
