<?php

use yii\helpers\Html;
use exoo\grid\GridView;

$this->title = Yii::t('system', 'Content languages');

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'buttons' => [
        Html::a(Yii::t('system', 'Create'), ['create'], [
            'class' => 'uk-button uk-button-success'
        ])
    ],
    'columns' => [
        [
            'attribute' => 'id',
            'format' => 'html',
            'value' => function($model) {
                return Html::a(Html::encode($model->id), ['update', 'id' => $model->id]);
            },
        ],
        'url',
        ['class' => 'exoo\position\PositionColumn'],
        [
            'class' => 'exoo\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return Html::a(Html::icon('fas fa-trash-alt'), $url, [
                        'data-confirm' => '<div class="uk-alert uk-alert-danger"><b>' . Yii::t('system', 'Attention!') . '</b><br>' . Yii::t('system', 'When you delete a language, all associated materials in it are deleted in all applications and modules') . '</div><p>' . Yii::t('system', 'Are you sure you want to delete?') . '</p>',
                        'data-method' => 'post',
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-pjax' => '0',
                        'class' => "uk-button uk-button-small uk-button-danger",
                        'style' => 'padding: 0 8px;',
                        'uk-tooltip' => true
                    ]);
                },
            ]
        ],
    ],
]); ?>
