<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div uk-margin>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div uk-margin>
            <?= Html::submitButton(Yii::t('system', 'Save'), [
                'class' => 'uk-button uk-button-success'
            ]) ?>
            <?= Html::a(Yii::t('system', 'Close'), ['index'], [
                'class' => 'uk-button uk-button-default'
            ]) ?>
            <?php if (!$model->isNewRecord): ?>
                <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], [
                    'data-method' => 'post',
                    'data-confirm' => '<div class="uk-alert uk-alert-danger"><b>' . Yii::t('system', 'Attention!') . '</b><br>' . Yii::t('system', 'When you delete a language, all associated materials in it are deleted in all applications and modules') . '</div><p>' . Yii::t('system', 'Are you sure you want to delete?') . '</p>',
                    'class' => 'uk-button uk-button-danger',
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="uk-card uk-card-default uk-card-body">
        <div uk-grid>
            <div class="uk-width-1-2@m">
                <?= $form->field($model, 'id')->dropdownList(Yii::$app->locale->list) ?>
                <?= $form
                    ->field($model, 'url')
                    ->textInput(['maxlength' => true])
                    ->hint(Yii::t('system', 'Language code for the URL') . ': ru, en, de')
                ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php
// $js = <<<JS
// var input = '#language-id';
// setLanguage()
//
// $(document).on('change', input, function() {
//     setLanguage()
// })
//
// function setLanguage() {
//     var value = $(input).val();
//     var charCount = value.replace(/\s/g, '').length;
//
//     if (charCount == 2) {
//         console.log(value);
//         $(input).parent().find('.uk-form-icon').html('<span class="flag-icon flag-icon-'+value+'"></span>')
//     }
// }
// JS;
// $this->registerJs($js);
