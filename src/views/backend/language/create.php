<?php
$this->title = Yii::t('system', 'Adding language');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
