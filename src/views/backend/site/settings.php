<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\htmleditor\HtmlEditor;

$this->title = Yii::t('system', 'Settings');
?>
<?php $form = ActiveForm::begin([
    'id' => 'settingsForm',
    'layout' => 'horizontal',
    'enableAjaxSubmit' => true
]); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div uk-margin>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div uk-margin>
            <?= Html::submitButton(Yii::t('system', 'Save'), [
                'class' => 'uk-button uk-button-success',
            ]) ?>
        </div>
    </div>
    <div class="uk-card uk-card-default uk-card-body">
        <div uk-grid>
            <div class="uk-width-auto@m">
                <ul class="uk-tab-left" uk-tab="connect: #tab-settings; animation: uk-animation-fade">
                    <li><a href="#"><?= Yii::t('system', 'System') ?></a></li>
                    <li><a href="#"><?= Yii::t('system', 'Localization') ?></a></li>
                    <li><a href="#"><?= Yii::t('system', 'Mail') ?></a></li>
                    <li><a href="#"><?= Yii::t('system', 'Registration') ?></a></li>
                    <li><a href="#"><?= Yii::t('system', 'Captcha') ?></a></li>
                    <li><a href="#"><?= Yii::t('system', 'Statistic') ?></a></li>
                    <li><a href="#"><?= Yii::t('system', 'Social network') ?></a></li>
                    <li><a href="#"><?= Yii::t('system', 'SEO') ?></a></li>
                    <li><a href="#"><?= Yii::t('system', 'Cache') ?></a></li>
                </ul>
            </div>
            <div class="uk-width-expand@m">
                <ul id="tab-settings" class="uk-switcher">
                    <li>
                        <?= $form->field($model, 'frontendUrl') ?>
                        <?= $form->field($model, 'storageUrl') ?>
                        <?= $form->field($model, 'logoUrl') ?>
                        <?= $form->field($model, 'offline')->toggle() ?>
                        <?= $form->field($model, 'offlineMessage')->textarea([
                            'placeholder' => Yii::t('system', 'The site is down for maintenance.'),
                            'rows' => 4,
                        ]) ?>
                    </li>
                    <li>
                        <h3><?= Yii::t('system', 'Backend'); ?></h3>
                        <?= $form->field($model, 'backendLanguage')->dropdownList([
                            'ru-RU' => 'Русский',
                            'en-GB' => 'English',
                        ]) ?>
                        <hr>
                        <h3><?= Yii::t('system', 'Frontend'); ?></h3>
                        <?= $form->field($model, 'frontendLanguage')->dropdownList(Yii::$app->locale->languageList) ?>
                        <?= $form->field($model, 'multilanguage')->toggle() ?>
                    </li>
                    <li>
                        <?= $form->field($model, 'fromEmail') ?>
                        <?= $form->field($model, 'fromName') ?>
                    </li>
                    <li>
                        <ul class="uk-list uk-list-divider">
                            <li><?= $form->field($model, 'registration')->dropdownList($model->registrations) ?>   </li>
                            <li><?= $form->field($model, 'signupCaptcha')->toggle() ?></li>
                            <li><?= $form->field($model, 'emailRegistered')->toggle() ?></li>
                            <li><?= $form->field($model, 'emailConfirm')->toggle() ?></li>
                        </ul>
                    </li>
                    <li>
                        <h3>ReCaptcha</h3>
                        <?= $form->field($model, 'siteKeyPublic') ?>
                        <?= $form->field($model, 'siteKeySecret') ?>
                        <div class="uk-margin">
                            <a class="uk-button uk-button-primary" href="https://www.google.com/recaptcha/admin" target="_blank">Get ReCaptcha</a>
                        </div>
                    </li>
                    <li>
                        <h3><?= Yii::t('system', 'Statistic') ?> Yandex Metrika</h3>
                        <?= $form->field($model, 'ymId')->textInput(['type' => 'number']) ?>
                        <?= $form->field($model, 'ymOAuthToken') ?>
                        <p>
                            <a class="uk-button uk-button-primary" href="https://metrika.yandex.ru/list?" target="_blank">Счетчики</a>
                            <a class="uk-button uk-button-primary" href="https://oauth.yandex.ru/" target="_blank">Получение токена</a>
                        </p>
                    </li>
                    <li>
                        <h3>Instagram</h3>
                        <?= $form->field($model, 'instaClientId') ?>
                        <?= $form->field($model, 'instaAccessToken') ?>
                        <?= Html::a(Yii::t('system', 'Get') . ' Access Token', '', [
                            'class' => 'uk-button uk-button-primary uk-margin js-insta-token'
                        ]) ?>
                    </li>
                    <li>
                        <?= $form->field($model, 'schemaOrg')->toggle() ?>
                        <?= $form->field($model, 'metaDescription')->textarea([
                            'rows' => 4,
                            'class' => 'uk-width-1-1'
                        ]) ?>
                        <?= $form->field($model, 'codeHeader')->widget(HtmlEditor::classname(), [
                            'clientOptions' => ['toolbar' => false],
                        ]) ?>
                        <?= $form->field($model, 'codeFooter')->widget(HtmlEditor::classname(), [
                            'clientOptions' => ['toolbar' => false],
                        ]) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            Yii::t('system', 'Clear cache'),
                            ['clear-cache', 'type' => 'flush'],
                            [
                                'class' => 'uk-button uk-button-danger',
                                'ex-ajax' => true
                            ]
                        ) ?>
                        <?= Html::a(
                            Yii::t('system', 'Clear settings cache'),
                            ['clear-cache', 'type' => 'settings'],
                            [
                                'class' => 'uk-button uk-button-primary',
                                'ex-ajax' => true
                            ]
                        ) ?>

                        <div class="uk-card uk-card-default uk-card-body uk-margin">
                            <h3 class="uk-card-title"><?= Yii::t('system', 'Admin') ?></h3>
                            <?= $form->field($model, 'forceCopyBackend')->toggle() ?>
                        </div>

                        <div class="uk-card uk-card-default uk-card-body uk-margin">
                            <h3 class="uk-card-title"><?= Yii::t('system', 'Site') ?></h3>
                            <?= $form->field($model, 'forceCopyFrontend')->toggle([], false) ?>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<div class="uk-card uk-card-default uk-card-body uk-card-small uk-text-small uk-margin-large">
    <div>Version ExooCMF - <?= Yii::$app->system->version ?></div>
    <div>Server - <?= $_SERVER['SERVER_SOFTWARE'] ?></div>
    <div>Client - <?= $_SERVER['HTTP_USER_AGENT'] ?></div>
    <div>IP:Port <?= $_SERVER['SERVER_ADDR'] ?>:<?= $_SERVER['SERVER_PORT'] ?></div>
</div>

<?php
$script = <<<JS
var mailer = '.js-select-mailer',
    instaclientid = '#settings-instaclientid',
    instaaccesstoken = '#settings-instaaccesstoken';

toggleMailer();
toggleInstaToken();

$(document).on('change', mailer, function() {
    toggleMailer();
});

$(document).on('change', instaclientid + ', ' + instaaccesstoken, function() {
    toggleInstaToken();
});

function toggleMailer() {
    var value = $(mailer).val();
    $('.js-smtp').toggle(value == 'smtp');
}

function toggleInstaToken() {
    var clientID = $(instaclientid).val(),
        token = $(instaaccesstoken).val(),
        button = $('.js-insta-token'),
        params = {
            client_id: clientID,
            redirect_uri: '$model->frontendUrl',
            response_type: 'token'
        },
        url = 'https://instagram.com/oauth/authorize/?';

    if (clientID && params.redirect_uri) {
        button.attr('href', url + $.param(params));
        button.toggle(clientID.length > 0 && token.length == 0);
    }

}
JS;
$this->registerJs($script);
