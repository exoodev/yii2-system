<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

$this->title = Yii::t('system', 'Company');

?>
<?php $form = ActiveForm::begin([
    'id' => 'companyForm',
    'enableAjaxSubmit' => true
]); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div uk-margin>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div uk-margin>
            <?= Html::submitButton(Yii::t('system', 'Save'), [
                'class' => 'uk-button uk-button-success',
            ]) ?>
        </div>
    </div>
    <div class="uk-card uk-card-default uk-card-body">
        <?= $form->field($model, 'name')->label(Yii::t('system', 'Name')) ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>

        <?php $form->layout = 'inline'; ?>

        <ul uk-tab>
            <li><a href="#"><?= $model->getAttributeLabel('phone') ?></a></li>
            <li><a href="#"><?= $model->getAttributeLabel('email') ?></a></li>
            <li><a href="#"><?= $model->getAttributeLabel('address') ?></a></li>
            <?php if (isset(Yii::$app->params['social'])): ?>
                <li><a href="#"><?= $model->getAttributeLabel('social') ?></a></li>
            <?php endif; ?>
        </ul>

        <ul class="uk-switcher uk-margin">
            <li>
                <?php for ($i=0; $i < 5; $i++): ?>
                    <div class="uk-margin-small uk-grid-small uk-flex uk-flex-middle" uk-grid>
                        <div style="width: 30px;">
                            <label class="uk-label"><?= $i + 1 ?></label>
                        </div>
                        <div class="uk-width-1-3@m">
                            <?= $form
                                ->field($model, 'phone[' . ($i + 1) . '][name]')
                                ->textInput([
                                    'autocomplete' => 'nope',
                                    'placeholder' => Yii::t('system', 'Name')
                                ])
                                ->label(false)
                            ?>
                        </div>
                        <div class="uk-width-expand@m">
                            <?= $form
                                ->field($model, 'phone[' . ($i + 1) . '][number]')
                                ->textInput([
                                    'placeholder' => Yii::t('system', 'Number')
                                ])
                                ->label(false)
                            ?>
                        </div>
                    </div>
                <?php endfor; ?>
            </li>
            <li>
                <?php for ($i=0; $i < 5; $i++): ?>
                    <div class="uk-margin-small uk-grid-small uk-flex uk-flex-middle" uk-grid>
                        <div style="width: 30px;">
                            <label class="uk-label"><?= $i + 1 ?></label>
                        </div>
                        <div class="uk-width-1-3@m">
                            <?= $form
                                ->field($model, 'email[' . ($i + 1) . '][name]')
                                ->textInput([
                                    'autocomplete' => 'nope',
                                    'placeholder' => Yii::t('system', 'Name')
                                ])
                                ->label(false)
                            ?>
                        </div>
                        <div class="uk-width-expand@m">
                            <?= $form
                                ->field($model, 'email[' . ($i + 1) . '][address]')
                                ->textInput([
                                    'placeholder' => Yii::t('system', 'Address')
                                ])
                                ->label(false)
                            ?>
                        </div>
                    </div>
                <?php endfor; ?>
            </li>
            <li>
                <?php for ($i=0; $i < 5; $i++): ?>
                    <div class="uk-margin-small uk-grid-small uk-flex uk-flex-middle" uk-grid>
                        <div style="width: 30px;">
                            <label class="uk-label"><?= $i + 1 ?></label>
                        </div>
                        <div class="uk-width-1-3@m">
                            <?= $form
                                ->field($model, 'address[' . ($i + 1) . '][name]')
                                ->textInput([
                                    'autocomplete' => 'nope',
                                    'placeholder' => Yii::t('system', 'Name')
                                ])
                                ->label(false)
                            ?>
                        </div>
                        <div class="uk-width-expand@m">
                            <?= $form
                                ->field($model, 'address[' . ($i + 1) . '][address]')
                                ->textInput([
                                    'autocomplete' => 'shipping street-address',
                                    'placeholder' => Yii::t('system', 'Address')
                                ])
                                ->label(false)
                            ?>
                        </div>
                    </div>
                <?php endfor; ?>
            </li>
            <?php if (isset(Yii::$app->params['social'])): ?>
                <li>
                    <?php foreach (Yii::$app->params['social'] as $key => $social): ?>
                        <?= $form
                            ->field($model, 'social[' . $key . '][name]')
                            ->hiddenInput([
                                'value' => $social['name'],
                            ])
                            ->label(false)
                        ?>
                        <?= $form
                            ->field($model, 'social[' . $key . '][icon]')
                            ->hiddenInput([
                                'value' => $social['icon'],
                            ])
                            ->label(false)
                        ?>
                        <div class="uk-margin-small uk-grid-small uk-flex uk-flex-middle" uk-grid>
                            <div class="uk-width-small@s">
                                <label class="uk-label uk-text-lowercase uk-width-1-1" uk-tooltip="<?= $social['name'] ?>"><?= $key ?></label>
                            </div>
                            <div class="uk-width-expand@s">
                                <?= $form
                                    ->field($model, 'social[' . $key . '][url]')
                                    ->textInput([
                                        'autocomplete' => 'social',
                                        'placeholder' => 'URL'
                                    ])
                                    ->label(false)
                                ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </li>
            <?php endif; ?>
        </ul>

        <?php $form->layout = 'stacked'; ?>
    </div>
<?php ActiveForm::end(); ?>
