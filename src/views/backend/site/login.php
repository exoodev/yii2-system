<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

$this->title = Yii::t('system', 'Authorization');
?>
<div class="uk-flex uk-flex-center uk-flex-middle uk-text-center uk-height-1-1">
    <div class="uk-card uk-card-default uk-card-body">
        <h3 class="uk-card-title"><?= Html::encode($this->title) ?></h3>
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?= $form->field($model, 'email')->textInput([
                'class' => 'uk-form-large uk-width-1-1',
                'placeholder' => $model->getAttributeLabel('email')
            ])->label(false) ?>
            <?= $form->field($model, 'password')->passwordInput([
                'class' => 'uk-form-large uk-width-1-1',
                'placeholder' => $model->getAttributeLabel('password')
            ])->label(false) ?>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <div class="uk-form-row">
                <?= Html::submitButton(Yii::t('system', 'Login'), [
                    'class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1',
                    'name' => 'login-button'
                ]) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
