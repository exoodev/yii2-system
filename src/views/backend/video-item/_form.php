<?php
use yii\helpers\Url;
use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\joditeditor\JoditEditor;

$variationModels = $model->variationModels;
$showLocales = count($variationModels) > 1 ? '' : ' uk-hidden';
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?php $form = ActiveForm::begin(['id' => 'videoForm']); ?>
        <div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
            <div>
                <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
            </div>
            <div>
                <?= $form
                    ->field($model, 'status')
                    ->toggle(['uk-tooltip' => Yii::t('system', 'Status')])
                    ->inline()
                    ->label(false)
                ?>
                <?= Html::submitButton(Yii::t('system', 'Save'), [
                    'class' => 'uk-button uk-button-success'
                ]) ?>
                <?= Html::a(Yii::t('system', 'Close'), Url::previous('video-view'), [
                    'class' => 'uk-button uk-button-default'
                ]) ?>
                <?php if (!$model->isNewRecord): ?>
                    <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], [
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('system', 'Are you sure want to delete this video?'),
                        'class' => 'uk-button uk-button-danger',
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="uk-child-width-1-2@m" uk-grid>
            <div>
                <div class="js-yt-placeholder"></div>
            </div>
            <div>
                <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'class' => 'js-yt-url']) ?>
                <?= $form->field($model, 'folder_id')->dropdownList($model->foldersList) ?>
                <div class="uk-width-1-3@s">
                    <?= $form->field($model, 'height')->textInput(['type' => 'number', 'min' => 200]) ?>
                </div>
            </div>
        </div>

        <div class="uk-grid-medium" uk-grid>
            <div class="uk-width-auto@m<?= $showLocales ?>">
                <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
                    <?php foreach ($variationModels as $index => $variationModel): ?>
                        <li><a href="#"><?= $variationModel->language->url ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="uk-width-expand@m">
                <ul id="component-tab-left" class="uk-switcher">
                    <?php foreach ($variationModels as $index => $variationModel): ?>
                    <li>
                        <?= $form
                            ->field($variationModel, "[{$index}]title")
                            ->textInput(['maxlength' => true])
                            ->label($variationModel->getAttributeLabel('title'))
                        ?>
                        <?= $form
                            ->field($variationModel, "[{$index}]description")
                            ->widget(JoditEditor::className(), [
                                'clientOptions' => [
                                    'height' => 600,
                                ]
                            ])
                            ->label($variationModel->getAttributeLabel('description'))
                        ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$js = <<<JS
$('#videoForm').on('beforeSubmit', function () {
    var form = $(this)
    $.post(form.attr('action'), form.serializeArray())
        .done(function (data) {
            if (data.success) {
                UIkit.notification(data.success, 'success')
            } else if (data.validation) {
                form.yiiActiveForm('updateMessages', data.validation, true)
            }
        })

    return false
})


var YTPlayer = {
    url: '.js-yt-url',
    placeholder: '.js-yt-placeholder',


    init: function() {
        var \$this = this
        this._createFrame()

        $(document).on('change', this.url, function() {
            var value = $(this).val()
            $(this).val(value.replace("watch?v=", "embed/"))
            \$this._createFrame()
        })
    },

    _createFrame: function() {
        var url = $(this.url).val()
        var placeholder

        if (url) {
            placeholder = $('<iframe width="100%" height="400" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>')
            placeholder.attr('src', $(this.url).val())
        } else {
            placeholder = '<div class="uk-placeholder uk-flex uk-flex-middle uk-flex-center" style="height: 270px;"><i class="fab fa-youtube uk-text-muted fa-4x"></i></div>'
        }

        $(this.placeholder).html(placeholder)
    }
}
YTPlayer.init()
JS;
$this->registerJs($js);
