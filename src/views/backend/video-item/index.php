<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\grid\GridView;
use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use exoo\position\PositionColumn;
use exoo\status\StatusColumn;

?>
<?= GridView::widget([
    'id' => 'videoGrid',
    'pjax' => true,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'buttons' => [
        Html::a(Yii::t('system', 'Add video'), ['video-item/create', 'folder_id' => $folder->id], [
            'class' => 'uk-button uk-button-success',
            'data-pjax' => 0
        ]),
        Html::a(Yii::t('system', 'Delete'), ['video-item/batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data' => [
                'method' => 'post',
                'confirm' => Yii::t('system', 'Are you sure you want to delete the selected items?'),
            ],
            'ex-selected' => true
        ]),
    ],
    'columns' => [
        ['class' => CheckboxColumn::class],
        [
            'attribute' => 'id',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'attribute' => 'video',
            'format' => 'raw',
            'value' => function($model) {
                return '<iframe src="' . Html::encode($model->url) . '" width="270" height="180" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen uk-responsive></iframe>';
            }
        ],
        [
            'class' => PositionColumn::class,
            'controller' => 'video-item',
        ],
        [
            'class' => StatusColumn::class,
            'controller' => 'video-item',
        ],

        [
            'class' => ActionColumn::class,
            'controller' => 'video-item',
            'template' => '{update} {delete}',
        ],
    ],
]); ?>
