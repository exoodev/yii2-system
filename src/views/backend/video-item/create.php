<?php
$this->title = Yii::t('system', 'Adding');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
