<?php

use yii\helpers\Url;
use yii\helpers\Html;

$url = Url::to(['view', 'id' => $model->id]);
?>

<a href="<?= $url ?>">
    <div class="uk-card uk-card-default uk-card-small uk-box-shadow-small uk-box-shadow-hover-medium">
        <div class="uk-card-header">
            <h3 class="uk-h5 uk-text-truncate uk-margin-top uk-margin-bottom uk-flex uk-flex-middle"><i class="fas fa-folder fa-2x uk-margin-small-right uk-text-muted"></i><?= Html::encode($model->name) ?></h3>
        </div>
    </div>
</a>
