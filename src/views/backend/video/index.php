<?php

use yii\helpers\Html;
use exoo\uikit\ListView;

/* @var $this yii\web\View */
/* @var $searchModel exoo\storage\models\backend\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('system', 'Video');

?>
<div class="uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
    <div>
        <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></a></h3>
    </div>
    <div>
        <?= Html::a('<i class="fas fa-folder-plus fa-lg uk-margin-small-right"></i>' . Yii::t('system', 'New folder'), ['create'], [
            'class' => 'uk-button uk-button-success',
        ]) ?>
    </div>
</div>
<div class="uk-margin">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_itemIndex',
        'layout' => '<div class="uk-child-width-1-5@m uk-grid-small" uk-grid>{items}</div>',
        'summaryOptions' => ['class' => 'uk-margin']
    ]); ?>
</div>
