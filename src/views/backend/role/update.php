<?php

use yii\helpers\Html;

$this->title = Yii::t('system', 'Update role');
?>

<h1><?= Html::encode($this->title) ?></h1>
<?php
	if ($rbac->name != $rbac->defaultRole) {
		echo '<div class="uk-alert uk-alert-warning" role="alert">';
		echo "\n" . Yii::t('system', 'Warning! If you change the parent element, you will lose all permissions of the current role.') . "\n";
		echo '</div>';
	}
?>
<?= $this->render('_form', [
    'rbac' => $rbac,
]) ?>
