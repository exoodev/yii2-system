<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use exoo\system\widgets\Permissions;
use exoo\nested\NestedView;

Url::remember(['index'], 'index');
$this->title = Yii::t('system', 'Roles and permissions');
?>
<h2><?= Html::encode($this->title) ?></h2>

<div class="uk-child-width-expand@s" uk-grid>
	<div>
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<div class="uk-card-title"><?= Yii::t('system', 'Roles') ?></div>
			</div>
			<div class="uk-card-body">
				<?= NestedView::widget([
					'id' => 'rbacNestable',
					'modelClass' => $rbac,
					'items' => $rbac->nestable,
					'handle' => true,
				]) ?>
			</div>
			<div class="uk-card-footer">
				<?= Html::a(Yii::t('system', 'Add'), ['create'], [
					'class' => 'uk-button uk-button-success'
				]) ?>
			</div>
		</div>
	</div>
	<div>
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<div class="uk-card-title"><?= Html::encode($rbac->role->name) ?></div>
			</div>
			<div class="uk-card-body">
				<?php $form = ActiveForm::begin([
					'id' => 'permissionForm',
					'action' => [
						'permission',
						'role' => $rbac->role->name,
					]
				]); ?>
				<?= $form
					->field($rbac, 'permission')
					->widget(Permissions::className(), [
						'items' => $permissions,
						'model' => $rbac,
					])
					->label(false) ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>

<?php
$url = Url::to(['nestable']);
$urlPermission = Url::to(['permission']);
$roleName = $rbac->role->name;
$script = <<<JS
// $(document).on('change.uk.nestable', '.uk-nestable', function() {
//     el = $(this);
//     var data = el.data("nestable").serialize();
// 	ajax("$url", {roles: data});
// });
var nestable = $("#rbacNestable").nestable();
nestable.on('change', function(e) {
    var list = e.length ? e : $(e.target);
	post("$url", {roles: list.nestable('serialize')})
});

$(document).on('change', '#permissionForm input[type="checkbox"]', function() {
    el = $(this);
	var data = {
		role: "$roleName",
		permission: el.attr('name'),
		checked: el.prop('checked')? 1 : 0,
	}
	post("$urlPermission", data)
});

function post(url, data) {
	$.post(url, data)
        .done(function(response) {
            if (response.message) {
                UIkit.notification(response.message, response.status);
            }
        });
}
JS;
// $this->registerJs($script);
