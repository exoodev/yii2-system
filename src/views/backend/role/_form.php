<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

$options = $rbac->name == $rbac::ADMIN_ROLE || $rbac->name == $rbac::DEFAULT_ROLE ? ['disabled' => true] : [];
?>

<div class="uk-card uk-card-default uk-card-body">
	<?php $form = ActiveForm::begin(['id' => 'rbacForm']); ?>
	<?= $form->field($rbac, 'name')->textInput(array_merge(['maxlength' => 64],$options)) ?>
	<?= $form->field($rbac, 'description')->textInput(['maxlength' => true]) ?>
	<?php if ($rbac->name != $rbac->defaultRole): ?>
	<?= $form->field($rbac, 'child')->dropDownList($rbac->rolesList, ['prompt' => '']) ?>
	<?php endif; ?>

	<div class="uk-form-row">
		<?= Html::submitButton(Yii::t('system', 'Save'), [
			'class' => 'uk-button uk-button-primary',
			'form' => 'rbacForm',
		]) ?>
		<?= Html::a(Yii::t('system', 'Cancel'), Yii::$app->user->returnUrl, [
			'class' => 'uk-button uk-button-default',
		]) ?>
		<?php if ($rbac->role !== null &&  $rbac->name != $rbac::ADMIN_ROLE && $rbac->name != $rbac::DEFAULT_ROLE): ?>
		<?= Html::a(Yii::t('system', 'Delete'), ['delete', 'name' => $rbac->name], [
			'data-method' => 'post',
			'data-confirm' => Yii::t('system', 'Are you sure want to delete this Role?'),
			'class' => 'uk-button uk-button-danger',
		]) ?>
		<?php endif; ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>
