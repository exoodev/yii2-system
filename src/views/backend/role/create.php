<?php

use yii\helpers\Html;

$this->title = Yii::t('system', 'Create role');
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', [
    'rbac' => $rbac,
]) ?>
