<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use exoo\widgets\Nav;
use exoo\widgets\NavBar;
use exoo\widgets\StickyAsset;
use exoo\widgets\ScrollerAsset;
use exoo\docs\assets\DocAsset;
use exoo\widgets\FormAdvancedAsset;

FormAdvancedAsset::register($this);
ScrollerAsset::register($this);
StickyAsset::register($this);
DocAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div data-uk-smooth-scroll data-uk-sticky="{top:-200}">
    <a class="tm-top-scroller uk-animation-slide-top" href="#"><i class="uk-icon-chevron-up"></i></a>
</div>
<?php NavBar::begin([
    'brandLabel' => '<span class="uk-label uk-label-success uk-margin-right"><span class="uk-h3">EXOO</span></span> CMS Documentation',
    'brandUrl' => ['/kit/docs/features'],
    'attached' => true,
    'options' => ['class' => 'tm-navbar']
]);
$menuItems = [
    ['label' => 'Application', 'url' => ['/']],
] ?>
<div class="uk-navbar-flip">
    <?= Nav::widget([
        'options' => ['class' => 'uk-navbar-nav'],
        'items' => $menuItems,
    ]) ?>
</div>
<?php NavBar::end(); ?>
<div class="uk-container uk-margin-large">
    <?php if (isset($this->blocks['sidebar-a'])) : ?>
        <div class="uk-grid-divider" uk-grid>
            <div class="uk-width-medium-3-10">
                <div class="tm-sidebar-a">
                    <?= $this->blocks['sidebar-a'] ?>
                </div>
            </div>
            <div class="uk-width-medium-7-10">
                <article class="uk-article">
                    <?= $content ?>
                </article>
            </div>
        </div>
    <?php else: ?>
        <?= $content ?>
    <?php endif; ?>
</div>

<footer class="uk-block uk-block-secondary uk-contrast uk-margin-top">
    <div class="uk-container">
        <div class="uk-float-left">&copy; Exoodev <?= date('Y') ?></div>
        <div class="uk-float-right"><?= Yii::powered() ?></div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
