<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\uikit\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = Yii::t('system', 'Login');
$authClients = ArrayHelper::getValue(Yii::$app->components, 'authClientCollection.clients');
?>
<div class="uk-flex uk-flex-center uk-flex-middle uk-text-center uk-height-1-1">
    <div class="uk-card uk-card-default uk-card-body uk-width-1-3@m">
        <h3 class="uk-card-title"><?= Html::encode($this->title) ?></h3>
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'email')->textInput([
                'class' => 'uk-form-large uk-width-1-1',
                'placeholder' => $model->getAttributeLabel('email'),
            ])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput([
                'class' => 'uk-form-large uk-width-1-1',
                'toggle' => true,
                'placeholder' => $model->getAttributeLabel('password'),
            ])->label(false) ?>

            <p><?= $form->field($model, 'rememberMe')->checkbox() ?></p>
            <p><?= Yii::t('system', 'Forgot your password?') ?> <?= Html::a(Yii::t('system', 'reset it'), ['user/request-password-reset']) ?></p>

            <?= Html::submitButton(Yii::t('system', 'Login'), [
                'class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1',
            ]) ?>

            <?php if (Yii::$app->system->isRegistrationEnabled): ?>
            <p class="uk-text-center"><?= Yii::t('system', 'No account yet?') ?> <?= Html::a(Yii::t('system', 'Signup'), ['user/signup']) ?></p>
            <?php endif; ?>

        <?php ActiveForm::end(); ?>
        <?php if ($authClients): ?>
            <?= AuthChoice::widget([
                'baseAuthUrl' => ['user/auth'],
                'popupMode' => false,
            ]) ?>
        <?php endif; ?>
    </div>
</div>
