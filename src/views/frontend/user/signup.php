<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\uikit\ActiveForm;
use exoo\widgets\ReCaptcha;
use yii\authclient\widgets\AuthChoice;

$this->title = Yii::t('system', 'Create account');
$captha = Yii::$app->settings->get('system', 'signupCaptcha');
$activation = Html::a(Yii::t('system', 'activation'), ['user/resend-activation']);
$authClients = ArrayHelper::getValue(Yii::$app->components, 'authClientCollection.clients');
?>
<div class="uk-flex uk-flex-center uk-flex-middle uk-text-center uk-height-1-1">
    <div class="uk-card uk-card-default uk-card-body uk-width-1-3@m">
        <h3 class="uk-card-title"><?= Html::encode($this->title) ?></h3>
        <?php $form = ActiveForm::begin([
            'id' => 'form-signup',
            'enableAjaxValidation' => true,
            'validateOnSubmit' => !$captha,
        ]); ?>

            <?= $form->field($model, 'username')->textInput([
                'class' => 'uk-form-large',
                'placeholder' => $model->getAttributeLabel('username'),
            ])->label(false) ?>

            <?= $form->field($model, 'email')->textInput([
                'class' => 'uk-form-large',
                'placeholder' => $model->getAttributeLabel('email'),
            ])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput([
                'class' => 'uk-form-large',
                'toggle' => true,
                'placeholder' => $model->getAttributeLabel('password'),
            ])->label(false) ?>

            <?php if ($captha): ?>
            <?= $form->field($model, 'captcha', ['enableAjaxValidation' => false])
                ->widget(\exoo\widgets\ReCaptcha::className(), [
                    'siteKey' => Yii::$app->settings->get('system', 'siteKeyPublic'),
                ])
                ->label(false) ?>
            <?php endif; ?>

            <p class="uk-form-row">
                <?= Html::submitButton(Yii::t('system', 'Signup'), [
                    'class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1',
                ]) ?>
            </p>

            <?php if (Yii::$app->settings->get('system', 'emailConfirm')): ?>
                <p class="uk-text-center">
                    <?= Yii::t('system', 'Did you miss your {activation} email?', ['activation' => $activation]) ?>
                </p>
            <?php endif; ?>

            <p class="uk-text-center">
                <?= Yii::t('system', 'I have an account?') ?>
                <?= Html::a(Yii::t('system', 'Login'), ['user/login']) ?>
            </p>

        <?php ActiveForm::end(); ?>
        <?php if ($authClients): ?>
            <?= AuthChoice::widget([
                'baseAuthUrl' => ['user/auth'],
                'popupMode' => false,
            ]) ?>
        <?php endif; ?>
    </div>
</div>
