<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

$this->title = Yii::t('system', 'Enter your e-mail');
?>
<div class="uk-width-1-2@m uk-width-large-1-3">
    <h1 class="uk-text-center"><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
        'id' => 'addEmailForm',
    ]); ?>

        <?= $form->field($model, 'email')->textInput([
            'class' => 'uk-form-large uk-width-1-1',
            'placeholder' => $model->getAttributeLabel('email'),
        ])->label(false) ?>

        <p class="uk-form-row">
            <?= Html::submitButton(Yii::t('system', 'Save'), [
                'class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1',
            ]) ?>
        </p>

    <?php ActiveForm::end(); ?>
</div>
