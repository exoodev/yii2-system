<?php

use yii\helpers\Html;

$this->title = Yii::t('system', 'Activate e-mail');

$url = ['resend-activation'];
$link = Yii::t('system', 'Check your email');

if ($email = Html::encode($email)) {
    $domain = array_pop(explode('@', $email));
    $link = Html::a($link, 'http://' . $domain, ['target' => '_blank']);
    $url = ['resend-activation', 'email' => $email];
}
?>

<div class="uk-width-1-2@m uk-width-large-1-3 uk-text-center">
    <p class="uk-margin-top">
        <i class="uk-icon-envelope-o uk-text-muted uk-icon-large"></i>
    </p>
    <h1 class="uk-margin-large"><?= Html::encode($this->title) ?></h1>
    <p class="uk-margin"><?= Yii::t('system', 'To complete the registration you need to activate e-mail. {link} and click the link in the email.', ['link' => $link]) ?></p>
    <p class="uk-margin-large-bottom uk-h1"><?= Html::encode($email) ?></p>
    <hr>
    <p class="uk-text-small"><?= Html::a(Yii::t('system', 'Re-send an email with the activation'), $url) ?></p>
</div>
