<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

$this->title = Yii::t('system', 'Reset password');
?>
<div class="uk-width-1-3@m uk-width-large-1-3">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Yii::t('system', 'Please fill out your e-mail. A link to reset password will be sent there.') ?></p>
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'email')->textInput([
            'class' => 'uk-form-large uk-width-1-1',
            'placeholder' => $model->getAttributeLabel('email'),
        ])->label(false) ?>
        <div class="uk-form-row">
            <?= Html::submitButton(Yii::t('system', 'Send'), ['class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
