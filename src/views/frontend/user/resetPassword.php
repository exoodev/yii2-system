<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

$this->title = Yii::t('system', 'Reset password');
?>
<div class="uk-width-1-3@m uk-width-large-1-3">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Yii::t('system', 'Create a new password.') ?></p>
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'password')->passwordInput([
            'toggle' => true,
            'class' => 'uk-form-large uk-width-1-1',
            'placeholder' => $model->getAttributeLabel('password'),
        ])->label(false) ?>
        <div class="uk-form-row">
            <?= Html::submitButton(Yii::t('system', 'Save'), ['class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
