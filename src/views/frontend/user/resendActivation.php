<?php
use yii\helpers\Html;
use exoo\uikit\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \exoo\system\models\frontend\ResendEmailConfirmForm */

$this->title = Yii::t('system', 'Resend activation');

?>
<div class="uk-width-1-2@m uk-width-large-1-3">
    <div class="uk-text-center">
        <h1><?= Html::encode($this->title) ?></h1>
        <p><?= Yii::t('system', 'Enter your Email to resend activation') ?></p>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'resend-activation-form'
    ]); ?>
        <?= $form->field($model, 'email')->textInput([
            'class' => 'uk-form-large uk-width-1-1',
            'placeholder' => 'E-mail',
        ])->label(false) ?>
        <div class="uk-form-row">
            <?= Html::submitButton(Yii::t('system', 'Send'), [
                'class' => 'uk-button uk-button-primary uk-button-large uk-width-1-1'
            ]) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
