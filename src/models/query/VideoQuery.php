<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2019
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\query;

use Yii;
use exoo\system\models\Video;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Video]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class VideoQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return parent::find()->with('defaultTranslation');
    }

    /**
     * Select active.
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => Video::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * Select models with slugs
     *
     * @param string|array $slug
     * @return $this
     */
    public function slug($slug)
    {
        $this->andWhere(['slug' => $slug]);
        return $this;
    }
}
