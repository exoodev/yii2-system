<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\query;

use Yii;
use exoo\system\models\Widget;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Widget]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class WidgetQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return parent::find()->with('defaultTranslation');
    }

    /**
     * Select active.
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => Widget::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * Select models with slugs
     *
     * @param string|array $slug
     * @return $this
     */
    public function slug($slug)
    {
        $this->andWhere(['slug' => $slug]);
        return $this;
    }
}
