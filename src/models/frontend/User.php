<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\frontend;

/**
 * User model for frontend application.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class User extends \exoo\system\models\User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password'], 'required'],
            [['username', 'password'], 'string', 'max' => 255],
            [['username', 'email'], 'unique'],
            [['role'], 'string', 'max' => 64],
            ['email', 'email'],
            ['status_id', 'default', 'value' => self::STATUS_ACTIVE],
            ['status_id', 'in', 'range' => array_keys(self::getStatuses())],
        ];
    }
}
