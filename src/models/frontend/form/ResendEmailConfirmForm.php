<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\frontend\form;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use exoo\system\models\User;

/**
 * Resend email confirm form.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ResendEmailConfirmForm extends Model
{
    /**
     * @var string $email
     */
    public $email;
    /**
     * @var User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'trim'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'exist',
                'targetClass' => User::className(),
                'filter' => function ($query) {
                    $query->inactive();
                },
                'message' => Yii::t('system', 'E-mail is not found or is already activated'),
            ]
        ];
    }

    /**
     * Resend email confirmation token
     *
     * @return boolean
     */
    public function resend()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->_user = User::findByInactiveEmail($this->email);
        if ($this->_user !== null) {
            $signup = new SignupForm();
            $signup->email = $this->email;
            return $signup->sendMailConfirm($this->_user);
        }
        return false;
    }
}
