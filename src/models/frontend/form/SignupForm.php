<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\frontend\form;

use Yii;
use yii\base\Model;
use exoo\system\models\User;
use exoo\system\models\Profile;
use exoo\system\models\setting\SystemSetting;
use exoo\system\validators\ReCaptchaValidator;

/**
 * Signup form.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $captcha;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email', 'message' => Yii::t('system', 'E-mail is invalid.')],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('system', 'This e-mail address has already been taken.'),
            ],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];

        if (Yii::$app->settings->get('system', 'signupCaptcha')) {
            $rules = array_merge($rules, [
                ['captcha', 'required'],
                [
                    'captcha',
                    ReCaptchaValidator::className(),
                    'secretKey' => Yii::$app->settings->get('system', 'siteKeySecret'),
                ],
            ]);
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('system', 'Username'),
            'password' => Yii::t('system', 'Password'),
            'email' => 'E-mail',
            'captcha' => 'Captcha',
        ];
    }

    /**
     * Signs user up.
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->_password = $this->password;

        if (Yii::$app->settings->get('system', 'registration') == SystemSetting::REGISTRATION_APPROVAL
            || Yii::$app->settings->get('system', 'emailConfirm')) {
            $user->status_id = User::STATUS_INACTIVE;
        } else {
            $user->status_id = User::STATUS_ACTIVE;
        }

        if (Yii::$app->settings->get('system', 'emailConfirm')) {
            $user->generateEmailToken();
        }

        $transaction = $user->getDb()->beginTransaction();
        if ($user->save()) {
            if (Yii::$app->settings->get('system', 'emailRegistered')) {
                $user->sendMailRegistered();
            }
            $profile = new Profile();
            $user->link('profile', $profile);
            $user->assignRole(User::ROLE_REGISTERED);
            $transaction->commit();
            return $user;
        }

        return null;
    }
}
