<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\frontend\form;

use Yii;
use yii\base\Model;
use exoo\system\models\User;

/**
 * Password reset request form.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class PasswordResetRequestForm extends Model
{
    /**
     * @var string User email.
     */
    public $email;
    /**
     * @var common\models\User
     */
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::className(),
                'filter' => function ($query) {
                    $query->active();
                },
                'message' => Yii::t('system', 'There is no user with such e-mail.'),
            ],
            ['email', 'validateSend'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateSend($attribute, $params)
    {
        if (!$this->hasErrors() && $user = $this->getUser()) {
            if (User::isPasswordResetTokenValid($user->password_reset_token)) {
                $this->addError($attribute, Yii::t('system', 'Token has already been sent.'));
            }
        }
    }

    /**
     * Sends an email with a link, for resetting the password.
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        if ($user = $this->getUser()) {
            $user->generatePasswordResetToken();
            if ($user->save()) {
                return Yii::$app->mailer->compose(
                        ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                        ['user' => $user]
                    )
                    ->setFrom([Yii::$app->settings->get('system', 'fromEmail') => Yii::$app->settings->get('system', 'fromName')])
                    ->setTo($this->email)
                    ->setSubject(Yii::t('system', 'Password reset for') . ' ' . Yii::$app->settings->get('system', 'company'))
                    ->send();
            }
        }

        return false;
    }

    /**
     * Find user by [[email]]
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
}
