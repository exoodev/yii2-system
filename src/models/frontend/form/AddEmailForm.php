<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\frontend\form;

use Yii;
use yii\base\Model;
use exoo\system\models\User;
use yii\base\InvalidParamException;

/**
 * Add email form.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class AddEmailForm extends Model
{
    public $email;

    /**
     * @var User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException(Yii::t('system', 'Email token cannot be blank.'));
        }
        $this->_user = User::findByEmailToken($token);
        if (!$this->_user) {
            throw new InvalidParamException(Yii::t('system', 'Wrong email token.'));
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email', 'message' => Yii::t('system', 'E-mail is invalid.'),],
            ['email', 'string', 'max' => 255],
            [
                'email', 'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('system', 'This e-mail address has already been taken.'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * Add email to user.
     * @return User|null the saved model or null if saving fails
     */
    public function addEmail()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->_user;
        $user->email = $this->email;

        if (!Yii::$app->settings->get('system', 'emailConfirm')) {
            $user->removeEmailToken();
            $user->status_id = User::STATUS_ACTIVE;
        }

        if ($user->save()) {
            if (Yii::$app->settings->get('system', 'emailRegistered')) {
                $user->sendMailRegistered();
            }
            return $user;
        }

        return null;
    }
}
