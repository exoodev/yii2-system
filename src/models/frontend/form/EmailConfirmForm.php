<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\frontend\form;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use exoo\system\models\User;

/**
 * Email confirm form.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class EmailConfirmForm extends Model
{
    /**
     * @var User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     * @param  string $token
     * @param  array $config
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException(Yii::t('system', 'Token confirm e-mail cannot be blank.'));
        }
        $this->_user = User::findByEmailToken($token);
        if (!$this->_user) {
            throw new InvalidParamException(Yii::t('system', 'Wrong confirm e-mail token.'));
        }
        parent::__construct($config);
    }

    /**
     * Confirm email.
     * @return boolean if email was confirmed.
     */
    public function confirmEmail()
    {
        $user = $this->_user;
        $user->status_id = User::STATUS_ACTIVE;
        $user->removeEmailToken();
        Yii::$app->user->login($user);

        return $user->save();
    }
}
