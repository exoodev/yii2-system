<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\backend;

use Yii;
use yii\base\Model;
use yii\helpers\Html;

/**
 * Rbac is the class used to manage roles.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Rbac extends Model
{
	const TYPE_ROLE = 1;
	const TYPE_PERMISSION = 2;
	const DEFAULT_ROLE = 'Registered';
	const ADMIN_ROLE = 'Admin';

	/**
	 * @var array permissions names.
	 */
	public $permission;

	/**
	 * @var null|Role
	 */
	public $role;

	/**
	 * @var string the role name.
	 */
	public $name;

	/**
	 * @var string the role descriptions.
	 */
	public $description;

	/**
	 * @var string the name of child role.
	 */
	public $child;

	/**
	 * @var Role[] array list of roles.
	 */
	protected $_roles;

	public function rules()
	{
		return [
			[['permission'], 'each', 'rule' => ['string']],
			[['name', 'description'], 'required'],
			['name', 'validateName'],
			[['name'], 'string', 'max' => 64],
			[['description'], 'string'],
			[['child'], 'in', 'range' => array_keys($this->getRolesList())],
		];
	}

	public function attributeLabels()
	{
		return [
			'name' => Yii::t('system', 'Name'),
			'description' => Yii::t('system', 'Description'),
			'child' => Yii::t('system', 'Parent'),
		];
	}

	public function validateName()
	{
		if (!$this->hasErrors()){
			$auth = Yii::$app->authManager;
			if (($auth->getRole($this->name) !== null) && empty($this->role)) {
				$this->addError('name', Yii::t('system', 'Role already exist'));
			}
		}
	}

	/**
	 * function finds the role corresponding to the specified name
	 * and set values for attributes of Rbac model via [[setValues()]].
	 * @param string $name the role name.
	 * @return Rbac
	 */
	public static function find($name)
	{
		$model = new static();
		$name = $name ? $name : $model->defaultRole;
	    $auth = Yii::$app->authManager;
		$role = $auth->getRole($name);
		if ($role !== null) {
			$model->setValues($role);
			return $model;
		}
		return null;
	}

	/**
	 * The function will set values for attributes of Rbac model.
	 * @param Role $role.
	 */
	public function setValues($role)
	{
		$this->role = $role;
		$this->name = $role->name;
		$this->description = $role->description;
		$this->child = $this->getChildByRole($role->name) ? $this->getChildByRole($role->name)->name : null;
		$this->permission = self::buildPermissionsByRole($role->name);
	}

	public function save()
	{
		if (!$this->validate()) {
			return false;
		}
		$auth = Yii::$app->authManager;
		$role = $auth->createRole($this->name);
		$role->description = $this->description;
		if ($auth->add($role)) {
			if (($child = $auth->getRole($this->child)) !== null
				&& !empty($this->child)) {
				return $auth->addChild($role, $child);
			}
		}
		return false;
	}

	public function update()
	{
		if (!$this->validate() || $this->role === null) {
			return false;
		}
		$role = $this->role;
		$oldName = $role->name;
		if ($oldName != $this::DEFAULT_ROLE && $oldName != $this::ADMIN_ROLE) {
			$role->name = $this->name;
		}
		$role->description = $this->description;
		$auth = Yii::$app->authManager;
		if ($auth->update($oldName, $role)) {
			$oldChild = $this->getChildByRole($role->name);
			$newChild = $auth->getRole($this->child);
			if ($oldChild != $newChild) {
				if ($oldChild) {
					$auth->removeChild($role, $oldChild);
				}
				if ($newChild) {
					$auth->addChild($role, $newChild);
				}
			}

			return true;
		}
		return false;
	}

	/**
	 * Return an array via functions [[buildNestable()]] and [[getChildrenRoles()]].
	 * @return array the result.
	 */
	public function getNestable()
	{
	    return $this->buildNestable($this->getChildrenRoles());
	}

	/**
	 * Returns list of Rbac roles in key-value pairs format,
	 * where key is the role name, and the value is the child role name.
	 * @return array list names of Rbac roles.
	 */
	public static function getChildrenRoles()
	{
		$result = [];
		$auth = Yii::$app->authManager;
		$roles = $auth->getRoles();
		foreach ($roles as $role) {
			$child = null;
			foreach ($auth->getChildren($role->name) as $item) {
				if ($item->type == self::TYPE_ROLE) {
					$child = $item->name;
				}
			}
			$result[$role->name] = $child;
		}
		return $result;
	}

	/**
	 * Returns all parents of rbac role via [[getChildrenRoles()]].
	 * @param string $role role name.
	 * @param array $roles array list of rbac roles.
	 * @return array
	 */
	public static function getAllParents($role, $roles = [])
	{
        $result = [];
		if (empty($roles)) {
			$roles = self::getChildrenRoles();
		}
		foreach ($roles as $parent => $child) {
            if ($child == $role) {
				$result[] = $parent;
                $result = array_merge($result, self::getAllParents($parent, $roles));
            }
        }

		return $result;
	}

	/**
	 * Return list of Rbac roles are inherited from the specified role
	 * @param Rbac item $role
	 * @return array of Rbac items
	 */
	public static function getInheritedRoles($role)
	{
		$result = [];
		$auth = Yii::$app->authManager;
		$items = $auth->getRoles();
		foreach ($items as $item) {
			foreach ($auth->getChildren($item->name) as $child) {
				if ($child->type == self::TYPE_ROLE) {
					if($child->name == $role->name) {
						$result[] = $item;
					}
				}
			}
		}
		return $result;
	}

	/**
	 * Return Rbac item extended role from the specified role
	 * @param Rbac item $role
	 * @return Rbac item|null
	 */
	public static function getExtendedRole($role)
	{
		$auth = Yii::$app->authManager;
	    $items = $auth->getRoles();

		foreach ($auth->getChildren($role->name) as $item) {
			if ($item->type == self::TYPE_ROLE) {
				return $item;
			}
		}
		return null;
	}

	/**
	 * Returns the array list in key-value format where key and value are the role name.
	 * @return array the result
	 */
	public function getRolesList()
	{
		$result = [];
		foreach ($this->roles as $role) {
			if ($this->role !== null) {
				if ($role->name == $this->role->name) {
					continue;
				}
			}
			$result[$role->name] = $role->name;
		}
		return $result;
	}

	/**
	 * Returns the array list.
	 * @param array|null $roles array list in key-value format where key is role name and value is child name
	 * @param string|null $parent role name
	 * @param integer|null $depth nesting depth element
	 * @return array multiple array.
	 */
	public static function getRolesListNested($roles = null, $parent = null, $depth = 0)
	{
		$result = [];
		$depth++;
		if ($roles === null) {
			$roles = self::getChildrenRoles();
		}
		foreach ($roles as $role => $child) {
			if ($child == $parent) {
				unset($roles[$role]);
				$result[] = [
					'id' => $role,
					'title' => $role,
					'depth' => $depth,
				];
				$result = array_merge($result, self::getRolesListNested($roles, $role, $depth));
			}
		}
		return $result;
	}

	/**
	 * @param string $name the parent name.
	 * @return null|Item[] the child role or null if child role doesn't exist.
	 */
	public static function getChildByRole($name)
	{
		$auth = Yii::$app->authManager;
		foreach ($auth->getChildren($name) as $child) {
			if ($child->type == Rbac::TYPE_ROLE) {
				return $child;
			}
		}
		return null;
	}

	/**
	 * @return Role[] all roles in the system. The array is indexed by the role names.
	 */
	public function getRoles()
	{
		if ($this->_roles == null) {
			$this->_roles = Yii::$app->authManager->getRoles();
		}
	    return $this->_roles;
	}

	public function hasChildPermission($permission)
	{
		$auth = Yii::$app->authManager;
		$permission = $auth->getPermission($permission);

		return $auth->hasChild($this->role, $permission);
	}

	public static function buildPermissionsByRole($role)
	{
		$auth = Yii::$app->authManager;
		$permissions = $auth->getPermissionsByRole($role);
		$items = [];

		foreach ($permissions as $item) {
			$items[] = $item->name;
		}
		return $items;
	}

	/**
	 * @param array $childrenRoles list of Rbac roles in key-value pairs format,
	 * where key is the role name, and the value is the child role name.
	 * @param string $parent the parent role name
	 * @return array nestable array list of roles with actions.
	 */
	protected function buildNestable($childrenRoles, $parent = null)
	{
		$result = [];
		foreach ($childrenRoles as $role => $child) {
			if ($child == $parent) {
				unset($childrenRoles[$role]);
				$panelOptions = [];
				if ($role == Yii::$app->controller->actionParams['name']) {
					$panelOptions = ['class' => 'uk-active'];
				}
				$result[] = [
					'id' => $role,
					'label' => $role,
					'url' => ['index', 'name' => $role],
					'panelOptions' => $panelOptions,
					'actions' => [
						[
							'icon' => '<span uk-icon="icon: pencil"></span>',
							'url' => ['update', 'name' => $role],
							'linkOptions' => [
                                'uk-tooltip' => true,
                                'title' => Yii::t('system', 'Edit'),
                                'class' => 'dd-nodrag',
                            ],
						],
					],
					'items' => self::buildNestable($childrenRoles, $role),
				];
			}
		}
		return $result;
	}

	/**
	 * the function returns default role via [[getChildrenRoles()]].
	 * @return string the default role name.
	 */
	protected function getDefaultRole()
	{
	    foreach ($this->getChildrenRoles() as $parent => $child) {
	    	if ($child == null) {
	    		return $parent;
	    	}
	    }
	}

}
