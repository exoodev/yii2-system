<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\backend;

use Yii;

/**
 * User model.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class User extends \exoo\system\models\User
{
    const SCENARIO_CREATE = 'create';

    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    private $_roles;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            [['username', 'email', 'roles'], 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 100],
            [
                'email',
                'unique',
                'message' => Yii::t('system', 'This e-mail address has already been taken.'),
            ],
            ['email', 'email'],
            ['roles', 'each', 'rule' => [
                'in', 'range' => array_keys(Yii::$app->authManager->getRoles())]
            ],
            ['password', 'required', 'on' => self::SCENARIO_CREATE],
            ['status_id', 'default', 'value' => self::STATUS_ACTIVE],
            ['status_id', 'in', 'range' => array_keys(self::getStatuses())],
            [['last_visit', 'ip'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['roles'] = Yii::t('system', 'Roles');
        return $labels;
    }

    /**
     * List of user roles.
     * @return array
     */
    public function getRoles()
    {
        if ($this->_roles === null) {
            $auth = Yii::$app->authManager;
            $this->_roles = array_keys($auth->getRolesByUser($this->id));
        }
        return $this->_roles;
    }

    /**
     * Set the value to private variable [[$_roles]].
     * @param mixed $value
     */
    public function setRoles($value)
    {
        $this->_roles = $value;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord || (!$this->isNewRecord && $this->password)) {
                $this->setPassword($this->password);
                $this->generateAuthKey();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $auth = Yii::$app->authManager;
        if (!$insert) {
            $auth->revokeAll($this->id);
        }
        if ($this->roles) {
            foreach ($this->roles as $role) {
                $auth->assign($auth->getRole($role), $this->id);
            }
        }
    }
}
