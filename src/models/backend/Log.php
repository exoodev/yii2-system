<?php

namespace exoo\system\models\backend;

use Yii;
use yii\log\Logger;

/**
 * This is the model class for table "{{%log}}".
 *
 * @property string $id
 * @property integer $level
 * @property string $category
 * @property double $log_time
 * @property string $prefix
 * @property string $message
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level'], 'integer'],
            [['log_time'], 'number'],
            [['prefix', 'message'], 'string'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('system', 'ID'),
            'level' => Yii::t('system', 'Level'),
            'category' => Yii::t('system', 'Category'),
            'log_time' => Yii::t('system', 'Log time'),
            'prefix' => Yii::t('system', 'Prefix'),
            'message' => Yii::t('system', 'Message'),
        ];
    }

    /**
     * Get level name
     * @return string
     */
    public function getLevelName()
    {
        return Logger::getLevelName($this->level);
    }
}
