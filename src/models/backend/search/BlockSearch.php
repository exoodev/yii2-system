<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\backend\search;

use exoo\system\models\Block;
use yii\data\ActiveDataProvider;

/**
 * BlockSearch is the class used to search and filter [[Block]] models.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class BlockSearch extends Block
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'position'], 'integer'],
            [['slug', 'name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Returns instance of the class ActiveDataProvider with introduced search query.
     * @param array $params Search parameters.
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}
