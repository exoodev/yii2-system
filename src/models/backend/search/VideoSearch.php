<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\backend\search;

use exoo\system\models\Video;
use yii\data\ActiveDataProvider;

/**
 * VideoSearch is the class used to search and filter [[Video]] models.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class VideoSearch extends Video
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'position'], 'integer'],
            [['url'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Returns instance of the class ActiveDataProvider with introduced search query.
     * @param array $params Search parameters.
     * @return ActiveDataProvider
     */
    public function search($params, $folder_id)
    {
        $query = self::find()->where(['folder_id' => $folder_id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'position' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}
