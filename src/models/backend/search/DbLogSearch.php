<?php

namespace exoo\system\models\backend\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use exoo\system\models\DbLog;

/**
 * ActiveRecordLogSearch represents the model behind the search form about `common\modules\system\models\ActiveRecordLog`.
 */
class DbLogSearch extends DbLog
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['user.username']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'event_id', 'model_id', 'user_id'], 'integer'],
            [['data', 'model', 'user.username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DbLog::find()
            ->from(['db' => DbLog::tableName()])
            ->joinWith(['user user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'db.id' => $this->id,
            'event_id' => $this->event_id,
            'model_id' => $this->model_id,
        ]);

        $query->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'user.username', $this->getAttribute('user.username')]);

        return $dataProvider;
    }
}
