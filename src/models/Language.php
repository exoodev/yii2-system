<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models;

use Yii;
use exoo\position\PositionBehavior;

/**
 * Language is the class for extension System and table {{%system_language}}.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system_language}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'url'], 'required'],
            [['id', 'url'], 'string', 'max' => 20],
            [['id'], 'unique'],
            [['position'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('system', 'Tag'),
            'url' => Yii::t('system', 'URL'),
            'position' => Yii::t('system', 'Position'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'positionBehavior' => [
                'class' => PositionBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->orderBy(['position' => SORT_ASC]);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        if (self::find()->count() < 2) {
            Yii::$app->session->setFlash('alert.danger', Yii::t('system', 'You can not delete all content languages'));
            return false;
        }

        return true;
    }
}
