<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * User profile.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Profile extends ActiveRecord
{
	const GENDER_NOT_SPECIFIED = 0;
	const GENDER_WOMAN = 1;
	const GENDER_MAN = 2;

	/**
	 * @var string User birth day.
	 */
	private $_birth;
	/**
	 * @var \yii\web\UploadedFile Uploaded file
	 */
	public $fileAvatar;
	/**
	 * @var string
	 */
	public $pathToAvatars;
	/**
	 * @var string
	 */
	public $folderAvatars;

	/**
     * @inheritdoc
     */
	public function init()
	{
	    parent::init();

		if (!$this->pathToAvatars) {
			$this->pathToAvatars = Yii::getAlias('@storage' . Yii::$app->params['user.avatar.path']);
		}

		if (!$this->folderAvatars) {
			$this->folderAvatars = date('y') . '/' . date('m') . '/' . date('d');
		}
	}

	/**
     * @inheritdoc
     */
	public static function tableName()
	{
		return '{{%system_profile}}';
	}

	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
			['phone', 'string', 'max' => 50],
			[['timezone'], 'string', 'max' => 100],
			[['first_name', 'last_name', 'address', 'avatar_url', 'city', 'country'], 'string', 'max' => 255],
			[['birth'], 'date'],
			[['birthday', 'gender_id'], 'integer'],
			[['fileAvatar'], 'image', 'extensions' => 'png, jpg', 'maxSize' => 3000000],
		];
	}

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
		return [
			'uniq_id' => 'uID',
			'avatar_url' => Yii::t('system', 'Avatar'),
			'fileAvatar' => Yii::t('system', 'Avatar'),
			'first_name' => Yii::t('system', 'First name'),
			'last_name' => Yii::t('system', 'Last name'),
			'birthday' => Yii::t('system', 'Birthday'),
			'birth' => Yii::t('system', 'Birthday'),
			'gender_id' => Yii::t('system', 'Gender'),
			'gender' => Yii::t('system', 'Gender'),
			'phone' => Yii::t('system', 'Phone'),
			'country' => Yii::t('system', 'Country'),
			'city' => Yii::t('system', 'City'),
			'address' => Yii::t('system', 'Address'),
			'timezone' => Yii::t('system', 'Timezone'),
		];
	}

	/**
     * Get gender list.
     * @return array
     */
    public static function getGenderList()
    {
        return [
            self::GENDER_WOMAN => Yii::t('system', 'Woman'),
            self::GENDER_MAN => Yii::t('system', 'Man'),
        ];
    }

    /**
     * Returns current gender of the model.
     * @return string gender of the model.
     */
    public function getGender()
    {
		if ($this->gender_id) {
			$gender = self::getGenderList();
	        return $gender[$this->gender_id];
		}
    }

	/**
	 * Upload and save file.
	 * @return boolean
	 */
	public function uploadAvatar()
	{
	    if ($this->fileAvatar !== null) {
			$params = Yii::$app->params;
			if ($this->avatar_url) {
				$image = $this->pathToAvatars  . $this->avatar_url;
				if (file_exists($image)) {
					@unlink($image);
				}
				$pathinfo = pathinfo($this->avatar_url);
                $folder = $pathinfo['dirname'];
            } else {
				$folder = $this->folderAvatars;
			}
			if (!file_exists($this->pathToAvatars . $folder)) {
				mkdir($this->pathToAvatars . $folder, 0777, true);
			}
			$fileName = '/' . uniqid() . '.' . $this->fileAvatar->extension;
			if ($this->fileAvatar->saveAs($image = $this->pathToAvatars . $folder . $fileName)) {
				Image::thumbnail($image, $params['user.avatar.width'], $params['user.avatar.heigth'])
					->save($image, [
						'quality' => Yii::$app->params['user.avatar.quality'],
					]);
				$this->avatar_url = $folder . $fileName;
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * Get avatar.
	 * @return string
	 */
	public function getAvatarUrl()
 	{
		if ($this->avatar_url) {
			$storage = Yii::$app->settings->get('system', 'storageUrl');
 	    	return $storage . Yii::$app->params['user.avatar.path'] . $this->avatar_url;
 	    }

		return Yii::$app->params['user.avatar.default'];
 	}

	public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	/**
	 * @return string User birthday.
	 */
	public function getBirth()
	{
	    if (!isset($this->_birth) && !$this->isNewRecord && $this->birthday) {
	    	$this->_birth = Yii::$app->formatter->asDate($this->birthday);
	    }
		return $this->_birth;
	}

	/**
	 * @param mixed $value Sets user birthday.
	 */
	public function setBirth($value)
	{
	    $this->_birth = $value;
	}

	/**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			if ($this->_birth) {
				$this->birthday = strtotime($this->_birth);
			}
            if ($this->isNewRecord) {
                $this->uniq_id = uniqid();
            }
            return true;
        } else {
            return false;
        }
    }
}
