<?php

namespace exoo\system\models\user;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 *
 */
class Upload extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            ArrayHelper::getValue(Yii::$app->params, 'user.upload.rules', [
                ['file'], 'file',
            ]),
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            Yii::$app->fileStorage->addBucket('users', [
                'baseSubPath' => '_users',
                'fileSubDirTemplate' => Yii::$app->user->id . '/{^name}/{^^name}',
            ]);
            $bucket = Yii::$app->fileStorage->getBucket('users');

            $filename = uniqid(rand()) . '.' . $this->file->extension;
            $uploaded = $bucket->copyFileIn($this->file->tempName, $filename);

            if ($uploaded) {
                return [
                    'name' => $this->file->name,
                    'url' => $bucket->getFileUrl($filename),
                    'type' => $this->file->type,
                    'size' => $this->file->size,
                ];
            }
        } else {
            return [
                'errors' => $this->getErrors('file'),
            ];
        }
    }
}
