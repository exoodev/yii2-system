<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models;

use Yii;
use exoo\system\models\query\BlockQuery;
use exoo\status\StatusTrait;
use yii\behaviors\SluggableBehavior;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;
use exoo\position\PositionBehavior;
use yii2tech\ar\variation\VariationBehavior;

/**
 * Block is the class for extension System and table {{%system_block}}.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Block extends \yii\db\ActiveRecord
{
    use StatusTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system_block}}';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new BlockQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['position'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatusesList())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('system', 'Name'),
            'slug' => Yii::t('system', 'Slug'),
            'status_id' => Yii::t('system', 'Status'),
            'position' => Yii::t('system', 'Position'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'ensureUnique' => false,
                'immutable' => true,
            ],
            [
                'class' => DynamicAttributeBehavior::className(),
                'saveDynamicAttributeDefaults' => false,
                'dynamicAttributeDefaults' => [],
            ],
            'positionBehavior' => [
                'class' => PositionBehavior::className(),
            ],
            'translations' => [
                'class' => VariationBehavior::className(),
                'variationsRelation' => 'translations',
                'defaultVariationRelation' => 'defaultTranslation',
                'variationOptionReferenceAttribute' => 'language_id',
                'optionModelClass' => Language::className(),
                'defaultVariationOptionReference' => Yii::$app->language,
                'variationAttributeDefaultValueMap' => [
                    'title' => 'name'
                ],
                'optionQueryFilter' => function ($query) {
                    $query->orderBy(['position' => SORT_ASC]);
                }
            ],
        ];
    }

    public static function getStatuses(): array
    {
        return [
            [
                'id' => self::STATUS_INACTIVE,
                'name' => Yii::t('system', 'Inactive'),
                'class' => 'off ex-text-grey-300',
            ],
            [
                'id' => self::STATUS_ACTIVE,
                'name' => Yii::t('system', 'Active'),
                'class' => 'on uk-text-success',
            ],
        ];
    }

    /**
     * Returns a value indicating whether the current model is active.
     * @return boolean if model is active value will be `true`, else `false`.
     */
    public function getIsActive()
    {
        return $this->status_id === self::STATUS_ACTIVE;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this
            ->hasMany(BlockTranslation::className(), ['block_id' => 'id'])
            ->with('language');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultTranslation()
    {
        return $this->hasDefaultVariationRelation();
    }
}
