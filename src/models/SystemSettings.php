<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 3.0.0
 */

namespace exoo\system\models;

use Yii;
use yii\base\Model;

/**
 * SystemSettings class.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 3.0
 */
class SystemSettings extends Model
{
    const REGISTRATION_DISABLED = 'disabled';
    const REGISTRATION_ENABLED = 'enabled';
    const REGISTRATION_APPROVAL = 'approval';

    /**
     * @var string the meta tag description
     */
    public $metaDescription;
    /**
     * @var boolean the enable site
     */
    public $offline = true;
    /**
     * @var string the offline message
     */
    public $offlineMessage = 'The website is on maintenance';
    /**
     * @var string the backend language
     */
    public $backendLanguage = 'ru-RU';
    /**
     * @var string the frontend language
     */
    public $frontendLanguage = 'ru-RU';
    /**
     * @var string
     */
    public $fromEmail = 'info@exoodev.com';
    /**
     * @var string
     */
    public $fromName = 'Noreply. My company';
    /**
     * @var string frontend url
     */
    public $frontendUrl = 'http://exoocmf.loc';
    /**
     * @var string url where user will be redirect after registration
     */
    public $loginRedirect = 'index';
    /**
     * @var string the registration
     */
    public $registration = 'enabled';
    /**
    * @var boolean the CAPTCHA registration
    */
    public $signupCaptcha = false;
    /**
    * @var boolean the confirm e-mail
    */
    public $emailConfirm = false;
    /**
    * @var boolean the registered e-mail
    */
    public $emailRegistered = false;
    /**
     * @var boolean the disable temporary files backend
     */
    public $forceCopyBackend = false;
    /**
     * @var boolean the disable temporary files frontend
     */
    public $forceCopyFrontend = false;
    /**
     * @var string the heading codes
     */
    public $codeHeader;
    /**
     * @var string the footer codes
     */
    public $codeFooter;
    /**
     * @var string Public sitekey used in form for reCaptcha.
     */
    public $siteKeyPublic;
    /**
     * @var string Secret sitekey used to validate user response.
     */
    public $siteKeySecret;
    /**
     * @var boolean multilinguage of frontend application.
     */
    public $multilanguage = false;
    /**
     * @var string
     */
    public $storageUrl = 'http://s.exoocmf.loc';
    /**
     * @var string
     */
    public $logoUrl;
    /**
     * @var integer the counter id yandex metrika
     */
    public $ymId;
    /**
     * @var string the OAuthToken yandex metrika
     */
    public $ymOAuthToken;
    /**
     * @var string
     */
    public $instaClientId;
    /**
     * @var string
     */
    public $instaAccessToken;
    /**
     * @var string the property
     */
    public $schemaOrg = true;

    /**
     * @inheritdoc
     */
    public function rules()
	{
		return [
			[
                [
                    'metaDescription',
                    'offlineMessage',
                    'backendLanguage',
                    'frontendLanguage',
                    'fromName',
                    'codeHeader',
                    'codeFooter',
                    'siteKeyPublic',
                    'siteKeySecret',
                    'storageUrl',
                    'ymOAuthToken',
                    'logoUrl',
                    'instaClientId',
                    'instaAccessToken',
                ],
                'string',
            ],
            [['siteKeyPublic', 'siteKeySecret'], 'validateCaptcha'],
            [['ymId'], 'integer'],
            [['offline', 'multilanguage'], 'boolean'],
            ['fromEmail', 'email'],
            ['frontendUrl', 'url'],
            [['emailConfirm', 'registration'], 'required'],
            [
                [
                    'signupCaptcha',
                    'emailRegistered',
                    'emailConfirm',
                    'forceCopyBackend',
                    'forceCopyFrontend',
                    'schemaOrg',
                ],
                'boolean',
            ],
            ['registration', 'in', 'range' => array_keys($this->registrations)],
		];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
	{
		return [
			'metaDescription' => Yii::t('system', 'Meta description'),
            'offline' => Yii::t('system', 'Offline'),
            'offlineMessage' => Yii::t('system', 'Message'),
            'backendLanguage' => Yii::t('system', 'Language'),
            'frontendLanguage' => Yii::t('system', 'Language'),
            'frontendUrl' => Yii::t('system', 'Frontend URL'),
            'mailer' => Yii::t('system', 'Mailer'),
            'fromEmail' => Yii::t('system', 'From e-mail'),
            'fromName' => Yii::t('system', 'From name'),
            'registration' => Yii::t('system', 'Registration'),
            'signupCaptcha' => Yii::t('system', 'Captcha at registration'),
            'emailRegistered' => Yii::t('system', 'Send user data at registration'),
            'emailConfirm' => Yii::t('system', 'Confirmation email address at registration'),
            'forceCopyBackend' => Yii::t('system', 'Disable temporary files'),
            'forceCopyFrontend' => Yii::t('system', 'Disable temporary files'),
            'codeHeader' => Yii::t('system', 'Header'),
            'codeFooter' => Yii::t('system', 'Footer'),
            'siteKeyPublic' => Yii::t('system', 'Site key'),
            'siteKeySecret' => Yii::t('system', 'Secret key'),
            'multilanguage' => Yii::t('system', 'Multilanguage'),
            'storageUrl' => Yii::t('system', 'URL files storage'),
            'logoUrl' => Yii::t('system', 'URL logo'),
            'ymId' => Yii::t('system', 'ID'),
            'ymOAuthToken' => Yii::t('system', 'OAuth token'),
            'instaClientId' => 'Client ID',
            'instaAccessToken' => 'Access Token',
            'schemaOrg' => Yii::t('system', 'Semantic Markup schema.org'),
		];
	}

    /**
     * Returns array list in key-value pairs format, where the key is constant value and value is a translation.
     * @return array
     */
    public function getRegistrations()
    {
        return [
            self::REGISTRATION_DISABLED => Yii::t('system', 'Disabled'),
            self::REGISTRATION_ENABLED => Yii::t('system', 'Enabled'),
            self::REGISTRATION_APPROVAL => Yii::t('system', 'Enabled with administration approval'),
        ];
    }

    public function validateCaptcha($attribute, $params)
    {
        if ($this->signupCaptcha && (!$this->siteKeyPublic || !$this->siteKeySecret)) {
            $this->addError($attribute, Yii::t('system', 'ReCaptcha keys is required.'));
        }
    }
}
