<?php

namespace exoo\system\models;

use Yii;

/**
 * This is the model class for table "{{%system_video_translation}}".
 *
 * @property int $video_id
 * @property int $language_id
 * @property string $title
 * @property string $content
 *
 * @property Video $video
 * @property Language $language
 */
class VideoTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%system_video_translation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['video_id'], 'integer'],
            [['language_id'], 'string', 'max' => 5],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['video_id'], 'exist', 'skipOnError' => true, 'targetClass' => Video::class, 'targetAttribute' => ['video_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'video_id' => Yii::t('system', 'Video'),
            'language_id' => Yii::t('system', 'Language'),
            'title' => Yii::t('system', 'Title'),
            'description' => Yii::t('system', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Video::class, ['id' => 'video_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }
}
