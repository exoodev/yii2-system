<?php

namespace exoo\system\models;

use Yii;

/**
 * This is the model class for table "{{%system_activerecord_log}}".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $data
 * @property string $model
 * @property integer $model_id
 * @property integer $user_id
 * @property integer $created_at
 *
 * @property SystemUser $user
 */
class DbLog extends \yii\db\ActiveRecord
{
    const EVENT_CREATE = 1;
    const EVENT_UPDATE = 2;
    const EVENT_DELETE = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system_dblog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'model', 'model_id', 'user_id', 'created_at'], 'required'],
            [['event_id', 'model_id', 'user_id', 'created_at'], 'integer'],
            [['data'], 'string'],
            [['model'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('system', 'ID'),
            'event_id' => Yii::t('system', 'Event'),
            'data' => Yii::t('system', 'Data'),
            'model' => Yii::t('system', 'Model'),
            'model_id' => Yii::t('system', 'Model ID'),
            'user_id' => Yii::t('system', 'User'),
            'created_at' => Yii::t('system', 'Created at'),
        ];
    }

    /**
     * Events.
     * @return array
     */
    public static function getEvents()
    {
        return [
            self::EVENT_CREATE => 'Create',
            self::EVENT_UPDATE => 'Update',
            self::EVENT_DELETE => 'Delete',
        ];
    }

    /**
     * Returns event.
     * @return string.
     */
    public function getEvent()
    {
        $events = self::getEvents();
        return $events[$this->event_id];
    }

    /**
     * Events labels.
     * @return array
     */
    public static function getEventLabels()
    {
        return [
            self::EVENT_CREATE => 'success',
            self::EVENT_UPDATE => 'default',
            self::EVENT_DELETE => 'danger',
        ];
    }

    /**
     * Returns event label.
     * @return string.
     */
    public function getEventLabel()
    {
        $labels = self::getEventLabels();
        return $labels[$this->event_id];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'user_id']);
    }
}
