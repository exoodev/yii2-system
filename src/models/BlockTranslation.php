<?php

namespace exoo\system\models;

use Yii;

/**
 * This is the model class for table "{{%system_block_translation}}".
 *
 * @property int $block_id
 * @property int $language_id
 * @property string $title
 * @property string $content
 *
 * @property Block $block
 * @property Language $language
 */
class BlockTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%system_block_translation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['content', 'required'],
            [['block_id'], 'integer'],
            [['language_id'], 'string', 'max' => 5],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::class, 'targetAttribute' => ['block_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'block_id' => Yii::t('system', 'Block'),
            'language_id' => Yii::t('system', 'Language'),
            'title' => Yii::t('system', 'Title'),
            'content' => Yii::t('system', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(Block::class, ['id' => 'block_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }
}
