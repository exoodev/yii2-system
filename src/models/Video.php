<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2019
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models;

use Yii;
use yii\helpers\ArrayHelper;
use exoo\system\models\query\VideoQuery;
use exoo\status\StatusTrait;
use yii2tech\ar\linkmany\LinkManyBehavior;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;
use exoo\position\PositionBehavior;
use yii2tech\ar\variation\VariationBehavior;

/**
 * Video is the class for extension System and table {{%system_video}}.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Video extends \yii\db\ActiveRecord
{
    use StatusTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system_video}}';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new VideoQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['position', 'height', 'folder_id', 'height'], 'integer'],
            ['url', 'url'],
            ['url', 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::getStatusesList())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'URL',
            'folder_id' => Yii::t('system', 'Folder'),
            'height' => Yii::t('system', 'Height'),
            'status_id' => Yii::t('system', 'Status'),
            'position' => Yii::t('system', 'Position'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => DynamicAttributeBehavior::className(),
                'saveDynamicAttributeDefaults' => false,
                'dynamicAttributeDefaults' => [
                    'height' => 300
                ],
            ],
            'positionBehavior' => [
                'class' => PositionBehavior::className(),
            ],
            'translations' => [
                'class' => VariationBehavior::className(),
                'variationsRelation' => 'translations',
                'defaultVariationRelation' => 'defaultTranslation',
                'variationOptionReferenceAttribute' => 'language_id',
                'optionModelClass' => Language::className(),
                'defaultVariationOptionReference' => Yii::$app->language,
                // 'variationAttributeDefaultValueMap' => [
                //     'title' => 'url'
                // ],
                'optionQueryFilter' => function ($query) {
                    $query->orderBy(['position' => SORT_ASC]);
                }
            ],
        ];
    }

    public static function getStatuses(): array
    {
        return [
            [
                'id' => self::STATUS_INACTIVE,
                'name' => Yii::t('system', 'Inactive'),
                'class' => 'off ex-text-grey-300',
            ],
            [
                'id' => self::STATUS_ACTIVE,
                'name' => Yii::t('system', 'Active'),
                'class' => 'on uk-text-success',
            ],
        ];
    }

    /**
     * Returns a value indicating whether the current model is active.
     * @return boolean if model is active value will be `true`, else `false`.
     */
    public function getIsActive()
    {
        return $this->status_id === self::STATUS_ACTIVE;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this
            ->hasMany(VideoTranslation::className(), ['video_id' => 'id'])
            ->with('language');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultTranslation()
    {
        return $this->hasDefaultVariationRelation();
    }

    public function getFoldersList()
    {
        $folders = VideoFolder::find()->all();
        return ArrayHelper::map($folders, 'id', 'name');
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->url = str_replace('watch?v=', 'embed/', $this->url);
        return true;
    }
}
