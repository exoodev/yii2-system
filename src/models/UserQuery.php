<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class UserQuery extends \yii\db\ActiveQuery
{
    /**
     * Select active.
     *
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status_id' => User::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * Select inactive.
     *
     * @return $this
     */
    public function inactive()
    {
        $this->andWhere(['status_id' => User::STATUS_INACTIVE]);

        return $this;
    }

}
