<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models;

use Yii;
use exoo\system\models\query\UserQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use exoo\system\behaviors\IpBehavior;

/**
 * User model.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;
    const STATUS_BLOCKED = 3;

    /**
     * Default role
     */
    const ROLE_REGISTERED = 'Registered';

    /**
     * @var string
     */
    public $_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system_user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            IpBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('system', 'Username'),
            'password' => Yii::t('system', 'Password'),
            'email' => 'E-mail',
            'status_id' => Yii::t('system', 'Status'),
            'created_at' => Yii::t('system', 'Registered since'),
            'updated_at' => Yii::t('system', 'Updated at'),
            'last_visit' => Yii::t('system', 'Last visit'),
            'ip' => Yii::t('system', 'IP address'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * Statuses used in class.
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('system', 'Active'),
            self::STATUS_INACTIVE => Yii::t('system', 'Inactive'),
            self::STATUS_DELETED => Yii::t('system', 'Deleted'),
            self::STATUS_BLOCKED => Yii::t('system', 'Blocked'),
        ];
    }

    /**
     * The current status of the model.
     * @return string
     */
    public function getStatus()
    {
        $statuses = self::getStatuses();
        return $statuses[$this->status_id];
    }

    /**
     * Css classes related with statuses.
     * @return array
     */
    public static function getStatusesCssClass()
    {
        return [
            self::STATUS_ACTIVE => 'success',
            self::STATUS_INACTIVE => 'warning',
            self::STATUS_DELETED => 'danger',
            self::STATUS_BLOCKED => 'danger',
        ];
    }

    /**
     * Css class depending on the status of the model.
     * @return string
     */
    public function getStatusCssClass()
    {
        $statuses = self::getStatusesCssClass();
        return $statuses[$this->status_id];
    }

    /**
     * Related model of the class [[Profile]].
     * @return Profile
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()->where(['id' => $id])->active()->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by name.
     *
     * @param string $name
     * @return static|null
     */
    public static function findByName($name)
    {
        return static::find()->where(['username' => $name])->active()->one();
    }

    /**
     * Finds user by email.
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::find()->where(['email' => $email])->active()->one();
    }

    /**
     * Finds user by inactive email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByInactiveEmail($email)
    {
        return static::find()->where(['email' => $email])->inactive()->one();
    }

    /**
     * Finds user by password reset token.
     *
     * @param string $token password reset token.
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::find()->where(['password_reset_token' => $token])->active()->one();
    }

    /**
     * @param string $email_token
     * @return static|null
     */
    public static function findByEmailToken($email_token)
    {
        return static::find()
            ->where(['email_token' => $email_token])
            ->one();
    }

    /**
     * Generates email token.
     */
    public function generateEmailToken()
    {
        $this->email_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Removes email token.
     */
    public function removeEmailToken()
    {
        $this->email_token = null;
    }

    /**
     * Finds out if password reset token is valid.
     *
     * @param string $token password reset token.
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate.
     * @return boolean if password provided is valid for current user.
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model.
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key.
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token.
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Returns the roles that are assigned to the user.
     * @param mixed $value
     * @return string the result
     */
    public function getRoles()
    {
        return array_keys(Yii::$app->authManager->getRolesByUser($this->id));
    }

    /**
     * Assign role for user
     * @param string $role
     */
    public function assignRole($role)
    {
        $auth = Yii::$app->authManager;
        $authRole = $auth->getRole($role);
        $auth->assign($authRole, $this->id);
    }

    /**
     * Send mail with the data access
     * @return boolean the send mail
     */
    public function sendMailRegistered()
    {
        $mail = Yii::$app->mailer->compose(
                ['html' => 'emailRegistered-html', 'text' => 'emailRegistered-text'],
                ['user' => $this]
            )
            ->setFrom([Yii::$app->settings->get('system', 'fromEmail') => Yii::$app->settings->get('system', 'fromName')])
            ->setTo($this->email)
            ->setSubject(Yii::t('system', 'Registration on') . ' ' . Yii::$app->settings->get('system', 'company'))
            ->send();

        if (!$mail) {
            Yii::$app->getSession()->setFlash('alert.danger', Yii::t('system', 'User data is not sent. Please notify the site administration.'));
        }
    }

    /**
     * Send mail
     * @return boolean the send mail
     */
    public function sendMailConfirm()
    {
        $mail = Yii::$app->mailer->compose(
                ['html' => 'emailConfirm-html', 'text' => 'emailConfirm-text'],
                ['user' => $this]
            )
            ->setFrom([Yii::$app->settings->get('system', 'fromEmail') => Yii::$app->settings->get('system', 'fromName')])
            ->setTo($this->email)
            ->setSubject(Yii::t('system', 'E-mail confirmation for') . ' ' . Yii::$app->settings->get('system', 'company'))
            ->send();

        if (!$mail) {
            Yii::$app->getSession()->setFlash('alert.danger', Yii::t('system', 'Confirmation for registration is not sent. Please notify the site administration.'));
        }

        return $mail;
    }
}
