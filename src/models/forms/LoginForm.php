<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\models\forms;

use Yii;
use yii\base\Model;
use exoo\system\models\User;

/**
 * Login form.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class LoginForm extends Model
{
    const SCENARIO_BACKEND_IN = 'backendIn';

    /**
     * @var string User email.
     */
    public $email;
    /**
     * @var string User password.
     */
    public $password;
    /**
     * @var boolean Whether to remember user password.
     */
    public $rememberMe = true;
    /**
     * @var User Instance of the class [[User]].
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'email',
                'required',
                'message' => Yii::t('system', 'Cannot be blank.'),
            ],
            ['password', 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            // access is validated by validateAccess()
            ['password', 'validateAccess', 'on' => self::SCENARIO_BACKEND_IN],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('system', 'E-mail'),
            'password' => Yii::t('system', 'Password'),
            'rememberMe' => Yii::t('system', 'Remember me'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('system', 'Incorrect e-mail or password.'));
            } elseif ($user && $user->status_id == User::STATUS_BLOCKED) {
                $this->addError($attribute, Yii::t('system', 'Your account has been suspended.'));
            } elseif ($user && $user->status_id == User::STATUS_INACTIVE) {
                $this->addError($attribute, Yii::t('system', 'Your account is not activate.'));
            }
        }
    }

    /**
     * Validates the access.
     * This method serves as the inline validation for access.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateAccess($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user && !Yii::$app->authManager->checkAccess($user->id, 'system.backendIn')) {
                $this->addError($attribute, Yii::t('system', 'Access id denied.'));
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return boolean whether the user is logged in successfully.
     */
    public function login()
    {
        if ($this->validate()) {
            $duration = $this->rememberMe ? Yii::$app->params['user.rememberMeDuration'] : 0;
            return Yii::$app->user->login($this->getUser(), $duration);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]].
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
}
