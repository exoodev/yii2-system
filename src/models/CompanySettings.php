<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 3.0.0
 */

namespace exoo\system\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * CompanySettings class.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 3.0
 */
class CompanySettings extends Model
{
    /**
     * @var string the company name
     */
    public $name = 'My Company';
    /**
     * @var string the description
     */
    public $description;
    /**
     * @var array the phones
     */
    public $phone;
    /**
     * @var array the emails
     */
    public $email;
    /**
     * @var array the addresses
     */
    public $address;
    /**
     * @var array the social
     */
    public $social;

    /**
     * @inheritdoc
     */
    public function rules()
	{
		return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['phone', 'address', 'social'], 'each', 'rule' => ['each', 'rule' => ['string']]],
            ['email', 'each', 'rule' => ['each', 'rule' => ['email']]],
		];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
	{
		return [
            'name' => Yii::t('system', 'Name'),
            'description' => Yii::t('system', 'Description'),
            'phone' => Yii::t('system', 'Phone'),
            'email' => Yii::t('system', 'E-mail'),
            'address' => Yii::t('system', 'Postal address'),
            'social' => Yii::t('system', 'Social networks'),
		];
	}
}
