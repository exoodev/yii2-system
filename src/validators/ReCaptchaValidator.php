<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\system\validators;

use Yii;
use yii\helpers\Json;
use yii\validators\Validator;

/**
 * Validator for reCaptcha.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 * @see https://developers.google.com/recaptcha/docs/verify
 */
class ReCaptchaValidator extends Validator
{
    /**
     * @var string
     */
    public $secretKey;
    /**
     * @var boolean whether this validation rule should be skipped if the attribute value
     * is null or an empty string.
     */
    public $skipOnEmpty = false;
    /**
     * @var string
     */
    private $api_url = 'https://www.google.com/recaptcha/api/siteverify?';
    /**
     * @var string
     */
    public $message;

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $request = Yii::$app->request;
        if ($response = $request->post('g-recaptcha-response')) {
            if (!$this->verify($response, $request->userIP)) {
                $message = $this->message ?: Yii::t('system', 'The captcha is incorrect.');
                $model->addError($attribute, $message);
            }
        }
    }

    /**
     * @param string $userIp
     * @param string $response
     * @return bool
     */
    private function verify($response, $userIp)
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => $this->api_url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => [
                'secret' => $this->secretKey,
                'response' => $response,
                'remoteip' => $userIp
            ],
            CURLOPT_RETURNTRANSFER => true
        ]);

        $output = curl_exec($ch);
        curl_close($ch);

        $output = Json::decode($output);

        return $output['success'];
    }
}
