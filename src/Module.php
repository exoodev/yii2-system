<?php
/**
 * @link https://exoodev.com/
 * @copyright Copyright (c) 2019 ExooDev LLC
 * @license https://exoodev.com/license/
 */

namespace exoo\system;

use Yii;
use exoo\system\components\Menu;

/**
 * Module class.
 * @since 1.0
 */
class Module extends \yii\base\Module
{
    /**
     * @var string the name extension
     */
    public $name;
    /**
     * @var boolean
     */
    public $isBackend = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->name = Yii::t('system', 'System');

        if ($this->isBackend === true) {
            $this->setViewPath('@exoo/system/views/backend');
            if ($this->controllerNamespace === null) {
                $this->controllerNamespace = 'exoo\system\controllers\backend';
            }
        } else {
            $this->setViewPath('@exoo/system/views/frontend');
            if ($this->controllerNamespace === null) {
                $this->controllerNamespace = 'exoo\system\controllers\frontend';
            }
        }

        parent::init();
    }
}
