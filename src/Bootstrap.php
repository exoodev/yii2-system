<?php
/**
 * @link https://exoodev.com/
 * @copyright Copyright (c) 2019 ExooDev LLC
 * @license https://exoodev.com/license/
 */

namespace exoo\system;

use Yii;
use yii\helpers\Json;
use yii\web\View;
use yii\web\Response;
use yii\base\BootstrapInterface;

/**
 * Bootstrap class.
 * @since 1.0
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        Yii::$classMap['yii\helpers\Html'] = '@exoo/system/helpers/Html.php';
        Yii::$classMap['yii\helpers\StringHelper'] = '@exoo/system/helpers/StringHelper.php';

        $app->i18n->translations['system'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@exoo/system/messages',
        ];

        if ($app instanceof \yii\web\Application) {
            if ($app->getModule('system')->isBackend) {
                $app->assetManager->forceCopy = $app->settings->get('system', 'forceCopyBackend');
                $app->language = $app->settings->get('system', 'backendLanguage');
                $this->setIndexUrlInUserSession();
            } else {
                $this->initFrontend();
                Yii::$app->getResponse()->on(Response::EVENT_AFTER_PREPARE, function($event) {
                    $event->sender->content = Yii::$app->shortcodes->parse($event->sender->content, true);
                });
            }
            $app->on($app::EVENT_BEFORE_ACTION, function ($event) {
                $this->userLastVisit();
            });
        }
    }

    /**
     * Frontend init
     */
    public function initFrontend()
    {
        $app = Yii::$app;
    
        if ($app instanceof \yii\web\Application) {
            if ($app->settings->get('system', 'offline') && !$app->user->can('system.backendIn')) {
                $app->catchAll = ['site/offline'];
            }
        }
    
        $app->on($app::EVENT_AFTER_ACTION, function($event) {
            $this->redirect();
        });
    
        $app->assetManager->forceCopy = $app->settings->get('system', 'forceCopyFrontend');
        $app->language = $app->settings->get('system', 'frontendLanguage');
    
        if ($app->settings->get('system', 'multilanguage')) {
            $this->setUrlManager();
        }
    }

    /**
     * Set current url in user session
     */
    public function setIndexUrlInUserSession()
    {
        Yii::$app->on(Yii::$app::EVENT_AFTER_ACTION, function ($event) {
            if (Yii::$app->request->isGet && !Yii::$app->request->isAjax && $event->action->id == 'index') {
                Yii::$app->user->setReturnUrl([Yii::$app->request->url]);
            }
        });
    }

    /**
     * Redirect 301
     */
    protected function redirect()
    {
        $params = Yii::$app->params;

        if (isset($params['redirect'])) {
            $links = $params['redirect'];
            $url = Yii::$app->request->url;

            if (key_exists($url, $links)) {
                Yii::$app->response->redirect($links[$url], 301)->send();
            }
        }
    }

    public function setUrlManager()
    {
        Yii::$app->setComponents([
            'urlManager' => array_merge(Yii::$app->components['urlManager'], [
                'class' => 'codemix\localeurls\UrlManager',
                'languages' => $this->getLocaleUrls(),
            ])
        ]);
    }

    /**
     * Get Locale Urls
     * @return array
     */
    protected function getLocaleUrls()
    {
        $languages = [];
        foreach (Yii::$app->locale->languages as $language) {
            if (!empty($language['url'])) {
                $languages[$language['url']] = $language['id'];
            }
        }

        return $languages;
    }

    /**
     * Set the last visit of the user
     */
    public function userLastVisit()
    {
        $user = Yii::$app->user;
        if (!$user->isGuest) {
            $user->identity->touch('last_visit');
            // $user->identity->setIP('ip');
        }
    }
}
